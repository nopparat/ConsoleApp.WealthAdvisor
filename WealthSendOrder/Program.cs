﻿using ClosedXML.Excel;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Configuration;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml;
using WealthSendOrder.Access;

namespace WealthSendOrder
{
    class Program
    {
        private static Logger logger = LogManager.GetLogger("databaseLogger");
        private static Logger logEService = LogManager.GetLogger("databaseLogger");
        private static string AWAITING_SEND_ORDER = "Awaiting send order";
        private static string COMPLETE = "complete";
        private static string sid { get; set; }
        private static string baseUrlGet = ConfigurationManager.AppSettings["WebApiGet"];
        private static string baseUrlSet = ConfigurationManager.AppSettings["WebApiSet"];
        private static string user = ConfigurationManager.AppSettings["user"];
        private static string password = ConfigurationManager.AppSettings["password"];
        private static string product = ConfigurationManager.AppSettings["product"];

        static void Main(string[] args)
        {
            if (CheckWorkingDay())
            {
                LineService.lineNotify($"Start Send Order");
                SendOrderNew();
            }
        }
        
        static void SendOrderNew()
        {
            Console.WriteLine("");
            Console.WriteLine("********* Start SendOrder *********");
            Console.WriteLine("");
            using (WealthAdvise_DevEntities wealthDB = new WealthAdvise_DevEntities())
            {
                using (DbContextTransaction transaction = wealthDB.Database.BeginTransaction())
                {
                    try
                    {
                        List<user_model_allocation> users = wealthDB.user_model_allocation.ToList().Where(u => !u.account_no.Equals(string.Empty) && u.is_active.Equals(true)).ToList();

                        List<BeforeSendOrder> groupOrderBuys = new List<BeforeSendOrder>();
                        String strEffDate = GetEffDate();
                        DateTime effDate = DateTime.ParseExact(strEffDate, "dd/MM/yyyy", CultureInfo.GetCultureInfo("en-US"));

                        List<temp_order_sell> _temp_order_sell = wealthDB.temp_order_sell.ToList();
                        List<string> listRefNo = new List<string>();

                        foreach (var user in users)
                        {
                            List<BeforeSendOrder> orderSend = new List<BeforeSendOrder>();

                            List<order_fund> orders = new List<order_fund>();
                                orders = wealthDB.order_fund.ToList()
                                .Where(o => o.profile_id.Equals(user.profile_id))
                                .Where(o => o.status.ToUpper().Equals(AWAITING_SEND_ORDER.ToUpper()))
                                .Where(o => o.is_rebalance.Equals(false))
                                .ToList();
                            
                            if (orders.Count > 0)
                            {
                                Console.WriteLine($"{user.profile_id} -> {orders.Count}");
                                Console.WriteLine("");
                            }

                            bool chkOrderBuy = orders.Count == 0;
                            foreach (var order in orders)
                            {
                                listRefNo.Add(order.reference_no);
                                SetOrderAwaitingSendOrderModel(orderSend, wealthDB, order, effDate);
                            }

                            decimal net_sell_rebalance = 0M;
                            #region Set Order Rebalance Buy
                            List<order_fund> orderRebalanceBuys = wealthDB.order_fund.ToList()
                                .Where(o => o.profile_id.Equals(user.profile_id))
                                .Where(o => o.status.ToUpper().Equals(AWAITING_SEND_ORDER.ToUpper()))
                                .Where(o => o.is_rebalance.Equals(true))
                                .Where(o => o.action.Equals("buy"))
                                .ToList();

                            foreach (var order in orderRebalanceBuys)
                            {
                                order_fund orderSell = wealthDB.order_fund.ToList()
                                    .Where(o => o.profile_id.Equals(user.profile_id))
                                    .Where(o => o.is_rebalance.Equals(true))
                                    .Where(o => o.action.Equals("sell"))
                                    .Where(o => o.rebalance_no.Equals(order.rebalance_no))
                                    .FirstOrDefault();

                                if (orderSell.status.ToUpper() == AWAITING_SEND_ORDER.ToUpper())
                                {
                                    listRefNo.Add(orderSell.reference_no);
                                    SetOrderAwaitingSendOrderModel(orderSend, wealthDB, orderSell, effDate);
                                }
                                else if (orderSell.status.ToUpper() == COMPLETE.ToUpper())
                                {
                                    temp_order_sell temp_sell = _temp_order_sell.Where(t => t.reference_no == orderSell.reference_no).FirstOrDefault();
                                    
                                    Console.WriteLine($"{user.profile_id} -> Order Rebalance Sell Buy Status {temp_sell.is_buy}");

                                    if (temp_sell.is_buy == true)
                                    {

                                        Console.WriteLine($"{user.profile_id} ->  Gernerate order buy");
                                        Console.WriteLine("");
                                        Console.WriteLine("");

                                        #region Gernerate order buy
                                        listRefNo.Add(order.reference_no);

                                        List<trading> tradings = wealthDB.trading.ToList()
                                            .Where(t => t.profile_id.Equals(user.profile_id))
                                            .Where(t => t.trade_date.Equals(orderSell.effdate))
                                            .Where(t => t.action.ToUpper().Equals("sell".ToUpper()))
                                            .ToList();

                                        var net_sell = tradings.GroupBy(t => t.profile_id)
                                            .Select(t => t.Sum(tr => tr.amount))
                                            .FirstOrDefault();

                                        net_sell = Math.Round(net_sell.GetValueOrDefault(), 2);
                                        net_sell_rebalance += net_sell.GetValueOrDefault();
                                        Console.WriteLine($"{user.account_no} -> net sell = {net_sell}");
                                        List<order_fund_item> items = wealthDB.order_fund_item.ToList()
                                            .Where(i => i.reference_no.Equals(order.reference_no))
                                            .OrderBy(i => decimal.Parse(i.total_cost))
                                            .ToList();

                                        var expected_total_buy = items.GroupBy(t => t.reference_no)
                                            .Select(t => t.Sum(tr => decimal.Parse(tr.total_cost)))
                                            .FirstOrDefault();

                                        foreach (var item in items)
                                        {
                                            ut_outstanding outstanding = wealthDB.ut_outstanding.ToList()
                                                .Where(u => u.profile_id.Equals(user.profile_id))
                                                .Where(u => u.fundcode.Equals(item.fundcode))
                                                .Where(u => u.tradedate.Equals(wealthDB.ut_outstanding.Max(x => x.tradedate)))
                                                .FirstOrDefault();

                                            fund_information information = wealthDB.fund_information.ToList()
                                                .Where(i => i.fundcode.Equals(item.fundcode))
                                                .FirstOrDefault();

                                            decimal amount_condition = 0M;
                                            if (outstanding != null)
                                            {
                                                // next
                                                amount_condition = information.min_next.GetValueOrDefault();
                                            }
                                            else
                                            {
                                                // init
                                                amount_condition = information.min_initial.GetValueOrDefault();
                                            }
                                            decimal percent_buy = Math.Round((Math.Round(decimal.Parse(item.total_cost), 2) / Math.Round(expected_total_buy, 2)), 2);
                                            decimal final_percent_buy = Math.Round(percent_buy, 2);
                                            decimal final_buy = Math.Round((final_percent_buy * net_sell.GetValueOrDefault()), 2);

                                            BeforeSendOrder data = new BeforeSendOrder();
                                            data.percent_buy = Math.Round(percent_buy, 2);
                                            data.final_percent_buy = Math.Round(final_percent_buy, 2);
                                            data.amount_condition = Math.Round(amount_condition, 2);
                                            data.amount_buy = Math.Round(final_buy, 2);

                                            data.allotdate = DateTime.Now.ToString("yyyyMMdd");
                                            data.orderdate = effDate.ToString("yyyyMMdd");
                                            data.account = order.account_no;

                                            StringBuilder sb = new StringBuilder();
                                            sb.AppendLine("select am.* from fund_am am ");
                                            sb.AppendLine("  left join fund_information fi on fi.unique_id = am.unique_id ");
                                            sb.AppendFormat("  where fi.fundcode = '{0}' ", item.fundcode);

                                            fund_am fundAM = wealthDB.Database.SqlQuery<fund_am>(sb.ToString()).FirstOrDefault();

                                            string action = item.action;
                                            // hardcode fix type PC/RD
                                            data.type = "PC";
                                            data.amount = 0;
                                            data.fundcode = item.fundcode;
                                            data.amcode = fundAM.amcode;

                                            data.paytype = "60";

                                            orderSend.Add(data);

                                            ProgressBar(items.Count, items.IndexOf(item) + 1); // prgress
                                        }

                                        Console.WriteLine("");

                                        #endregion

                                    }

                                }
                            }
                            #endregion

                            #region Sum Order Duplicate
                            List<BeforeSendOrder> groupOrders = orderSend.GroupBy(o => new { o.account, o.fundcode, o.type })
                                .Select(s => new BeforeSendOrder()
                                {
                                    allotdate = s.FirstOrDefault().allotdate,
                                    orderdate = s.FirstOrDefault().orderdate,
                                    account = s.Key.account,
                                    type = s.FirstOrDefault().type,
                                    amount = s.Sum(p => p.amount),
                                    unit = s.Sum(p => p.unit),
                                    fundcode = s.Key.fundcode,
                                    amcode = s.FirstOrDefault().amcode,
                                    paytype = s.FirstOrDefault().paytype,
                                    
                                    percent_buy = s.Sum(p => p.percent_buy),
                                    final_percent_buy = s.Sum(p => p.final_percent_buy),
                                    amount_condition = s.Sum(p => p.amount_condition),
                                    amount_buy = s.Sum(p => p.amount_buy)
                                })
                                .ToList();
                            #endregion

                            #region Add Order Sell
                            // Add order rebalance sell
                            List<BeforeSendOrder> finalGroupOrdersSell = groupOrders.Where(o => o.type.ToLower() == "RD".ToLower()).ToList();
                            foreach (var item in finalGroupOrdersSell)
                            {
                                groupOrderBuys.Add(item);
                            }
                            #endregion

                            #region Calculate Min Buy
                            List<BeforeSendOrder> finalGroupOrders = CalculateMinBuy(groupOrders, net_sell_rebalance);
                            foreach (var item in finalGroupOrders)
                            {
                                item.amount = item.amount + item.amount_buy;
                                groupOrderBuys.Add(item);
                            }
                            #endregion
                        }


                        #region Send Mail
                        System.Threading.Thread.Sleep(2000);
                        Console.WriteLine($"item orider {groupOrderBuys.Count}");

                        if (groupOrderBuys.Count > 0)
                        {
                            #region Check Money
                            var buyTotal = groupOrderBuys.Where(g => g.type == "PC")
                                .GroupBy(o => new { o.account, o.type })
                                .Select(s => new { account = s.Key.account, type = s.Key.type, sum = s.Sum(p => p.amount) })
                                .ToList();

                            if (buyTotal.Count > 0)
                            {
                                foreach (var item in buyTotal)
                                {
                                    using (EFUnitOfWorkEService ef = new EFUnitOfWorkEService())
                                    {
                                        string q = "select [strfrontcustid], [strbackcustid], [cashbalance_bf] from [tbl_initial_creditline] where strbackcustid = '{0}' or strfrontcustid = '{0}'";
                                        q = string.Format(q, item.account);

                                        DataTable dt = ef.GetData(q);

                                        if (dt.Rows.Count > 0)
                                        {
                                            string strMoney = dt.Rows[0]["cashbalance_bf"].ToString();
                                            decimal currentMoney = 0;
                                            if (strMoney != string.Empty && strMoney != null)
                                            {
                                                currentMoney = decimal.Parse(strMoney);
                                            }
                                            System.Threading.Thread.Sleep(2000);
                                            Console.WriteLine($"account -> {item.account} buy = {item.sum}, currentMoney = {currentMoney}");
                                            // เช็คยอดเงินที่จะซื้อ หากซื้อมากกว่าเงินในบัญชีให้ทำการ adjust ยอดซื้อ
                                            if (item.sum > currentMoney)
                                            {
                                                decimal diff = item.sum - currentMoney;
                                                Console.WriteLine($"Diff = {diff}");
                                                // get max buy
                                                var maxBuy = groupOrderBuys.Where(g => g.account == item.account).OrderByDescending(x => x.amount).FirstOrDefault();
                                                if (maxBuy != null)
                                                {
                                                    foreach (var buy in groupOrderBuys)
                                                    {
                                                        if (buy.account == maxBuy.account && buy.fundcode == maxBuy.fundcode)
                                                        {
                                                            buy.amount = maxBuy.amount - diff;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    } 
                                }

                            }
                            #endregion

                            List<FinalOrderSendOrder> orderText = new List<FinalOrderSendOrder>();
                            List<FinalOrderSendOrderImport> orderTextImport = new List<FinalOrderSendOrderImport>();
                            LineService.lineNotify($"Send Order Total {groupOrderBuys.Count}");

                            List<fund_holiday> allfholiday = wealthDB.fund_holiday.ToList();

                            foreach (var item in groupOrderBuys)
                            {
                                //LineService.lineNotify($"{item.account} | {item.fundcode} | {item.type} | {item.amount}");

                                Console.WriteLine($"{item.fundcode} | {item.percent_buy} | {item.final_percent_buy} | {item.amount}");

                                userwinfo userwinfo = wealthDB.userwinfo.ToList()
                                    .Where(u => u.account_no.Equals(item.account))
                                    .Where(u => u.amcode.Equals(item.amcode))
                                    .FirstOrDefault();

                                if (item.type == "RD")
                                {
                                    Console.WriteLine($"{item.fundcode} | unit -> {item.unit} | {item.final_percent_buy} | {item.amount}");
                                }
                                FinalOrderSendOrderImport data = new FinalOrderSendOrderImport();
                                data.transaction_date = DateTime.Now.ToString("dd-MM-yyyy");
                                DateTime lastEffDate = GetEffDateFund(allfholiday, item.fundcode, effDate);

                                data.eff_date = lastEffDate.ToString("dd-MM-yyyy");

                                //data.transaction_date = item.allotdate;
                                //data.eff_date = item.orderdate;
                                data.trx_type = item.type;
                                data.amcode = item.amcode;
                                data.fundcode = item.fundcode;
                                data.account_no = item.account;

                                if (userwinfo != null)
                                {
                                    //data.unit_holder = userwinfo.unit_holder;
                                    data.cust_name = userwinfo.name;
                                    data.account_no = userwinfo.account_back;
                                }
                                else
                                {
                                    string acc = data.account_no.Substring(0, 6) + "-" + data.account_no.Substring(6 + "-".Length);

                                    data.account_no = acc;
                                    //data.unit_holder = $"DUMMY-{acc}";
                                    data.cust_name = $"บริษัทหลักทรัพย์หยวนต้า (ประเทศไทย) จำกัด เพื่อ {acc}";
                                }

                                if (data.trx_type.Equals("PC"))
                                {
                                    data.flag = "AMT";
                                    data.amount = String.Format("{0:0.00}", item.amount);
                                }
                                else
                                {
                                    data.flag = "UNIT";
                                    data.unit = String.Format("{0:0.0000}", item.unit);
                                }

                                data.paytype = "60";

                                orderTextImport.Add(data);

                                orderText.Add(new FinalOrderSendOrder()
                                {
                                    allotdate = item.allotdate,
                                    orderdate = item.orderdate,
                                    account = item.account,
                                    type = item.type,
                                    amount = String.Format("{0:n4}", item.amount),
                                    unit = String.Format("{0:n4}", item.unit),
                                    fundcode = item.fundcode,
                                    amcode = item.amcode,
                                    paytype = item.paytype
                                });
                            }

                            try
                            {
                                string SUBJECT_MAIL = $"Order My Wealth at {effDate.ToString("dd/MM/yyyy")}";
                                string SUBJECT_MAIL_DEV = $"Order My Wealth at {effDate.ToString("dd/MM/yyyy")}";
                                var wb = new XLWorkbook();
                                var ws = wb.Worksheets.Add("Order");

                                var wbip = new XLWorkbook();
                                var wsip = wbip.Worksheets.Add("Order");

                                // From a list of strings
                                var listOfHeader = new List<String[]>();
                                listOfHeader.Add(new String[] { "allotdate", "orderdate", "account", "type", "amcode", "fundcode", "unit", "amount", "paytype" });
                                var rangeWithStrings = ws.Cell(1, 1).InsertData(listOfHeader);
                                var rangeWithData = ws.Cell(2, 1).InsertData(orderText.AsEnumerable());
                                ws.Columns().AdjustToContents();
                                var filePath = $@"C:\wealth_log\{effDate.ToString("yyyy-MM-dd")}\" + string.Format("{0}-{1}.xlsx", "Wealth", DateTime.Now.ToString("yyyyMMdd-hhmmss"));
                                wb.SaveAs(filePath);

                                // File for import
                                var listOfHeaderImport = new List<String[]>();
                                listOfHeaderImport.Add(new String[] { "TransDate", "EffDate", "TrxType", "Account", "CustName", "AMCode", "FundCode", "Flag", "Amt", "UNIT", "PayType", "Omnibusflag", "Remark" });
                                var rangeWithStringImports = wsip.Cell(1, 1).InsertData(listOfHeaderImport);
                                var rangeWithDataImport = wsip.Cell(2, 1).InsertData(orderTextImport.AsEnumerable());
                                wsip.Columns().AdjustToContents();
                                var filePathImport = $@"C:\wealth_log\{effDate.ToString("yyyy-MM-dd")}\" + string.Format("{0}-{1}.xlsx", "Wealth", DateTime.Now.ToString("yyyyMMdd-hhmmss"));
                                wbip.SaveAs(filePathImport);


                                // send mail to selling agent                
                                List<string> filePaths = new List<string>();
                                //filePaths.Add(filePath); // Noting send order
                                filePaths.Add(filePathImport);
                                
                                Console.Write("Send Mail Dev");
                                System.Threading.Thread.Sleep(1000);
                                SendMail(SUBJECT_MAIL_DEV, "Nopparat.M@yuanta.co.th", TempleteMailOrder(strEffDate), filePaths, null, null);

                                Console.Write(".");
                                System.Threading.Thread.Sleep(1000);
                                Console.Write(".");
                                System.Threading.Thread.Sleep(1000);
                                Console.Write(".");
                                System.Threading.Thread.Sleep(2000);
                                Console.WriteLine("Send Mail Dev Success");

                                #region Send Mail To Agent     
                                Console.Write("Send Mail Marketing");
                                System.Threading.Thread.Sleep(1000);
                                List<string> cc = new List<string>();
                                cc.Add("wealthsvc@yuanta.co.th");
                                SendMail(SUBJECT_MAIL, "Esettlement@yuanta.co.th;Custodian@yuanta.co.th;Pinyo.M@yuanta.co.th", TempleteMailOrder(strEffDate), filePaths, null, cc);

                                Console.Write(".");
                                System.Threading.Thread.Sleep(1000);
                                Console.Write(".");
                                System.Threading.Thread.Sleep(1000);
                                Console.Write(".");
                                System.Threading.Thread.Sleep(2000);
                                Console.WriteLine("Send Mail Marketing Success");

                                ///// update status user_model_allocation (Awaiting buy/Awaiting sell) and order_fund (processing)
                                System.Threading.Thread.Sleep(2000);
                                Console.WriteLine($"Update Reference no total = {listRefNo.Count}");
                                foreach (var item in listRefNo)
                                {
                                    UpdateStatusSendOrderNew(effDate, item, orderText);
                                }
                                #endregion
                            }
                            catch (Exception ex)
                            {
                                LineService.lineNotify($"Error SendOrderAllotment = {ex.Message}");
                                Console.WriteLine($"Error SendOrderAllotment = {ex.Message}");
                                System.Threading.Thread.Sleep(2000);
                                logger.Error(ex, $"SendOrderAllotment: {ex.Message}");
                            }
                        }
                        else
                        {
                            LineService.lineNotify($"No data not send order to ut");
                            Console.WriteLine($"No data not send order to ut");
                            System.Threading.Thread.Sleep(2000);
                            logger.Info("No data not send order to ut");
                        }
                        #endregion

                    }
                    catch (Exception ex)
                    {
                        LineService.lineNotify($"Error API SendOrder, {ex.Message}");
                        Console.WriteLine($"Error API SendOrder, {ex.Message}");
                        System.Threading.Thread.Sleep(2000);

                        transaction.Rollback();
                        logger.Error(ex, $"API SendOrderNew MigrateDataRebalance, {ex.Message}");
                    }

                    transaction.Commit();
                }
            }
        }

        public static void UpdateStatusSendOrderNew(DateTime effdate, string refNo, List<FinalOrderSendOrder> orderText)
        {
            try
            {
                using (WealthAdvise_DevEntities context = new WealthAdvise_DevEntities())
                {
                    List<order_fund> orderFunds = context.order_fund.ToList()
                        .Where(orf => orf.status.ToLower().Equals("Awaiting send order".ToLower()) && orf.reference_no.Equals(refNo))
                        .ToList();

                    System.Threading.Thread.Sleep(2000);
                    Console.WriteLine($"Find order Awaiting send order: ref no = {refNo}, total {orderFunds.Count}");

                    if (orderFunds != null)
                    {
                        List<order_fund_item> _odf = context.order_fund_item.ToList();
                        List<order_fund_item> _order_fund_item = _odf.Where(i => orderFunds.Any(o => o.reference_no == i.reference_no)).ToList();
                        
                        List<fund_holiday> allfholiday = context.fund_holiday.ToList();
                        foreach (order_fund orderFund in orderFunds)
                        {
                            List<order_fund_item> order_items = _order_fund_item.ToList()
                                .Where(ofi =>
                                ofi.transition_status.ToLower() == "pending"
                                && ofi.reference_no == refNo
                                ).ToList();

                            List<order_fund_item> _export_status_item = order_items.Where(i =>
                            orderText.Any(o => o.account == orderFund.account_no && o.fundcode == i.fundcode)
                            ).ToList();

                            List<order_fund_item> _cancel_status_item = order_items.Where(i =>
                           !orderText.Any(o => o.account == orderFund.account_no && o.fundcode == i.fundcode)
                           ).ToList();

                            System.Threading.Thread.Sleep(2000);
                            Console.WriteLine($"Find order item Pending: ref no = {refNo}, total {order_items.Count}");
                            if (_export_status_item.Count > 0) Console.WriteLine($"Find order item Pending: ref no = {refNo}, export status total {_export_status_item.Count}");
                            if (_cancel_status_item.Count > 0) Console.WriteLine($"Find order item Pending: ref no = {refNo}, cancel status total {_cancel_status_item.Count}");

                            if (_export_status_item != null)
                            {
                                foreach (var exp in _export_status_item)
                                {
                                    DateTime lastEffDate = GetEffDateFund(allfholiday, exp.fundcode, effdate);
                                    exp.transition_status = "exported";
                                    exp.effdate = lastEffDate;

                                    var buy = orderText.Where(o => o.account.Substring(0,6) == orderFund.account_no.Substring(0, 6) && o.fundcode == exp.fundcode).FirstOrDefault();
                                    if (buy != null && exp.is_rebalance.GetValueOrDefault())
                                    {
                                        if (exp.transition_status == "buy")
                                        {
                                            exp.total_cost = buy.amount;
                                        }
                                    }
                                    
                                    context.SaveChanges();
                                    if (exp.action == "buy")
                                    {
                                        LineService.lineNotify($"{orderFund.account_no} | {exp.action} | {exp.fundcode} | {exp.total_cost} | {lastEffDate.ToString("dd/MM/yyyy", CultureInfo.GetCultureInfo("en-US"))}");
                                    }
                                    else
                                    {
                                        LineService.lineNotify($"{orderFund.account_no} | {exp.action} | {exp.fundcode} | {exp.expected_unit} | {lastEffDate.ToString("dd/MM/yyyy", CultureInfo.GetCultureInfo("en-US"))}");
                                    }

                                }
                                //_export_status_item.ForEach(ofi => { ofi.transition_status = "exported"; ofi.effdate = effdate; });
                                context.SaveChanges();
                            }

                            if (_cancel_status_item != null)
                            {
                                foreach (var ofi in _cancel_status_item)
                                {
                                    ofi.transition_status = "cancel";
                                    context.SaveChanges();
                                }
                            }

                            orderFund.effdate = effdate;
                        }

                        foreach (var orf in orderFunds)
                        {
                            orf.status = "processing";
                            orf.modified_date = DateTime.Now;
                            orf.effdate = effdate;
                            context.SaveChanges();
                        }
                        
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                LineService.lineNotify($"Error UpdateStatusSendOrder: {ex.Message}");
                logger.Error(ex, $"UpdateStatusSendOrder: {ex.Message}");
            }
        }

        static DateTime GetEffDateFund(List<fund_holiday> allfholiday, string fundcode, DateTime effdate)
        {
            fund_holiday fholiday = allfholiday.Where(h => h.fund_abbr_name == fundcode && h.holidaydate == effdate).FirstOrDefault();

            if (fholiday != null)
            {
                String strEffDate = GetEffDate(effdate);
                DateTime newEffDate = DateTime.ParseExact(strEffDate, "dd/MM/yyyy", CultureInfo.GetCultureInfo("en-US"));

                return GetEffDateFund(allfholiday, fundcode, newEffDate);
            }

            return effdate;
        }

        static List<BeforeSendOrder> CalculateMinBuy(List<BeforeSendOrder> buys, decimal net_sell)
        {
            int chk = 1;
            if (buys.Count > 0)
            {
                int totalChk = 0;
                while (chk > 0)
                {
                    totalChk++;
                    chk = 0;
                    bool firstChk = false;
                    foreach (var item in buys)
                    {
                        decimal net_amount_buy = Math.Round((Math.Round(item.amount, 2) + Math.Round(item.amount_buy, 2)), 2);

                        if (item.percent_buy > 0 && item.amount_condition > net_amount_buy && !firstChk)
                        {
                            chk = 1;
                            firstChk = true;

                            item.percent_buy = 0;
                            item.final_percent_buy = 0;
                            item.amount_buy = 0;
                            //break;
                        }
                        else
                        {
                            if (item.percent_buy > 0)
                            {
                                decimal sump = buys.Where(t => t.percent_buy > 0).Sum(t => t.percent_buy);

                                decimal final_percent_buy = Math.Round((Math.Round(item.percent_buy, 2) / Math.Round(sump, 2)), 6);
                                decimal final_buy = Math.Round((Math.Round(final_percent_buy, 6) * Math.Round(net_sell, 2)), 2);

                                item.final_percent_buy = Math.Round(final_percent_buy, 2);
                                item.amount_buy = Math.Round(final_buy, 2);

                                Console.WriteLine($"{item.fundcode} | {item.amount} + {item.amount_buy} = {item.amount + net_amount_buy}");
                            }
                        }
                    }
                }

                if (net_sell > 0)
                {
                    var totalAmountBuy = buys.GroupBy(t => t.account)
                                               .Select(t => t.Sum(tr => tr.amount_buy))
                                               .FirstOrDefault();

                    decimal calAmount = net_sell - totalAmountBuy;
                    buys = buys.OrderByDescending(o => o.amount_buy).ToList();
                    if (calAmount > 0)
                    {
                        // plus
                        buys[0].amount_buy = buys[0].amount_buy + calAmount;
                    }
                    else if (calAmount < 0)
                    {
                        // minus
                        buys[0].amount_buy = buys[0].amount_buy + calAmount;
                    }
                }
            }
            
            return buys.Where(item => (item.amount + item.amount_buy) > 0).ToList();
        }

        static void SetOrderAwaitingSendOrderModel(List<BeforeSendOrder> orderSend, WealthAdvise_DevEntities wealthDB, order_fund order, DateTime effDate)
        {
            List<order_fund_item> items = wealthDB.order_fund_item.ToList().Where(i => i.reference_no.Equals(order.reference_no)).ToList();

            foreach (var item in items)
            {
                BeforeSendOrder data = new BeforeSendOrder();
                data.allotdate = DateTime.Now.ToString("yyyyMMdd");
                data.orderdate = effDate.ToString("yyyyMMdd");
                data.account = order.account_no;

                StringBuilder sb = new StringBuilder();
                sb.AppendLine("select am.* from fund_am am ");
                sb.AppendLine("  left join fund_information fi on fi.unique_id = am.unique_id ");
                sb.AppendFormat("  where fi.fundcode = '{0}' ", item.fundcode);

                fund_am fundAM = wealthDB.Database.SqlQuery<fund_am>(sb.ToString()).FirstOrDefault();

                string action = item.action;
                // hardcode fix type PC/RD
                data.type = action.ToLower().Equals("buy") ? "PC" : "RD";

                if (data.type.Equals("PC"))
                {
                    data.amount = Convert.ToDecimal(item.total_cost);
                    //data.amount = item.total_cost;
                    //data.amount = String.Format("{0:n6}", Convert.ToDecimal(item.total_cost.ToString()));
                }
                else
                {
                    data.unit = Convert.ToDecimal(item.expected_unit);
                    //data.unit = item.expected_unit;
                    //data.unit = String.Format("{0:n6}", Convert.ToDecimal(item.expected_unit.ToString()));
                }

                data.fundcode = item.fundcode;
                data.amcode = fundAM.amcode;

                data.paytype = "60";

                orderSend.Add(data);
            }
        }

        private static string GetEffDate()
        {
            DataTable dtRes = new DataTable();
            string res = string.Empty;

            try
            {
                string now = DateTime.Now.ToString("dd/MM/yyyy", CultureInfo.GetCultureInfo("en-US"));
                StringBuilder str = new StringBuilder();
                str.Append("SELECT TOP 1 [Trade_Date], [Due_Date]");
                str.Append("  FROM [DUE_DATE]");
                str.AppendFormat("  where convert(date,Trade_Date,103) > convert(date,'{0}',103)", now);
                str.Append("  order by convert(date,Trade_Date,103)");

                using (EFUnitOfWorkWEBDB unitOfWork = new EFUnitOfWorkWEBDB())
                {
                    dtRes = unitOfWork.GetData(str.ToString());
                }

                DataTable dt = dtRes;

                if (dt != null && dt.Rows.Count > 0)
                {
                    res = dt.Rows[0]["Trade_Date"].ToString();
                }
                else
                {
                    logger.Info("No data");
                }
            }
            catch (Exception ex)
            {
                LineService.lineNotify($"Error GetEffDate: {ex.Message}");
                logger.Error(ex, $"GetEffDate: {ex.Message}");
            }

            return res;
        }

        private static string GetEffDate(DateTime dtEffDate)
        {
            DataTable dtRes = new DataTable();
            string res = string.Empty;

            try
            {
                string eff = dtEffDate.ToString("dd/MM/yyyy", CultureInfo.GetCultureInfo("en-US"));
                StringBuilder str = new StringBuilder();
                str.Append("SELECT TOP 1 [Trade_Date], [Due_Date]");
                str.Append("  FROM [DUE_DATE]");
                str.AppendFormat("  where convert(date,Trade_Date,103) > convert(date,'{0}',103)", eff);
                str.Append("  order by convert(date,Trade_Date,103)");

                using (EFUnitOfWorkWEBDB unitOfWork = new EFUnitOfWorkWEBDB())
                {
                    dtRes = unitOfWork.GetData(str.ToString());
                }

                DataTable dt = dtRes;

                if (dt != null && dt.Rows.Count > 0)
                {
                    res = dt.Rows[0]["Trade_Date"].ToString();
                }
                else
                {
                    logger.Info("No data");
                }
            }
            catch (Exception ex)
            {
                LineService.lineNotify($"Error GetEffDate: {ex.Message}");
                logger.Error(ex, $"GetEffDate: {ex.Message}");
            }

            return res;
        }

        public static void SendMail(string subject, string emailTo, AlternateView htmlbody, List<string> attachlist, List<string> emailBCC = null, List<string> emailCC = null)
        {
            LogManager.ThrowExceptions = true;
            SmtpSection section = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");

            string smtpAddress = section.Network.Host;
            int portNumber = section.Network.Port;
            bool enableSSL = false;

            string emailFrom = section.From;
            string password = "";

            //  string body = htmlbody;
            try
            {
                using (MailMessage mail = new MailMessage())
                {
                    mail.From = new MailAddress(emailFrom);
                    mail.Headers.Add("Disposition-Notification-To", section.From);
                    //mail.To.Add(emailTo);

                    foreach (var address in emailTo.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        mail.To.Add(address);
                    }

                    if (emailCC != null)
                        foreach (var cc in emailCC)
                        {
                            mail.CC.Add(cc);
                        }
                    if (emailBCC != null)
                        foreach (var bcc in emailBCC)
                        {
                            mail.Bcc.Add(bcc);
                        }

                    mail.Subject = subject;
                    mail.AlternateViews.Add(htmlbody);
                    mail.IsBodyHtml = true;

                    if (attachlist != null)
                    {
                        List<Stream> streams = new List<Stream>();

                        foreach (string file in attachlist)
                        {
                            Stream attachmentStream = File.OpenRead(file);
                            mail.Attachments.Add(new Attachment(attachmentStream, Path.GetFileName(file)));
                        }
                    }
                    using (SmtpClient smtp = new SmtpClient(smtpAddress, portNumber))
                    {
                        smtp.Credentials = new NetworkCredential(emailFrom, password);
                        smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

                        smtp.EnableSsl = enableSSL;
                        smtp.Send(mail);

                    }
                }
            }
            catch (SmtpFailedRecipientException ex)
            {
                LineService.lineNotify("Error Send Fail Email " + emailTo);
                throw new Exception("Send Fail Email " + emailTo, ex);
            }
            catch (Exception ex)
            {
                LineService.lineNotify("Error Send Fail Email " + emailTo);
                throw new Exception("Send Fail Email " + emailTo, ex);
            }
        }

        public static AlternateView TempleteMailOrder(string effDate)
        {
            string body = "Please find Attached document for place order at " + effDate + "." +
                "<br/>" +
                "<br/>" +
                "My wealth System";

            return AlternateView.CreateAlternateViewFromString(body, new System.Net.Mime.ContentType("text/html"));
        }

        private static bool CheckWorkingDay()
        {
            DataTable dtRes = new DataTable();
            bool chk = false;

            try
            {
                string now = DateTime.Now.ToString("dd/MM/yyyy", CultureInfo.GetCultureInfo("en-US"));
                string str = "";
                str += " SELECT *";
                str += " FROM   DUE_DATE";
                str += " WHERE  Due_Date = '" + now + "'";

                using (EFUnitOfWorkWEBDB unitOfWork = new EFUnitOfWorkWEBDB())
                {
                    dtRes = unitOfWork.GetData(str.ToString());
                }

                DataTable dt = dtRes;

                if (dt != null && dt.Rows.Count > 0)
                {
                    chk = true;
                }
                else
                {
                    chk = false;
                }
            }
            catch (Exception ex)
            {
                chk = false;
                LineService.lineNotify($"Error CheckWorkingDay, {ex.Message}");
                logger.Error(ex, $"CheckWorkingDay: {ex.Message}");
            }

            if (chk)
            {
                LineService.lineNotify($"Starting Process...");
            }
            else
            {
                LineService.lineNotify($"Holiday Not Start Process...");
            }


            return chk;
        }

        private static void ProgressBar(int totalLine, int countTotal)
        {
            var percent = calPercent(totalLine, countTotal);

            StringBuilder per = new StringBuilder(42);
            per.Append("                ");
            per.Append(percent.ToString());
            per.Append(" % Complete");
            per.Append(' ', 40 - per.Length);

            StringBuilder progress = new StringBuilder(42);
            progress.Append('[');
            progress.Append('#', (int.Parse(percent.ToString()) * 40 / 100));
            progress.Append('_', 41 - progress.Length);
            progress.Append(']');

            Console.CursorTop -= 1;
            Console.CursorLeft = 0;
            Console.WriteLine(progress.ToString());
            Console.CursorLeft = 0;
            Console.Write(per.ToString());
            System.Threading.Thread.Sleep(30);
        }

        private static double calPercent(int totalLine, int countTotal)
        {
            double cal1 = totalLine - countTotal;
            double cal2 = totalLine - cal1;
            double cal3 = cal2 / totalLine;
            double percent = Math.Round(cal3, 2) * 100;

            return percent;
        }

    }
}
