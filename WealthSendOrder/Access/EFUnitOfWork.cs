﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Reflection;

namespace WealthSendOrder.Access
{
    public partial class WealthContainer : DbContext
    {
        public WealthContainer(string environmentName)
            : base("name=WEALTH_DEVEntities") // change name from db web this mock data fund db
        {
        }
    }

    public class EFUnitOfRoboWealthDB : IDisposable
    {
        private static string ConnectionString = ConfigurationManager.ConnectionStrings["WEALTH_DEVEntities"].ToString();

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
        public DataTable GetData(string sql, SqlParameter[] param)
        {
            try
            {
                DataSet retVal = new DataSet();

                SqlConnection sqlConn = new SqlConnection(ConnectionString);
                SqlCommand cmdReport = new SqlCommand(sql, sqlConn);
                SqlDataAdapter daReport = new SqlDataAdapter(cmdReport);
                using (cmdReport)
                {
                    SqlParameter[] questionIdPrm = param;
                    cmdReport.CommandTimeout = int.MaxValue;
                    cmdReport.CommandType = CommandType.StoredProcedure;
                    foreach (SqlParameter item in param)
                    {
                        cmdReport.Parameters.Add(item);
                    }
                    daReport.Fill(retVal);
                }
                sqlConn.Close();

                return retVal.Tables[0];
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }

        }
        public DataTable GetDataStoreNonParam(string sql)
        {
            try
            {
                DataSet retVal = new DataSet();

                SqlConnection sqlConn = new SqlConnection(ConnectionString);
                SqlCommand cmdReport = new SqlCommand(sql, sqlConn);
                SqlDataAdapter daReport = new SqlDataAdapter(cmdReport);
                using (cmdReport)
                {
                    cmdReport.CommandTimeout = int.MaxValue;
                    cmdReport.CommandType = CommandType.StoredProcedure;

                    daReport.Fill(retVal);
                }
                sqlConn.Close();

                return retVal.Tables[0];
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }

        }
        public DataTable GetData(string sql)
        {
            try
            {
                DataSet retVal = new DataSet();
                SqlConnection sqlConn = new SqlConnection(ConnectionString);
                SqlCommand cmdReport = new SqlCommand(sql, sqlConn);
                SqlDataAdapter daReport = new SqlDataAdapter(cmdReport);
                using (SqlCommand cmd = new SqlCommand(sql, sqlConn))
                {
                    cmdReport.CommandType = CommandType.Text;
                    daReport.Fill(retVal);
                }
                sqlConn.Close();

                return retVal.Tables[0];
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }

        }
        public string ExecuteSqlCommand(string sql, SqlParameter[] param)
        {
            string result = string.Empty;
            try
            {
                using (SqlConnection con = new SqlConnection(ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(sql, con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandTimeout = int.MaxValue;

                        foreach (SqlParameter item in param)
                        {
                            cmd.Parameters.Add(item);
                        }

                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }

            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }
        public Int32 ExecuteSqlCommandDataTable(string sql, string param_name, DataTable dt)
        {
            Int32 _rows = 0;
            try
            {
                using (SqlConnection con = new SqlConnection(ConnectionString))
                {
                    //sqlcon as SqlConnection  
                    SqlCommand sqlcom = new SqlCommand(sql, con);
                    sqlcom.CommandType = CommandType.StoredProcedure;
                    sqlcom.Parameters.AddWithValue(param_name, dt);
                    //sqlcom.Parameters.Add(prmReturn);
                    con.Open();
                    _rows = sqlcom.ExecuteNonQuery();
                }
                return _rows;
            }
            catch (Exception ex)
            {
                string _ex = ex.Message;
                return _rows;
            }


        }
        public Int32 execCmdwithTransaction(string _sql)  // parameter
        {
            Int32 _rows = 0;
            try
            {
                DataTable dt = new DataTable();

                using (SqlConnection con = new SqlConnection(ConnectionString))
                {
                    con.Open();
                    SqlTransaction transaction;
                    transaction = con.BeginTransaction("WealthTransaction");
                    //con.BeginTransaction();
                    try
                    {
                        using (SqlCommand cmd = new SqlCommand(_sql, con))
                        {
                            cmd.CommandType = CommandType.Text;
                            cmd.Connection = con;
                            cmd.Transaction = transaction;
                            _rows = cmd.ExecuteNonQuery();
                            transaction.Commit();
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        con.Close();

                        throw new ApplicationException(ex.Message);
                    }
                    con.Close();
                }
                return _rows;
            }
            catch (Exception ex)
            {
                string _ex = ex.Message;
                return _rows;
            }

        }

        public void testBulkInsert<T>(string table, IEnumerable<T> list)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();

                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connection, SqlBulkCopyOptions.Default, transaction))
                {
                    bulkCopy.BatchSize = 100;
                    
                    try
                    {
                        DataTable dt = CreateDataTable(list);
                        bulkCopy.DestinationTableName = table;
                        bulkCopy.WriteToServer(dt);
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        connection.Close();
                        throw new ApplicationException(ex.Message);
                    }
                }

                transaction.Commit();
            }

        }

        public void testBulkInsertDT(string table, DataTable dt)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();

                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connection, SqlBulkCopyOptions.Default, transaction))
                {
                    bulkCopy.BatchSize = 100;

                    try
                    {
                        bulkCopy.DestinationTableName = table;
                        bulkCopy.WriteToServer(dt);
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        connection.Close();
                        throw new ApplicationException(ex.Message);
                    }
                }

                transaction.Commit();
            }

        }

        public static DataTable CreateDataTable<T>(IEnumerable<T> list)
        {
            Type type = typeof(T);
            var properties = type.GetProperties();

            DataTable dataTable = new DataTable();
            foreach (PropertyInfo info in properties)
            {
                dataTable.Columns.Add(new DataColumn(info.Name, Nullable.GetUnderlyingType(info.PropertyType) ?? info.PropertyType));
            }
            
            foreach (T item in list)
            {
                DataRow row = dataTable.NewRow();
                foreach (PropertyInfo prop in properties)
                {
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                }                    
                dataTable.Rows.Add(row);
            }

            return dataTable;
        }
    }

    public partial class SSOContainer : DbContext
    {
        public SSOContainer(string environmentName)
            : base("name=EService_NEWDEVEntities")
        {
        }
    }

    public class EFUnitOfWorkEService : IDisposable
    {
        string ConnectionString = ConfigurationManager.ConnectionStrings["EService_NEWDEVEntities"].ToString();

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
        public string ExecuteSqlCommand(string sql, SqlParameter[] param)
        {
            string result = string.Empty;
            try
            {
                //SqlConnection sqlConn = new SqlConnection(ConnectionString);
                //SqlCommand cmd = new SqlCommand(sql, sqlConn);
                //using (cmd)
                //{
                //    SqlParameter[] questionIdPrm = param;
                //    cmd.CommandTimeout = int.MaxValue;
                //    cmd.CommandType = CommandType.StoredProcedure;
                //    foreach (SqlParameter item in param)
                //    {
                //        cmd.Parameters.Add(item);
                //    }
                //    cmd.ExecuteNonQuery();
                //}
                //sqlConn.Close();

                using (SqlConnection con = new SqlConnection(ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(sql, con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandTimeout = int.MaxValue;

                        foreach (SqlParameter item in param)
                        {
                            cmd.Parameters.Add(item);
                        }

                        //cmd.Parameters.Add("@FirstName", SqlDbType.VarChar).Value = txtFirstName.Text;
                        //cmd.Parameters.Add("@LastName", SqlDbType.VarChar).Value = txtLastName.Text;

                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }

            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }
        public DataTable GetData(string sql, SqlParameter[] param)
        {
            try
            {
                DataSet retVal = new DataSet();

                SqlConnection sqlConn = new SqlConnection(ConnectionString);
                SqlCommand cmdReport = new SqlCommand(sql, sqlConn);
                SqlDataAdapter daReport = new SqlDataAdapter(cmdReport);
                using (cmdReport)
                {
                    SqlParameter[] questionIdPrm = param;
                    cmdReport.CommandTimeout = int.MaxValue;
                    cmdReport.CommandType = CommandType.StoredProcedure;
                    foreach (SqlParameter item in param)
                    {
                        cmdReport.Parameters.Add(item);
                    }
                    daReport.Fill(retVal);
                }
                sqlConn.Close();

                return retVal.Tables[0];
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }

        }
        public DataTable GetData(string sql)
        {
            try
            {
                DataSet retVal = new DataSet();

                SqlConnection sqlConn = new SqlConnection(ConnectionString);
                SqlCommand cmdReport = new SqlCommand(sql, sqlConn);
                SqlDataAdapter daReport = new SqlDataAdapter(cmdReport);
                using (cmdReport)
                {
                    cmdReport.CommandTimeout = int.MaxValue;
                    cmdReport.CommandType = CommandType.Text;
                    daReport.Fill(retVal);
                }
                sqlConn.Close();

                return retVal.Tables[0];
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }

        }
        public Int32 execCmdwithTransaction(string _sql)  // parameter
        {
            Int32 _rows = 0;
            try
            {
                DataTable dt = new DataTable();

                using (SqlConnection con = new SqlConnection(ConnectionString))
                {
                    con.Open();
                    SqlTransaction transaction;
                    transaction = con.BeginTransaction("WealthTransaction");
                    //con.BeginTransaction();
                    try
                    {
                        using (SqlCommand cmd = new SqlCommand(_sql, con))
                        {
                            cmd.CommandType = CommandType.Text;
                            cmd.Connection = con;
                            cmd.Transaction = transaction;
                            _rows = cmd.ExecuteNonQuery();
                            transaction.Commit();
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        con.Close();

                        throw new ApplicationException(ex.Message);
                    }
                    con.Close();
                }
                return _rows;
            }
            catch (Exception ex)
            {
                string _ex = ex.Message;
                return _rows;
            }

        }
        public void xx()
        {

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                // Create the command and set its properties.
                // SqlCommand command = new SqlCommand();
                SqlCommand command = new SqlCommand("Proc_name", connection);

                command.CommandType = CommandType.StoredProcedure;

                // Add the input parameters and set the properties.
                SqlParameter parameter1 = new SqlParameter();
                parameter1.ParameterName = "@Param1";
                parameter1.SqlDbType = SqlDbType.NVarChar;
                parameter1.Direction = ParameterDirection.Input;
                parameter1.Value = "param1";

                SqlParameter parameter2 = new SqlParameter();
                parameter2.ParameterName = "@Param2";
                parameter2.SqlDbType = SqlDbType.NVarChar;
                parameter2.Direction = ParameterDirection.Input;
                parameter2.Value = "param2";

                // Same for params 3 and 4...


                // Add the parameter to the Parameters collection. 
                command.Parameters.Add(parameter1);
                command.Parameters.Add(parameter2);


                //command.Parameters.Add(parameter3);
                //command.Parameters.Add(parameter4);


                // Open the connection and execute the reader.
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                reader.Close();
            }
        }
        protected string AddArrayParameters(SqlCommand sqlCommand, string[,] array, string paramName)
        {
            var parameters = new string[array.Length];
            for (int i = 0; i < array.Length; i++)
            {
                parameters[i] = string.Format("@{0}{1}", paramName, i);
                sqlCommand.Parameters.AddWithValue(parameters[i], array[i, i]);
            }

            return string.Join(", ", parameters);
        }
    }

    public class EFUnitOfWorkWEBDB : IDisposable
    {
        string ConnectionString = ConfigurationManager.ConnectionStrings["WEB_DEVEntities"].ToString();

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
        public string ExecuteSqlCommand(string sql, SqlParameter[] param)
        {
            string result = string.Empty;
            try
            {
                using (SqlConnection con = new SqlConnection(ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(sql, con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandTimeout = int.MaxValue;

                        foreach (SqlParameter item in param)
                        {
                            cmd.Parameters.Add(item);
                        }

                        //cmd.Parameters.Add("@FirstName", SqlDbType.VarChar).Value = txtFirstName.Text;
                        //cmd.Parameters.Add("@LastName", SqlDbType.VarChar).Value = txtLastName.Text;

                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }

            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }
        public DataTable GetData(string sql, SqlParameter[] param)
        {
            try
            {
                DataSet retVal = new DataSet();

                SqlConnection sqlConn = new SqlConnection(ConnectionString);
                SqlCommand cmdReport = new SqlCommand(sql, sqlConn);
                SqlDataAdapter daReport = new SqlDataAdapter(cmdReport);
                using (cmdReport)
                {
                    SqlParameter[] questionIdPrm = param;
                    cmdReport.CommandTimeout = int.MaxValue;
                    cmdReport.CommandType = CommandType.StoredProcedure;
                    foreach (SqlParameter item in param)
                    {
                        cmdReport.Parameters.Add(item);
                    }
                    daReport.Fill(retVal);
                }
                sqlConn.Close();

                return retVal.Tables[0];
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }

        }
        public DataTable GetData(string sql)
        {
            try
            {
                DataSet retVal = new DataSet();

                SqlConnection sqlConn = new SqlConnection(ConnectionString);
                SqlCommand cmdReport = new SqlCommand(sql, sqlConn);
                SqlDataAdapter daReport = new SqlDataAdapter(cmdReport);
                using (cmdReport)
                {
                    cmdReport.CommandTimeout = int.MaxValue;
                    cmdReport.CommandType = CommandType.Text;
                    daReport.Fill(retVal);
                }
                sqlConn.Close();

                return retVal.Tables[0];
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }

        }
        public Int32 execCmdwithTransaction(string _sql)  // parameter
        {
            Int32 _rows = 0;
            try
            {
                DataTable dt = new DataTable();

                using (SqlConnection con = new SqlConnection(ConnectionString))
                {
                    con.Open();
                    SqlTransaction transaction;
                    transaction = con.BeginTransaction("WealthTransaction");
                    //con.BeginTransaction();
                    try
                    {
                        using (SqlCommand cmd = new SqlCommand(_sql, con))
                        {
                            cmd.CommandType = CommandType.Text;
                            cmd.Connection = con;
                            cmd.Transaction = transaction;
                            _rows = cmd.ExecuteNonQuery();
                            transaction.Commit();
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        con.Close();

                        throw new ApplicationException(ex.Message);
                    }
                    con.Close();
                }
                return _rows;
            }
            catch (Exception ex)
            {
                string _ex = ex.Message;
                return _rows;
            }

        }
        public void xx()
        {

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                // Create the command and set its properties.
                // SqlCommand command = new SqlCommand();
                SqlCommand command = new SqlCommand("Proc_name", connection);

                command.CommandType = CommandType.StoredProcedure;

                // Add the input parameters and set the properties.
                SqlParameter parameter1 = new SqlParameter();
                parameter1.ParameterName = "@Param1";
                parameter1.SqlDbType = SqlDbType.NVarChar;
                parameter1.Direction = ParameterDirection.Input;
                parameter1.Value = "param1";

                SqlParameter parameter2 = new SqlParameter();
                parameter2.ParameterName = "@Param2";
                parameter2.SqlDbType = SqlDbType.NVarChar;
                parameter2.Direction = ParameterDirection.Input;
                parameter2.Value = "param2";

                // Same for params 3 and 4...


                // Add the parameter to the Parameters collection. 
                command.Parameters.Add(parameter1);
                command.Parameters.Add(parameter2);


                //command.Parameters.Add(parameter3);
                //command.Parameters.Add(parameter4);


                // Open the connection and execute the reader.
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                reader.Close();
            }
        }
        protected string AddArrayParameters(SqlCommand sqlCommand, string[,] array, string paramName)
        {
            var parameters = new string[array.Length];
            for (int i = 0; i < array.Length; i++)
            {
                parameters[i] = string.Format("@{0}{1}", paramName, i);
                sqlCommand.Parameters.AddWithValue(parameters[i], array[i, i]);
            }

            return string.Join(", ", parameters);
        }
    }

}
