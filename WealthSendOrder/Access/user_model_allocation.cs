//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WealthSendOrder.Access
{
    using System;
    using System.Collections.Generic;
    
    public partial class user_model_allocation
    {
        public int id { get; set; }
        public string profile_id { get; set; }
        public string account_no { get; set; }
        public int model_id { get; set; }
        public Nullable<System.DateTime> created_date { get; set; }
        public Nullable<System.DateTime> modified_date { get; set; }
        public bool is_active { get; set; }
        public string status { get; set; }
        public string initial_budget { get; set; }
    
        public virtual model model { get; set; }
    }
}
