//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WealthCutofftime.Access
{
    using System;
    using System.Collections.Generic;
    
    public partial class investment_performance
    {
        public int id { get; set; }
        public string profile_id { get; set; }
        public string account_no { get; set; }
        public Nullable<int> user_model_allocation_id { get; set; }
        public Nullable<System.DateTime> created_date { get; set; }
        public Nullable<decimal> daily_return { get; set; }
        public string daily_property_value { get; set; }
        public string thai_equity_value { get; set; }
        public string foreign_equity_value { get; set; }
        public string goverment_bond_value { get; set; }
        public string corporate_bond_value { get; set; }
        public string property_value { get; set; }
        public string commodity_value { get; set; }
        public string cash_value { get; set; }
        public string base_value { get; set; }
        public string best_value { get; set; }
        public string worst_value { get; set; }
    }
}
