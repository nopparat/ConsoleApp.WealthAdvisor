//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WealthCutofftime.Access
{
    using System;
    using System.Collections.Generic;
    
    public partial class log_allocation
    {
        public int id { get; set; }
        public string profile_id { get; set; }
        public string account_no { get; set; }
        public Nullable<int> user_model_allocation_id { get; set; }
        public Nullable<int> temp_user_model_allocation_id { get; set; }
        public string fundcode { get; set; }
        public Nullable<decimal> percent_investment { get; set; }
        public Nullable<decimal> total_cost { get; set; }
        public Nullable<System.DateTime> allocation_date { get; set; }
    }
}
