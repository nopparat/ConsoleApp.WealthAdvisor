//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WealthCutofftime.Access
{
    using System;
    using System.Collections.Generic;
    
    public partial class log_adjoutstanding
    {
        public int id { get; set; }
        public string account_no { get; set; }
        public string fundcode { get; set; }
        public string flag { get; set; }
        public string amount { get; set; }
        public string amount_adj { get; set; }
        public string amount_net { get; set; }
        public Nullable<System.DateTime> created_date { get; set; }
        public string unit { get; set; }
        public string unit_adj { get; set; }
        public string unit_net { get; set; }
    }
}
