﻿using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WealthCutofftime.Access;

namespace WealthCutofftime
{
    class Program
    {
        private static Logger logger = LogManager.GetLogger("databaseLogger");

        //private static string PENDING = "pending";
        private static string AWAITING_SEND_ORDER = "Awaiting send order";

        static void Main(string[] args)
        {
            if (CheckWorkingDay())
            {
                //LineService.lineNotify($"Start Cutofftime Wealth Process");
                Console.WriteLine("********* Start Cutofftime Wealth Process *********");

                MigrateDataRebalancePending();

                ChangeStatusOrder();

                MigrateDataCash();

                MigrateDataOutStanding();

                for (int i = 0; i <= 30; i++)
                {
                    string as_of_date = DateTime.Now.AddDays(-i).ToString("yyyyMMdd", CultureInfo.GetCultureInfo("en-US"));

                    CalulateReportHoldingAndInvestment(as_of_date, i);
                }

                //Console.WriteLine("For Insert");
                //MigrateDailyCalculateChart();

                Console.WriteLine("");
                DailyCalculateChart();

                //Console.WriteLine("For Update");
                //MigrateDailyCalculateChart();
                Console.WriteLine("********* End Cutofftime Wealth Process *********");
            }
        }

        static void MigrateDailyCalculateChart()
        {
            Console.WriteLine("********* Start MigrateDailyCalculateChart *********");
            Console.WriteLine("");
            Console.WriteLine("");
            using (WealthAdvise_DevEntities db = new WealthAdvise_DevEntities())
            {
                string query = @"
select o.* 
  from order_fund o,
  (select profile_id, min(convert(date, created_date)) created_date from order_fund group by profile_id) om
 where om.profile_id = o.profile_id and convert(date, om.created_date) = convert(date, o.created_date) and o.status != 'cancel'
 order by om.created_date
";
                List<order_fund> results = db.Database.SqlQuery<order_fund>(query).ToList();

                foreach (var item in results)
                {
                    if (item.effdate.HasValue)
                    {
                        for (DateTime date = item.created_date.GetValueOrDefault().Date; item.effdate.GetValueOrDefault().Date.AddDays(1).CompareTo(date) > 0; date = date.AddDays(1.0))
                        {
                            // logic here
                            daily_chart_report daily_chart = db.daily_chart_report.Where(r => r.profile_id == item.profile_id && r.report_date == date.Date).FirstOrDefault();
                            user_model_allocation user = db.user_model_allocation.Where(u => u.profile_id == item.profile_id).FirstOrDefault();

                            if (daily_chart != null)
                            {
                                // update
                                if (user != null)
                                {
                                    decimal budget = decimal.Parse(user.initial_budget);
                                    daily_chart.property_value_1month = budget;
                                    daily_chart.property_value_6month = budget;
                                    daily_chart.property_value_1year = budget;
                                    daily_chart.property_value_max = budget;
                                    daily_chart.return_value_1month = 100;
                                    daily_chart.return_value_6month = 100;
                                    daily_chart.return_value_1year = 100;
                                    daily_chart.return_value_max = 100;
                                    db.SaveChanges();
                                }
                            }
                            else
                            {
                                // insert
                                if (user != null)
                                {
                                    if (user.profile_id != "144301")
                                    {
                                        decimal budget = decimal.Parse(user.initial_budget);
                                        daily_chart = new daily_chart_report();
                                        daily_chart.profile_id = user.profile_id;
                                        daily_chart.account_no = user.account_no;
                                        daily_chart.report_date = date.Date;
                                        daily_chart.property_value_1month = budget;
                                        daily_chart.property_value_6month = budget;
                                        daily_chart.property_value_1year = budget;
                                        daily_chart.property_value_max = budget;
                                        daily_chart.return_value_1month = 100;
                                        daily_chart.return_value_6month = 100;
                                        daily_chart.return_value_1year = 100;
                                        daily_chart.return_value_max = 100;
                                        daily_chart.base_value = decimal.Parse(user.model.base_case);
                                        daily_chart.best_value = decimal.Parse(user.model.best_case);
                                        daily_chart.worst_value = decimal.Parse(user.model.worst_case);
                                        db.daily_chart_report.Add(daily_chart);
                                        db.SaveChanges();
                                    }
                                }
                            }

                        }
                    }

                    ProgressBar(results.Count, results.IndexOf(item) + 1);
                }
            }
            Console.WriteLine("");
        }

        static void MigrateDataRebalancePending()
        {
            Console.WriteLine("********* Start MigrateDataRebalancePending *********");
            //System.Threading.Thread.Sleep(2000);

            using (WealthAdvise_DevEntities wealthDB = new WealthAdvise_DevEntities())
            {
                using (DbContextTransaction transaction = wealthDB.Database.BeginTransaction())
                {
                    try
                    {
                        List<user_model_allocation> users = wealthDB.user_model_allocation.ToList().Where(u => !u.account_no.Equals(string.Empty) && u.is_active.Equals(true)).ToList();
                        var _order_fund = wealthDB.order_fund.ToList();
                        var _temp_user_model_allocation = wealthDB.temp_user_model_allocation.ToList();
                        var _model = wealthDB.model.ToList();

                        foreach (var user in users)
                        {
                            //System.Threading.Thread.Sleep(2000);
                            Console.WriteLine($"user account_no = {user.account_no}");
                            List<order_fund> orderPendings = _order_fund.ToList()
                                .Where(o => o.profile_id.Equals(user.profile_id))
                                .Where(o => o.status.Equals("pending"))
                                .Where(o => o.is_rebalance.Equals(true))
                                .ToList();

                            if (orderPendings.Count > 0)
                            {
                                temp_user_model_allocation temp_user = _temp_user_model_allocation.Where(t => t.profile_id.Equals(user.profile_id)).FirstOrDefault();
                                int count_is_change_model = 0;
                                foreach (var order in orderPendings)
                                {
                                    //System.Threading.Thread.Sleep(2000);
                                    Console.WriteLine($"{order.action} | {order.profile_id} | {order.reference_no} | {order.rebalance_no} | {order.is_change_model}");
                                    // check change model
                                    if (order.is_change_model.GetValueOrDefault(true))
                                    {
                                        // set user_model and init
                                        model model = _model.Where(m => m.id.Equals(temp_user.model_id)).FirstOrDefault();

                                        wealthDB.log_change_model.Add(new log_change_model()
                                        {
                                            profile_id = user.profile_id,
                                            account_no = user.account_no,
                                            user_model_allocation_id_from = user.id,
                                            model_id_from = user.model_id,
                                            model_id_to = temp_user.model_id.Value,
                                            is_selected_exchange_rate_risk = model.is_selected_exchange_rate_risk,
                                            change_date = temp_user.created_date
                                        });

                                        user.model_id = temp_user.model_id.Value;
                                        user.modified_date = DateTime.Now;

                                        wealthDB.SaveChanges();
                                        count_is_change_model++;
                                    }
                                }

                                List<temp_rebalance> rebalances = wealthDB.temp_rebalance.ToList().Where(r => r.profile_id.Equals(user.profile_id) && r.new_percent_allocatiom > 0).ToList();

                                List<log_allocation> new_log_allocation = new List<log_allocation>();
                                List<initial_allocation> new_initial_allocation = new List<initial_allocation>();
                                var _fund_information = wealthDB.fund_information.ToList();
                                var _initial_allocation = wealthDB.initial_allocation.ToList();

                                foreach (var rebalance in rebalances)
                                {
                                    log_allocation log_Allocation = new log_allocation();
                                    log_Allocation.profile_id = user.profile_id;
                                    log_Allocation.account_no = user.account_no;
                                    log_Allocation.user_model_allocation_id = user.id;
                                    log_Allocation.temp_user_model_allocation_id = (temp_user == null ? 0 : temp_user.id);
                                    log_Allocation.fundcode = rebalance.fundcode;
                                    log_Allocation.percent_investment = rebalance.new_percent_allocatiom;
                                    log_Allocation.total_cost = rebalance.net_amount_weight;
                                    log_Allocation.allocation_date = DateTime.Now;
                                    new_log_allocation.Add(log_Allocation);

                                    fund_information information = _fund_information.Where(f => f.fundcode.Equals(rebalance.fundcode)).FirstOrDefault();
                                    new_initial_allocation.Add(new initial_allocation()
                                    {
                                        profile_id = user.profile_id,
                                        account_no = user.account_no,
                                        user_model_allocation_id = user.id,
                                        asset_type_id = rebalance.asset_type_id.Value,
                                        proj_id = information.proj_id,
                                        percent_invest = rebalance.new_percent_allocatiom.ToString(),
                                        amount = rebalance.net_amount_weight.ToString(),
                                        created_date = DateTime.Now,
                                        modified_date = DateTime.Now,
                                        expected_unit = rebalance.net_unit_weight.ToString(),
                                        fundcode = rebalance.fundcode
                                    });
                                }

                                List<initial_allocation> remove = _initial_allocation.Where(i => i.profile_id.Equals(user.profile_id)).ToList();
                                wealthDB.initial_allocation.RemoveRange(remove);
                                wealthDB.SaveChanges();

                                wealthDB.log_allocation.AddRange(new_log_allocation);
                                wealthDB.initial_allocation.AddRange(new_initial_allocation);
                                wealthDB.SaveChanges();
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        LineService.lineNotify($"Error API MigrateDataRebalance, {ex.Message}");
                        transaction.Rollback();
                        logger.Error(ex, $"API MigrateDataRebalance, {ex.Message}");
                    }

                    transaction.Commit();
                }
            }
        }

        static void ChangeStatusOrder()
        {
            try
            {
                Console.WriteLine("********* Start ChangeStatusOrder *********");
                //System.Threading.Thread.Sleep(2000);

                using (WealthAdvise_DevEntities context = new WealthAdvise_DevEntities())
                {
                    List<order_fund> order_funds = context.order_fund.ToList().Where(orf => orf.status.ToLower().Equals("pending")).ToList();

                    logger.Info($"Running Change status pending to Awaiting send order total = {order_funds.Count}");

                    //System.Threading.Thread.Sleep(2000);
                    Console.WriteLine($"Running Change status pending to Awaiting send order total = {order_funds.Count}");

                    if (order_funds != null)
                    {
                        order_funds.ForEach(orf => orf.status = AWAITING_SEND_ORDER);
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                LineService.lineNotify($"Error Update StatusAwaitingSendOrder, {ex.Message}");
                logger.Error(ex, $"Update StatusAwaitingSendOrder: {ex.Message}");
            }
        }

        private static void MigrateDataCash()
        {
            Console.WriteLine("********* Start MigrateDataCash *********");
            //System.Threading.Thread.Sleep(2000);

            WealthAdvise_DevEntities db = new WealthAdvise_DevEntities();

            try
            {
                var sb = new StringBuilder();

                // TODO new logic 13/02/2019 12.56 Check amount temp_due
                sb.Append("	select order_item.profile_id, order_item.account_no, sum(order_item.sum_total_cost) sum_total_cost from (  ");
                sb.Append("		  SELECT orf.profile_id, orf.account_no, sum(cast(orfi.total_cost as decimal(18,2))) sum_total_cost  ");
                sb.Append("		    FROM order_fund_item orfi  ");
                sb.Append("		    left join order_fund orf on orf.reference_no = orfi.reference_no  ");
                sb.Append("		    where orfi.transition_status != 'complete'  ");
                sb.Append("			and orfi.transition_status != 'cancel'  ");
                sb.Append("		    and orf.status != 'complete'  ");
                sb.Append("		    and orf.status != 'cancel'  ");
                sb.Append("		    and orfi.action = 'buy'  ");
                sb.Append("			and orf.is_rebalance = '0'  ");
                sb.Append("		    group by orf.profile_id, orf.account_no  ");
                sb.Append("			 ");
                sb.Append("		union all  ");
                sb.Append("		     ");
                sb.Append("		  SELECT data.profile_id, data.account_no, cast(sum((cast(data.expected_unit as decimal(18,4)) * cast(data.nav as decimal(18,4)))) as decimal(18,2)) sum_total_cost from  ");
                sb.Append("		  (SELECT orf.profile_id, orf.account_no, cast(orfi.expected_unit as decimal(18,4)) expected_unit, (select top 1 nav from ut_fund_nav where fundcode = orfi.fundcode and as_of_date <= GETDATE() order by as_of_date desc) nav  ");
                sb.Append("		    FROM order_fund_item orfi  ");
                sb.Append("		    left join order_fund orf on orf.reference_no = orfi.reference_no  ");
                sb.Append("		    where orfi.transition_status != 'complete'  ");
                sb.Append("			and orfi.transition_status != 'cancel' ");
                sb.Append("		    and orf.status != 'complete'  ");
                sb.Append("		    and orf.status != 'cancel'  ");
                sb.Append("		    and orfi.action = 'sell'  ");
                sb.Append("			and orf.is_rebalance = '0') data  ");
                sb.Append("		  group by data.profile_id, data.account_no  ");
                sb.Append("			  ");
                sb.Append("		union all  ");
                //sb.Append("		  /** is_buy = false, sum all temp due **/ ");
                sb.Append("		  select rb_sell.profile_id, rb_sell.account_no, SUM(cast(td.amount as decimal(18,2))) sum_total_cost  ");
                sb.Append("			from temp_order_sell tos ");
                sb.Append("			, (select * from temp_due) td ");
                sb.Append("			, (select * from order_fund) rb_sell ");
                sb.Append("			, (select * from order_fund_item) irb_sell ");
                sb.Append("			where rb_sell.[status] = 'complete' ");
                sb.Append("					and tos.is_buy = '0' ");
                sb.Append("					and tos.account_no = td.account_no ");
                sb.Append("					and td.trade_date = irb_sell.effdate  ");
                sb.Append("					and td.account_no = rb_sell.account_no  ");
                sb.Append("					and td.fundcode = irb_sell.fundcode ");
                sb.Append("					and tos.account_no = rb_sell.account_no  ");
                sb.Append("					and tos.reference_no = rb_sell.reference_no ");
                sb.Append("					and irb_sell.reference_no = rb_sell.reference_no ");
                sb.Append("			group by	rb_sell.profile_id, rb_sell.account_no ");
                sb.Append("		union all 	 ");
                //sb.Append("		  /** is_buy = true, getdate < effdate use sum amount temp_due **/ ");
                sb.Append("			select rb_sell.profile_id, rb_sell.account_no, SUM(cast(td.amount as decimal(18,2))) sum_total_cost  ");
                sb.Append("				from temp_order_sell tos ");
                sb.Append("				, (select * from temp_due) td ");
                sb.Append("				, (select * from order_fund) rb_sell ");
                sb.Append("				, (select * from order_fund_item) irb_sell ");
                sb.Append("				where rb_sell.[status] = 'complete' ");
                sb.Append("						and tos.is_buy = '1' ");
                sb.Append("						and CAST(GETDATE() as date) < CAST(tos.eff_buy_date as date) ");
                sb.Append("						and tos.account_no = td.account_no ");
                sb.Append("						and td.trade_date = irb_sell.effdate  ");
                sb.Append("						and td.account_no = rb_sell.account_no  ");
                sb.Append("						and td.fundcode = irb_sell.fundcode ");
                sb.Append("						and tos.account_no = rb_sell.account_no  ");
                sb.Append("						and tos.reference_no = rb_sell.reference_no ");
                sb.Append("						and irb_sell.reference_no = rb_sell.reference_no ");
                sb.Append("				group by	rb_sell.profile_id, rb_sell.account_no ");
                sb.Append("		union all 	 ");
                //sb.Append("		/** is_buy = true, getdate >= effdate use order buy **/ ");
                sb.Append("		  SELECT orf.profile_id, orf.account_no, sum(cast(orfi.total_cost as decimal(18,2))) sum_total_cost  ");
                sb.Append("		    FROM order_fund_item orfi  ");
                sb.Append("		    left join order_fund orf on orf.reference_no = orfi.reference_no ");
                sb.Append("			, (select * from temp_order_sell) t ");
                sb.Append("			, (select * from order_fund) rb_sell ");
                sb.Append("		    where orfi.transition_status != 'complete'  ");
                sb.Append("			and orfi.transition_status != 'cancel' ");
                sb.Append("		    and orf.status != 'complete'  ");
                sb.Append("		    and orf.status != 'cancel'  ");
                sb.Append("		    and orfi.action = 'buy'  ");
                sb.Append("			and orf.is_rebalance = '1'  ");
                sb.Append("			and (select status from order_fund where rebalance_no = orf.rebalance_no and action = 'sell') = 'complete'  ");
                sb.Append("			and (rb_sell.rebalance_no = orf.rebalance_no) ");
                sb.Append("			and (t.account_no = orf.account_no and t.reference_no = rb_sell.reference_no and t.is_buy = '1' and CAST(GETDATE() as date) >= CAST(t.eff_buy_date as date)) ");
                sb.Append("		    group by orf.profile_id, orf.account_no ");
                sb.Append("  ");
                sb.Append("		union all  ");
                sb.Append("			  ");
                sb.Append("		SELECT data.profile_id, data.account_no, cast(sum((cast(data.expected_unit as decimal(18,4)) * cast(data.nav as decimal(18,4)))) as decimal(18,2)) sum_total_cost from  ");
                sb.Append("		  (SELECT orf.profile_id, orf.account_no, cast(orfi.expected_unit as decimal(18,4)) expected_unit, (select top 1 nav from ut_fund_nav where fundcode = orfi.fundcode and as_of_date <= GETDATE() order by as_of_date desc) nav  ");
                sb.Append("		    FROM order_fund_item orfi  ");
                sb.Append("		    left join order_fund orf on orf.reference_no = orfi.reference_no  ");
                sb.Append("		    where orfi.transition_status != 'complete'  ");
                sb.Append("			and orfi.transition_status != 'cancel' ");
                sb.Append("		    and orf.status != 'complete'  ");
                sb.Append("		    and orf.status != 'cancel'  ");
                sb.Append("		    and orfi.action = 'sell'  ");
                sb.Append("			and orf.is_rebalance = '1') data  ");
                sb.Append("		  group by data.profile_id, data.account_no  ");
                sb.Append("			  ");
                sb.Append("	 ) order_item  ");
                sb.Append("	 group by order_item.profile_id, order_item.account_no  ");
                sb.Append("	 order by order_item.profile_id  ");

                string tradeDate = GetTradeDate();
                DateTime as_of_date = DateTime.ParseExact(tradeDate, "dd/MM/yyyy", CultureInfo.GetCultureInfo("en-US"));
                //DateTime as_of_date = DateTime.Now.Date;
                var order_items = db.Database.SqlQuery<SummaryTotalCost>(sb.ToString()).ToList();

                //System.Threading.Thread.Sleep(2000);
                Console.WriteLine($"Order Not Complete Count: {order_items.Count}");

                List<report_cash> newReportCash = new List<report_cash>();
                var _wdividend = db.wdividend.ToList();

                Console.WriteLine($"");
                Console.WriteLine($"");
                Console.WriteLine($"======> MigrateDataCash");
                Console.WriteLine("");
                Console.WriteLine("");

                foreach (var item in order_items)
                {
                    var sumTotal = _wdividend
                                    .Where(w => w.is_buy.Equals(false))
                                    .Where(w => w.profile_id.Equals(item.profile_id))
                                    .GroupBy(w => w.profile_id)
                                    .Select(w => w.Sum(x => x.amount))
                                    .FirstOrDefault();

                    report_cash _Cash = new report_cash();
                    _Cash.profile_id = item.profile_id;
                    _Cash.account_no = item.account_no;
                    _Cash.report_date = as_of_date;
                    _Cash.amount = item.sum_total_cost + sumTotal.GetValueOrDefault();

                    //System.Threading.Thread.Sleep(2000);
                    //Console.WriteLine($"Report Cash: {tradeDate} {item.profile_id}, cash = {_Cash.amount}");

                    newReportCash.Add(_Cash);

                    ProgressBar(order_items.Count, order_items.IndexOf(item) + 1);
                }

                try
                {
                    List<report_cash> removeReportCash = db.report_cash.ToList().Where(r => r.report_date.Equals(as_of_date)).ToList();

                    if (removeReportCash.Count > 0)
                    {
                        Console.WriteLine("report_cash != null remove old data");
                        db.report_cash.RemoveRange(removeReportCash);
                        db.SaveChanges();
                    }

                    db.report_cash.AddRange(newReportCash);
                    db.SaveChanges();

                }
                catch (Exception ex)
                {
                    LineService.lineNotify($"Error occurred, {ex.Message}");
                    Console.WriteLine($"Error occurred, {ex.Message}");
                    logger.Info(ex, $"SaveReportCash: {ex.Message}");
                }

                Console.WriteLine($"order_items count {order_items.Count}");
            }
            catch (Exception ex)
            {
                LineService.lineNotify($"Error MigrateDataCash, {ex.Message}");
                Console.WriteLine($"MigrateDataCash: {ex.Message}");
            }


            Console.WriteLine("======================= MigrateDataCash End =======================");
        }

        private static void MigrateDataOutStanding()
        {
            Console.WriteLine("********* Start MigrateDataOutStanding *********");
            //System.Threading.Thread.Sleep(2000);

            using (WealthAdvise_DevEntities context = new WealthAdvise_DevEntities())
            {

                using (DbContextTransaction transaction = context.Database.BeginTransaction())
                {

                    try
                    {
                        string tradeDate = GetTradeDate();
                        DateTime effDate = DateTime.ParseExact(tradeDate, "dd/MM/yyyy", CultureInfo.GetCultureInfo("en-US"));

                        #region new condition
                        // TODO Change logic update outstanding
                        List<order_fund> _orders = context.order_fund.Where(o => o.status == "processing" && o.action.ToLower() == "sell").ToList();
                        List<order_fund_item> _order_items = context.order_fund_item.ToList()
                                            .Where(i =>
                                                _orders.Any(o => o.reference_no == i.reference_no)
                                                && i.transition_status == "exported"
                                                ).ToList();

                        List<ut_outstanding> _outstandings = context.ut_outstanding.ToList();
                        List<log_adjoutstanding> _log_adjoutstandings = context.log_adjoutstanding.ToList();
                        foreach (var item in _order_items)
                        {

                            var _order = _orders.Where(o => o.reference_no == item.reference_no).FirstOrDefault();
                            var chkAdj = _log_adjoutstandings.Where(l =>
                                l.account_no == _order.account_no
                                && l.fundcode == item.fundcode
                                && l.created_date == DateTime.Now.Date
                            ).Count();

                            if (chkAdj == 0)
                            {
                                List<ut_outstanding> outstandings = _outstandings.ToList()
                                .Where(o =>
                                o.tradedate == effDate
                                && o.fundcode == item.fundcode
                                && o.account_no == _order.account_no
                                )
                                .ToList();

                                List<log_adjoutstanding> adjoutstandings = new List<log_adjoutstanding>();
                                foreach (var _out in outstandings)
                                {
                                    decimal unit = decimal.Parse(_out.unit) - decimal.Parse(item.expected_unit);
                                    decimal amount = decimal.Parse(_out.unit) * _out.nav.GetValueOrDefault(0M);

                                    adjoutstandings.Add(new log_adjoutstanding()
                                    {
                                        account_no = _order.account_no,
                                        fundcode = item.fundcode,
                                        flag = "M",
                                        amount = _out.amount,
                                        amount_adj = item.total_cost,
                                        amount_net = amount.ToString("0.000000"),
                                        unit = _out.unit,
                                        unit_adj = item.expected_unit,
                                        unit_net = unit.ToString("0.000000"),
                                        created_date = DateTime.Now.Date
                                    });


                                    _out.unit = unit.ToString("0.000000");
                                    _out.amount = amount.ToString("0.000000");
                                    context.SaveChanges();
                                    Console.WriteLine($"adjust outstanding .{outstandings.IndexOf(_out)} _out | {_out.tradedate} | {_out.fundcode} | {_out.unit} | {_out.amount}");
                                }

                                if (outstandings.Count > 0)
                                {
                                    context.log_adjoutstanding.AddRange(adjoutstandings);
                                    context.SaveChanges();
                                }
                            }
                        }
                        #endregion

                    }
                    catch (Exception ex)
                    {
                        LineService.lineNotify($"Error SaveReportHolding, {ex.Message}");
                        transaction.Rollback();
                        Console.WriteLine("Error occurred.");
                        logger.Info(ex, $"SaveReportHolding: {ex.Message}");
                    }


                    transaction.Commit();
                }
            }
        }

        private static void CalulateReportHoldingAndInvestment(string as_of_date, int index)
        {
            List<OutstandingModel> outstandings = GetOutstandingNav(as_of_date);
            WealthAdvise_DevEntities db = new WealthAdvise_DevEntities();
            if (outstandings.Count > 0)
            {
                string profile_id = string.Empty;

                List<ReportHoldingCustomerModel> reportList = new List<ReportHoldingCustomerModel>();

                ReportHoldingCustomerModel reportHolding = null;
                List<ReportHoldingItemModel> holdingList = null;
                foreach (OutstandingModel outstanding in outstandings)
                {
                    if (!profile_id.Equals(outstanding.profile_id))
                    {
                        if (!profile_id.Equals(string.Empty))
                        {
                            reportHolding.reportHoldingItems = holdingList;
                            reportList.Add(reportHolding);
                        }

                        profile_id = outstanding.profile_id;
                        reportHolding = new ReportHoldingCustomerModel();
                        reportHolding.profile_id = outstanding.profile_id;
                        reportHolding.account_no = outstanding.account_no;
                        reportHolding.sumport = 0M;
                        reportHolding.sumport_property_value_t0 = 0M;
                        reportHolding.sumport_property_value_t_1 = 0M;
                        reportHolding.user_model_allocation_id = outstanding.user_model_allocation_id;
                        reportHolding.tradedate = DateTime.Now.AddDays(-index).ToString("yyyy-MM-dd", CultureInfo.GetCultureInfo("en-US"));
                        reportHolding.trade_date = DateTime.Now.AddDays(-index).Date;

                        holdingList = new List<ReportHoldingItemModel>();
                    }

                    ReportHoldingItemModel holding = new ReportHoldingItemModel();

                    holding.account_no = outstanding.account_no;
                    holding.user_model_allocation_id = outstanding.user_model_allocation_id;

                    holding.percent_holding = 0M;
                    holding.quantity = decimal.Parse(outstanding.unit);
                    holding.market_price = decimal.Parse(outstanding.navt0);

                    if (holding.quantity != 0.0M && decimal.Parse(outstanding.amount) != 0.0M)
                    {
                        holding.avg_purchase_price = Math.Round((decimal.Parse(outstanding.amount) / holding.quantity), 4);
                        holding.cumulative_change_value = Math.Round(((holding.quantity * holding.market_price) - (holding.quantity * holding.avg_purchase_price)), 4);

                        //TODO P'Chin Change calculate 09/11/2018
                        holding.percent_last_change = (((holding.quantity * holding.market_price) - decimal.Parse(outstanding.amount)) * 100) / decimal.Parse(outstanding.amount);
                    }


                    //TODO P'Pin
                    //holding.percent_last_change = Math.Round(((decimal.Parse(outstanding.navt0) / decimal.Parse(outstanding.navt_1)) - 1), 4);

                    holding.base_value = Math.Round((holding.quantity * holding.market_price), 4);

                    holding.fundcode = outstanding.fundcode;
                    holding.profile_id = outstanding.profile_id;
                    holding.tradedate = DateTime.Now.AddDays(-index).ToString("yyyy-MM-dd", CultureInfo.GetCultureInfo("en-US"));

                    holding.property_value_t0 = Math.Round((holding.quantity * decimal.Parse(outstanding.navt0)), 4);
                    holding.property_value_t_1 = Math.Round((holding.quantity * decimal.Parse(outstanding.navt_1)), 4);
                    holding.asset_type_id = outstanding.asset_type_id;

                    holdingList.Add(holding);

                    if (profile_id.Equals(outstanding.profile_id))
                    {
                        reportHolding.sumport += holding.base_value;
                        reportHolding.sumport_property_value_t0 += holding.property_value_t0;
                        reportHolding.sumport_property_value_t_1 += holding.property_value_t_1;
                    }
                    //Console.WriteLine("Count: " + outstandings.Count + " == " + outstandings.IndexOf(outstanding));

                    if (outstandings.Count.Equals(outstandings.IndexOf(outstanding) + 1))
                    {
                        reportHolding.reportHoldingItems = holdingList;
                        reportList.Add(reportHolding);
                        //Console.WriteLine("reportHolding sumport = " + reportHolding.sumport);
                    }

                }

                // calculate percent_holding
                if (reportList.Count > 0)
                {

                    List<report_holding> reportEntityList = new List<report_holding>();
                    List<investment_performance> investmentPerformanceEntityList = new List<investment_performance>();
                    foreach (ReportHoldingCustomerModel report in reportList)
                    {
                        Console.WriteLine($"");
                        Console.WriteLine($"");
                        Console.WriteLine($"[{reportList.IndexOf(report) + 1}/{reportList.Count}] ======> ReportHolding at {as_of_date} profile = {report.profile_id}");
                        Console.WriteLine("");
                        Console.WriteLine("");

                        decimal daily_property_value = 0M;

                        report_cash cash = db.report_cash.ToList().Where(r => r.report_date.Equals(report.trade_date) && r.profile_id.Equals(report.profile_id)).FirstOrDefault();

                        var log = cash != null ? cash.amount.ToString() : "no cash";

                        //System.Threading.Thread.Sleep(1000);
                        //Console.WriteLine($"report.profile_id, {report.profile_id} report.trade_date: {report.trade_date},  {log}");
                        if (cash != null)
                        {
                            daily_property_value = Math.Round((report.sumport_property_value_t0 + cash.amount.GetValueOrDefault(0m)), 4);
                        }
                        else
                        {
                            daily_property_value = report.sumport_property_value_t0;
                        }

                        investment_performance investment = new investment_performance();
                        investment.profile_id = report.profile_id;
                        investment.account_no = report.account_no;
                        investment.user_model_allocation_id = int.Parse(report.user_model_allocation_id);
                        investment.created_date = DateTime.Parse(report.tradedate);
                        investment.daily_property_value = Math.Round(daily_property_value, 4).ToString("0.00");
                        decimal daily_return = 0;
                        if (report.sumport_property_value_t0 == 0 && report.sumport_property_value_t_1 == 0)
                        {

                        }
                        else
                        {
                            daily_return = Math.Round(((report.sumport_property_value_t0 / report.sumport_property_value_t_1) - 1), 4);
                        }

                        investment.daily_return = daily_return;
                        investment.thai_equity_value = "0";
                        investment.foreign_equity_value = "0";
                        investment.goverment_bond_value = "0";
                        investment.corporate_bond_value = "0";
                        investment.property_value = "0";
                        investment.commodity_value = "0";
                        investment.cash_value = "0";

                        foreach (ReportHoldingItemModel item in report.reportHoldingItems)
                        {
                            if (item.base_value != 0 && report.sumport != 0)
                            {
                                item.percent_holding = Math.Round(((item.base_value / report.sumport) * 100), 4);
                            }
                            else
                            {
                                item.percent_holding = 0;
                            }


                            report_holding reportEnt = new report_holding();
                            reportEnt.profile_id = item.profile_id;
                            reportEnt.account_no = item.account_no;
                            reportEnt.percent_holding = Math.Round(item.percent_holding, 4).ToString();
                            reportEnt.quantity = item.quantity.ToString("0.000000");
                            reportEnt.avg_purchase_price = Math.Round(item.avg_purchase_price, 4).ToString();
                            reportEnt.market_price = Math.Round(item.market_price, 4).ToString();
                            reportEnt.percent_last_change = Math.Round(item.percent_last_change, 4).ToString();
                            reportEnt.cumulative_change_value = Math.Round(item.cumulative_change_value, 4).ToString();
                            reportEnt.base_value = Math.Round(item.base_value, 4).ToString();
                            reportEnt.created_date = DateTime.Parse(item.tradedate);

                            reportEnt.fundcode = item.fundcode;
                            reportEntityList.Add(reportEnt);


                            if ("1".Equals(item.asset_type_id))
                            {
                                decimal value = Math.Round((decimal.Parse(investment.thai_equity_value) + item.base_value), 4);
                                investment.thai_equity_value = Math.Round(value, 4).ToString();
                            }
                            else if ("2".Equals(item.asset_type_id))
                            {
                                decimal value = Math.Round(decimal.Parse(investment.foreign_equity_value) + item.base_value, 4);
                                investment.foreign_equity_value = Math.Round(value, 4).ToString();
                            }
                            else if ("3".Equals(item.asset_type_id))
                            {
                                decimal value = Math.Round(decimal.Parse(investment.goverment_bond_value) + item.base_value, 4);
                                investment.goverment_bond_value = Math.Round(value, 4).ToString();
                            }
                            else if ("4".Equals(item.asset_type_id))
                            {
                                decimal value = Math.Round(decimal.Parse(investment.corporate_bond_value) + item.base_value, 4);
                                investment.corporate_bond_value = Math.Round(value, 4).ToString();
                            }
                            else if ("5".Equals(item.asset_type_id))
                            {
                                decimal value = Math.Round(decimal.Parse(investment.property_value) + item.base_value, 4);
                                investment.property_value = Math.Round(value, 4).ToString();
                            }
                            else if ("6".Equals(item.asset_type_id))
                            {
                                decimal value = Math.Round(decimal.Parse(investment.commodity_value) + item.base_value, 4);
                                investment.commodity_value = Math.Round(value, 4).ToString();
                            }
                            else if ("7".Equals(item.asset_type_id))
                            {
                                decimal value = Math.Round(decimal.Parse(investment.cash_value) + item.base_value, 4);
                                investment.cash_value = Math.Round(value, 4).ToString();
                            }

                            ProgressBar(report.reportHoldingItems.Count, report.reportHoldingItems.IndexOf(item) + 1);
                        }

                        investmentPerformanceEntityList.Add(investment);
                    }

                    SaveReportHolding(reportEntityList);
                    SaveInvestmentPerformance(investmentPerformanceEntityList);
                }
            }
        }

        private static void DailyCalculateChart()
        {
            Console.WriteLine("");
            Console.WriteLine("********* Start DailyCalculateChart *********");
            //System.Threading.Thread.Sleep(2000);

            using (WealthAdvise_DevEntities context = new WealthAdvise_DevEntities())
            {
                List<user_model_allocation> user_Models = context.user_model_allocation.ToList()
                                                            .Where(u => u.is_active.Equals(true))
                                                            .Where(u => u.status.Equals("Active"))
                                                            .ToList();

                string chart_report_1month = DateTime.Now.AddMonths(-1).ToString("yyyy-MM-dd", CultureInfo.GetCultureInfo("en-US"));
                string chart_report_6month = DateTime.Now.AddMonths(-6).ToString("yyyy-MM-dd", CultureInfo.GetCultureInfo("en-US"));
                string chart_report_1year = DateTime.Now.AddYears(-1).ToString("yyyy-MM-dd", CultureInfo.GetCultureInfo("en-US"));

                foreach (var _user in user_Models)
                {
                    Console.WriteLine($"");
                    Console.WriteLine($"");
                    Console.WriteLine($"[{user_Models.IndexOf(_user) + 1}/{user_Models.Count}] ======> DailyCalculateChart profile = {_user.profile_id}");
                    Console.WriteLine("");
                    Console.WriteLine("");

                    decimal base_value = Math.Round(100 * 1 + decimal.Parse(_user.model.base_case), 4);
                    decimal best_value = Math.Round(100 * 1 + decimal.Parse(_user.model.best_case), 4);
                    decimal worst_value = Math.Round(100 * 1 + decimal.Parse(_user.model.worst_case), 4);

                    List<investment_performance> investmentPerformances = context.investment_performance.ToList()
                                                                            .Where(i => i.profile_id.Equals(_user.profile_id))
                                                                            .OrderBy(i => i.created_date)
                                                                            .ToList();

                    List<daily_chart_report> dailyChartReports = new List<daily_chart_report>();

                    decimal return_value_1month = 0;
                    decimal return_value_6month = 0;
                    decimal return_value_1year = 0;
                    decimal return_value_max = 100;
                    bool is_set_return_value_1month = false;
                    bool is_set_return_value_6month = false;
                    bool is_set_return_value_1year = false;

                    foreach (var _investP in investmentPerformances)
                    {
                        daily_chart_report _Report = new daily_chart_report();
                        _Report.profile_id = _investP.profile_id;
                        _Report.account_no = _investP.account_no;
                        _Report.report_date = _investP.created_date;

                        // check first data more than date
                        if (investmentPerformances.IndexOf(_investP) == 0)
                        {
                            DateTime dateTime_1month = DateTime.Parse(chart_report_1month);
                            DateTime dateTime_6month = DateTime.Parse(chart_report_6month);
                            DateTime dateTime_1year = DateTime.Parse(chart_report_1year);

                            if (!is_set_return_value_1month)
                            {
                                if (dateTime_1month <= _investP.created_date)
                                {
                                    return_value_1month = 100;
                                    is_set_return_value_1month = true;
                                }
                            }

                            if (!is_set_return_value_6month)
                            {
                                if (dateTime_6month <= _investP.created_date)
                                {
                                    return_value_6month = 100;
                                    is_set_return_value_6month = true;
                                }
                            }

                            if (!is_set_return_value_1year)
                            {
                                if (dateTime_1year <= _investP.created_date)
                                {
                                    return_value_1year = 100;
                                    is_set_return_value_1year = true;
                                }
                            }
                        }
                        else
                        {
                            DateTime dateTime_1month = DateTime.Parse(chart_report_1month);
                            DateTime dateTime_6month = DateTime.Parse(chart_report_6month);
                            DateTime dateTime_1year = DateTime.Parse(chart_report_1year);

                            if (!is_set_return_value_1month)
                            {
                                if (dateTime_1month <= _investP.created_date)
                                {
                                    return_value_1month = 100;
                                    is_set_return_value_1month = true;
                                }
                            }
                            else
                            {
                                // check first data more than date
                                return_value_1month = Math.Round(return_value_1month * (1 + _investP.daily_return.GetValueOrDefault(0m)), 4);
                            }

                            if (!is_set_return_value_6month)
                            {
                                if (dateTime_6month <= _investP.created_date)
                                {
                                    return_value_6month = 100;
                                    is_set_return_value_6month = true;
                                }
                            }
                            else
                            {
                                // check first data more than date
                                return_value_6month = Math.Round(return_value_6month * (1 + _investP.daily_return.GetValueOrDefault(0m)), 4);
                            }

                            if (!is_set_return_value_1year)
                            {
                                if (dateTime_1year <= _investP.created_date)
                                {
                                    return_value_1year = 100;
                                    is_set_return_value_1year = true;
                                }
                            }
                            else
                            {
                                // check first data more than date
                                return_value_1year = Math.Round(return_value_1year * (1 + _investP.daily_return.GetValueOrDefault(0m)), 4);
                            }

                            return_value_max = Math.Round(return_value_max * (1 + _investP.daily_return.GetValueOrDefault(0m)), 4);
                        }

                        _Report.return_value_1month = return_value_1month;
                        _Report.return_value_6month = return_value_6month;
                        _Report.return_value_1year = return_value_1year;
                        _Report.return_value_max = return_value_max;


                        _Report.property_value_1month = decimal.Parse(_investP.daily_property_value);
                        _Report.property_value_6month = decimal.Parse(_investP.daily_property_value);
                        _Report.property_value_1year = decimal.Parse(_investP.daily_property_value);
                        _Report.property_value_max = decimal.Parse(_investP.daily_property_value);

                        _Report.best_value = best_value;
                        _Report.base_value = base_value;
                        _Report.worst_value = worst_value;

                        dailyChartReports.Add(_Report);
                        //Console.WriteLine($"index {investmentPerformances.IndexOf(_investP)} -> {chart_report_1month} | {chart_report_6month} | {chart_report_1year}");
                        ProgressBar(investmentPerformances.Count, investmentPerformances.IndexOf(_investP) + 1);
                    }

                    SaveDailyChartReport(dailyChartReports);
                }

                Console.WriteLine("");
            }
        }

        private static string GetTradeDate()
        {
            DataTable dtRes = new DataTable();
            string res = string.Empty;

            try
            {
                string now = DateTime.Now.ToString("dd/MM/yyyy", CultureInfo.GetCultureInfo("en-US"));
                StringBuilder str = new StringBuilder();
                str.Append("SELECT TOP 1 [Trade_Date], [Due_Date]");
                str.Append("  FROM [DUE_DATE]");
                str.AppendFormat("  where convert(date,Trade_Date,103) < convert(date,'{0}',103)", now);
                str.Append("  order by convert(date,Trade_Date,103) desc");

                using (EFUnitOfWorkWEBDB unitOfWork = new EFUnitOfWorkWEBDB())
                {
                    dtRes = unitOfWork.GetData(str.ToString());
                }

                DataTable dt = dtRes;

                if (dt != null && dt.Rows.Count > 0)
                {
                    res = dt.Rows[0]["Trade_Date"].ToString();
                }
                else
                {
                    logger.Info("No data");
                }
            }
            catch (Exception ex)
            {
                LineService.lineNotify($"Error GetEffDate, {ex.Message}");
                logger.Error(ex, $"GetEffDate: {ex.Message}");
            }

            return res;
        }

        private static List<OutstandingModel> GetOutstandingNav(string as_of_date)
        {
            List<OutstandingModel> listResult = new List<OutstandingModel>();

            DataTable dtRes = new DataTable();
            string res = string.Empty;

            try
            {

                StringBuilder str = new StringBuilder();
                str.Append("        select ut.*, f_nav.*, uma.id user_model_allocation_id, mf.asset_type_id from ut_outstanding ut");
                str.Append("            left join (");
                str.Append("                select nav.fundcode, nav.nav navt0, nav.nav navt_1");
                str.Append("                from[ut_fund_nav] nav, (select max(a.as_of_date) maxdate, a.fundcode");
                str.Append("                from[ut_fund_nav] a");
                str.AppendFormat("          where a.as_of_date <= '{0}'", as_of_date);
                str.Append("                group by a.fundcode");
                str.AppendFormat("          having max(a.as_of_date) < '{0}') as maxdate", as_of_date);
                str.Append("        where nav.fundcode = maxdate.fundcode");
                str.Append("        and nav.as_of_date = maxdate.maxdate");
                str.Append("        union all");
                str.Append("        select a.fundcode, sum(a.navt0) navt0, sum(a.navt_1) navt_1");
                str.Append("        from(select nav.fundcode, nav.nav navt0, 0 navt_1");
                str.Append("            from[ut_fund_nav] nav, (SELECT a.fundcode, max(a.as_of_date) as_of_date");
                str.Append("                FROM[ut_fund_nav] a");
                str.AppendFormat("          where a.as_of_date <= '{0}'", as_of_date);
                str.Append("                group by a.fundcode");
                str.AppendFormat("          having max(a.as_of_date) = '{0}') maxdate", as_of_date);
                str.Append("            where nav.fundcode = maxdate.fundcode");
                str.Append("            and nav.as_of_date = maxdate.as_of_date");
                str.Append("        union all");
                str.Append("        select nav.fundcode, 0 navt0, nav.nav navt_1");
                str.Append("        from[ut_fund_nav] nav, (SELECT a.fundcode, max(a.as_of_date) as_of_date");
                str.Append("            FROM[ut_fund_nav] a, (SELECT max(as_of_date) as_of_date, fundcode");
                str.Append("                FROM[ut_fund_nav]");
                str.AppendFormat("          where as_of_date <= '{0}'", as_of_date);
                str.Append("                group by fundcode");
                str.AppendFormat("          having max(as_of_date) = '{0}') b", as_of_date);
                str.Append("            where a.fundcode = b.fundcode");
                str.Append("            and a.as_of_date < b.as_of_date");
                str.Append("            group by a.fundcode) t_1");
                str.Append("        where nav.fundcode = t_1.fundcode");
                str.Append("        and nav.as_of_date = t_1.as_of_date) a");
                str.Append("        group by a.fundcode");
                str.Append("        ) f_nav on f_nav.fundcode = ut.fundcode");
                str.Append("        left join user_model_allocation uma on ut.profile_id = uma.profile_id");
                str.Append("        left join model_fund mf on mf.model_id = uma.model_id and mf.fundcode = f_nav.fundcode");
                str.AppendFormat("  where ut.fundcode = f_nav.fundcode and ut.tradedate = '{0}'", as_of_date);
                str.Append("        and ut.profile_id = uma.profile_id and uma.is_active = '1'");
                str.Append("        order by ut.account_no, mf.asset_type_id");


                using (EFUnitOfRoboWealthDB unitOfWork = new EFUnitOfRoboWealthDB())
                {
                    dtRes = unitOfWork.GetData(str.ToString());
                }
                DataTable dt = dtRes;

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        OutstandingModel data = new OutstandingModel();

                        data.tradedate = dr["tradedate"].ToString();
                        data.profile_id = dr["profile_id"].ToString();
                        data.account_no = dr["account_no"].ToString();
                        data.fundcode = dr["fundcode"].ToString();
                        data.unit = dr["unit"].ToString();
                        data.amount = dr["amount"].ToString();
                        data.nav = dr["nav"].ToString();
                        data.created_date = dr["created_date"].ToString();
                        data.navt0 = dr["navt0"].ToString();
                        data.navt_1 = dr["navt_1"].ToString();
                        data.user_model_allocation_id = dr["user_model_allocation_id"].ToString();
                        data.asset_type_id = dr["asset_type_id"].ToString();

                        //Console.WriteLine(data.created_date + " " + data.account_no + " " + data.fundcode + " " + data.nav + " = (" + data.navt0 + "  -  " + data.navt_1 + ")");
                        //Console.WriteLine("user_model_allocation_id :: " + data.user_model_allocation_id);

                        listResult.Add(data);
                    }
                }
                else
                {
                    logger.Info("No data");
                }
            }
            catch (Exception ex)
            {
                LineService.lineNotify($"Error GetOutstandingNav, {ex.Message}");
                res = ex.Message;
                logger.Error(ex, $"API GetOutstandingNav: {ex.Message}");
            }

            return listResult;
        }

        private static void SaveReportHolding(List<report_holding> reportEntityList)
        {
            if (reportEntityList.Count > 0)
            {
                //Console.WriteLine("********* Start SaveReportHolding *********");
                //System.Threading.Thread.Sleep(2000);

                using (WealthAdvise_DevEntities context = new WealthAdvise_DevEntities())
                {
                    //context.Database.Log = Console.Write;

                    using (DbContextTransaction transaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            List<report_holding> reportSort = context.report_holding.ToList().Where(r => r.created_date.Equals(reportEntityList[0].created_date)).ToList();

                            if (reportSort != null)
                            {
                                //Console.WriteLine("report_Holdings != null");
                                context.report_holding.RemoveRange(reportSort);
                                context.SaveChanges();
                            }

                            context.report_holding.AddRange(reportEntityList);
                            context.SaveChanges();

                        }
                        catch (Exception ex)
                        {
                            LineService.lineNotify($"Error SaveReportHolding, {ex.Message}");
                            transaction.Rollback();
                            Console.WriteLine("Error occurred.");
                            logger.Info(ex, $"SaveReportHolding: {ex.Message}");
                        }

                        transaction.Commit();
                    }
                }
            }
        }

        private static void SaveInvestmentPerformance(List<investment_performance> investments)
        {
            if (investments.Count > 0)
            {
                //Console.WriteLine("********* Start SaveInvestmentPerformance *********");
                //System.Threading.Thread.Sleep(2000);

                using (WealthAdvise_DevEntities context = new WealthAdvise_DevEntities())
                {
                    //context.Database.Log = Console.Write;

                    using (DbContextTransaction transaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            List<investment_performance> investSort = context.investment_performance.ToList().Where(r => r.created_date.Equals(investments[0].created_date)).ToList();

                            if (investSort != null)
                            {
                                //Console.WriteLine("report_Holdings != null");
                                context.investment_performance.RemoveRange(investSort);
                                context.SaveChanges();
                            }

                            context.investment_performance.AddRange(investments);
                            context.SaveChanges();
                        }
                        catch (Exception ex)
                        {
                            LineService.lineNotify($"Error SaveInvestmnest, {ex.Message}");
                            transaction.Rollback();
                            Console.Write(ex);
                            Console.WriteLine("Error occurred.");
                            logger.Info(ex, $"SaveInvestmnest: {ex.Message}");
                        }

                        transaction.Commit();
                    }
                }
            }
        }

        private static void SaveDailyChartReport(List<daily_chart_report> reportEntityList)
        {
            if (reportEntityList.Count > 0)
            {
                using (WealthAdvise_DevEntities context = new WealthAdvise_DevEntities())
                {
                    //context.Database.Log = Console.Write;

                    using (DbContextTransaction transaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            List<daily_chart_report> reportSort = context.daily_chart_report.ToList().Where(r => r.profile_id.Equals(reportEntityList[0].profile_id)).ToList();

                            if (reportSort != null)
                            {
                                //Console.WriteLine("daily_chart_report != null");
                                context.daily_chart_report.RemoveRange(reportSort);
                                context.SaveChanges();
                            }

                            context.daily_chart_report.AddRange(reportEntityList);
                            context.SaveChanges();

                        }
                        catch (Exception ex)
                        {
                            LineService.lineNotify($"Error SaveDailyChartReport, {ex.Message}");
                            transaction.Rollback();
                            Console.WriteLine($"Error occurred. {ex.Message}");
                            logger.Error(ex, $"SaveDailyChartReport: {ex.Message}");
                        }

                        transaction.Commit();
                    }
                }
            }
        }

        private static bool CheckWorkingDay()
        {
            DataTable dtRes = new DataTable();
            bool chk = false;

            try
            {
                string now = DateTime.Now.ToString("dd/MM/yyyy", CultureInfo.GetCultureInfo("en-US"));
                string str = "";
                str += " SELECT *";
                str += " FROM   DUE_DATE";
                str += " WHERE  Due_Date = '" + now + "'";

                using (EFUnitOfWorkWEBDB unitOfWork = new EFUnitOfWorkWEBDB())
                {
                    dtRes = unitOfWork.GetData(str.ToString());
                }

                DataTable dt = dtRes;

                if (dt != null && dt.Rows.Count > 0)
                {
                    chk = true;
                }
                else
                {
                    chk = false;
                }
            }
            catch (Exception ex)
            {
                chk = false;
                LineService.lineNotify($"Error CheckWorkingDay, {ex.Message}");
                logger.Error(ex, $"CheckWorkingDay: {ex.Message}");
            }

            if (chk)
            {
                LineService.lineNotify($"Starting Process...");
            }
            else
            {
                LineService.lineNotify($"Holiday Not Start Process...");
            }


            return chk;
        }

        private static void ProgressBar(int totalLine, int countTotal)
        {
            var percent = calPercent(totalLine, countTotal);

            StringBuilder per = new StringBuilder(42);
            per.Append("                ");
            per.Append(percent.ToString());
            per.Append(" % Complete");
            per.Append(' ', 40 - per.Length);

            StringBuilder progress = new StringBuilder(42);
            progress.Append('[');
            progress.Append('#', (int.Parse(percent.ToString()) * 40 / 100));
            progress.Append('_', 41 - progress.Length);
            progress.Append(']');

            Console.CursorTop -= 1;
            Console.CursorLeft = 0;
            Console.WriteLine(progress.ToString());
            Console.CursorLeft = 0;
            Console.Write(per.ToString());
            System.Threading.Thread.Sleep(30);
        }

        private static double calPercent(int totalLine, int countTotal)
        {
            double cal1 = totalLine - countTotal;
            double cal2 = totalLine - cal1;
            double cal3 = cal2 / totalLine;
            double percent = Math.Round(cal3, 2) * 100;

            return percent;
        }

    }
}
