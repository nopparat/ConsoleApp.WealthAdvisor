﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WealthCutofftime
{
    public class OrderAwaitingSendOrderModel
    {
        public string allotdate { get; set; }
        public string orderdate { get; set; }
        public string account { get; set; }
        public string type { get; set; }
        public string amcode { get; set; }
        public string fundcode { get; set; }
        public string unit { get; set; }
        public string amount { get; set; }
        public string paytype { get; set; }
    }

    public class EsServiceApiModels
    {
        [JsonProperty(PropertyName = "code")]
        public string code { get; set; }

        [JsonProperty(PropertyName = "msg")]
        public string msg { get; set; }

        [JsonProperty(PropertyName = "Amount")]
        public string Amount { get; set; }

        [JsonProperty(PropertyName = "Units")]
        public string Units { get; set; }

        [JsonProperty(PropertyName = "Cost")]
        public string Cost { get; set; }

        [JsonProperty(PropertyName = "codeIFIS")]
        public string codeIFIS { get; set; }

        [JsonProperty(PropertyName = "msgIFIS")]
        public string msgIFIS { get; set; }
    }

    public class FundNavModel
    {
        public string fundcode { get; set; }
        public string navt0 { get; set; }
        public string navt_1 { get; set; }
    }

    public class OutstandingModel
    {
        public string tradedate { get; set; }
        public string profile_id { get; set; }
        public string account_no { get; set; }
        public string fundcode { get; set; }
        public string unit { get; set; }
        public string amount { get; set; }
        public string nav { get; set; }
        public string created_date { get; set; }
        public string navt0 { get; set; }
        public string navt_1 { get; set; }
        public string proj_abbr_name { get; set; }
        public string user_model_allocation_id { get; set; }
        public string asset_type_id { get; set; }

    }

    public class ReportHoldingModel
    {
        public string id { get; set; }
        public string profile_id { get; set; }
        public string account_no { get; set; }
        public string user_model_allocation_id { get; set; }
        public string proj_id { get; set; }
        public string proj_abbr_name { get; set; }
        public string percent_holding { get; set; }
        public string quantity { get; set; }
        public string avg_purchase_price { get; set; }
        public string market_price { get; set; }
        public string percent_last_change { get; set; }
        public string cumulative_change_value { get; set; }
        public string base_value { get; set; }
        public string created_date { get; set; }
        public string fundcode { get; set; }
        public string asset_id { get; set; }
        public string base_value_chart { get; set; }
        public string best_value_chart { get; set; }
        public string worst_value_chart { get; set; }

    }

    public class SumPortAssetType
    {
        public string profile_id { get; set; }
        public string sum_port { get; set; }
    }

    public class ReportHoldingCustomerModel
    {
        public string profile_id { get; set; }
        public string account_no { get; set; }
        public decimal sumport { get; set; }
        public decimal sumport_property_value_t0 { get; set; }
        public decimal sumport_property_value_t_1 { get; set; }
        public string user_model_allocation_id { get; set; }
        public string tradedate { get; set; }
        public DateTime trade_date { get; set; }
        public List<ReportHoldingItemModel> reportHoldingItems { get; set; }
    }

    public class ReportHoldingItemModel
    {
        public string profile_id { get; set; }
        public string account_no { get; set; }
        public string user_model_allocation_id { get; set; }
        public string fundcode { get; set; }
        public decimal percent_holding { get; set; }
        public decimal quantity { get; set; }
        public decimal avg_purchase_price { get; set; }
        public decimal market_price { get; set; }
        public decimal percent_last_change { get; set; }
        public decimal cumulative_change_value { get; set; }
        public decimal base_value { get; set; }
        public string tradedate { get; set; }
        public decimal property_value_t0 { get; set; }
        public decimal property_value_t_1 { get; set; }
        public string asset_type_id { get; set; }
    }

    public class InvestmentPerformance
    {
        public string profile_id { get; set; }
        public string account_no { get; set; }
        public string user_model_allocation_id { get; set; }
        public string created_date { get; set; }
        public string daily_return { get; set; }
        public string daily_property_value { get; set; }
        public string thai_equity_value { get; set; }
        public string foreign_equity_value { get; set; }
        public string goverment_bond_value { get; set; }
        public string corporate_bond_value { get; set; }
        public string property_value { get; set; }
        public string commodity_value { get; set; }
        public string cash_value { get; set; }
        public string base_value { get; set; }
        public string best_value { get; set; }
        public string worst_value { get; set; }
    }

    public class InvestmentPerformanceCustomerModel
    {
        public string profile_id { get; set; }
        public string account_no { get; set; }
        public string created_date { get; set; }
        public string user_model_allocation_id { get; set; }
        public decimal daily_return;
        public decimal daily_property_value;
        public decimal thai_equity;
        public decimal foreign_equity;
        public decimal goverment_bond;
        public decimal corporate_bond;
        public decimal property_value;
        public decimal commodity_value;
        public decimal cash_value;
        public decimal base_value;
        public decimal best_value;
        public decimal worst_value;
        public decimal daily_return_chart;

    }

    public class SummaryTotalCost
    {
        public string profile_id { get; set; }
        public string account_no { get; set; }
        public decimal sum_total_cost { get; set; }
    }

    public class UserAdjust
    {
        public string Account { get; set; }
        public string Amount { get; set; }
    }

    public class FundInformationOrder
    {
        public string fundcode { get; set; }
        public string proj_id { get; set; }
        public decimal? min_next { get; set; }
        public decimal? amount { get; set; }
        public DateTime tradedate { get; set; }
    }
}
