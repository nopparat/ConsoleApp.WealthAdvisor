﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SendEmailMyWealth
{
    public partial class Main : Form
    {
        static string ConnectionString = "Data Source=10.251.1.130;Initial Catalog=WealthAdvise_Dev;User ID=WAUserdb;Password=YT.wa@19;";
        private BindingSource bindSource = new BindingSource();

        public Main()
        {
            InitializeComponent();
        }

        private void Main_Load(object sender, EventArgs e)
        {
            SetCustomerData();
            dataGridView1.DataSource = bindSource;
        }

        private List<Customer> GetCustomers()
        {
            List<Customer> res = new List<Customer>();
            try
            {
                string sqlQ = "";
                sqlQ += " select * from user_model_allocation u";
                sqlQ += " where LOWER(u.[Status]) = LOWER('Active')";

                DataTable dtRes = GetData(sqlQ);

                if (dtRes.Rows.Count > 0)
                {
                    foreach (DataRow row in dtRes.Rows)
                    {
                        res.Add(new Customer()
                        {
                            profile_id = row["profile_id"].ToString(),
                            account_no = row["account_no"].ToString()
                        });
                    }

                    bindSource.DataSource = dtRes;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"error: {ex.Message}");
                throw;
            }
            return res;
        }

        private static DataTable GetData(string sql)
        {
            try
            {
                string connectionString = ConnectionString; ;
                DataSet retVal = new DataSet();
                SqlConnection sqlConn = new SqlConnection(connectionString);
                SqlCommand cmdReport = new SqlCommand(sql, sqlConn);
                SqlDataAdapter daReport = new SqlDataAdapter(cmdReport);
                using (SqlCommand cmd = new SqlCommand(sql, sqlConn))
                {
                    cmdReport.CommandType = CommandType.Text;
                    daReport.Fill(retVal);
                }
                sqlConn.Close();

                return retVal.Tables[0];
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }

        }

        private void SetCustomerData()
        {
            DataTable dtEmp = new DataTable();
            // add column to datatable  
            dtEmp.Columns.Add("sendEmail", typeof(bool));
            dtEmp.Columns.Add("profile_id", typeof(string));
            dtEmp.Columns.Add("account_no", typeof(string));
            dtEmp.Columns.Add("firstName", typeof(string));
            dtEmp.Columns.Add("lastName", typeof(string));
            dtEmp.Columns.Add("email", typeof(string));
            dtEmp.Columns.Add("emailIC", typeof(string));


            dtEmp.Rows.Add(false, "139399", "1393999W", "fName", "lName", "mail@mail.com", "ic.mail@mail.com");


            bindSource.DataSource = dtEmp;

            // Resize the DataGridView columns to fit the newly loaded content.
            dataGridView1.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCellsExceptHeader);
        }

        private void btn_send_mail_Click(object sender, EventArgs e)
        {

        }
    }

    public class Customer
    {
        public string profile_id { get; set; }
        public string account_no { get; set; }
    }

    public class CustomerInfo
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string email { get; set; }
        public string emailIC { get; set; }
    }

    public class CustomerData
    {
        public bool sendEmail { get; set; }
        public string profile_id { get; set; }
        public string account_no { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string email { get; set; }
        public string emailIC { get; set; }
    }
}
