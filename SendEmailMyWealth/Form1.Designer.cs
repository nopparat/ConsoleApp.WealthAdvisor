﻿namespace SendEmailMyWealth
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.btn_save_mail = new System.Windows.Forms.Button();
            this.btn_load_mail = new System.Windows.Forms.Button();
            this.btn_customer = new System.Windows.Forms.Button();
            this.btn_send_mail = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(12, 12);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(419, 616);
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = "";
            // 
            // btn_save_mail
            // 
            this.btn_save_mail.Location = new System.Drawing.Point(437, 586);
            this.btn_save_mail.Name = "btn_save_mail";
            this.btn_save_mail.Size = new System.Drawing.Size(91, 42);
            this.btn_save_mail.TabIndex = 1;
            this.btn_save_mail.Text = "Save Email";
            this.btn_save_mail.UseVisualStyleBackColor = true;
            // 
            // btn_load_mail
            // 
            this.btn_load_mail.Location = new System.Drawing.Point(534, 586);
            this.btn_load_mail.Name = "btn_load_mail";
            this.btn_load_mail.Size = new System.Drawing.Size(91, 42);
            this.btn_load_mail.TabIndex = 1;
            this.btn_load_mail.Text = "Load Email";
            this.btn_load_mail.UseVisualStyleBackColor = true;
            // 
            // btn_customer
            // 
            this.btn_customer.Location = new System.Drawing.Point(631, 586);
            this.btn_customer.Name = "btn_customer";
            this.btn_customer.Size = new System.Drawing.Size(91, 42);
            this.btn_customer.TabIndex = 1;
            this.btn_customer.Text = "Get User";
            this.btn_customer.UseVisualStyleBackColor = true;
            // 
            // btn_send_mail
            // 
            this.btn_send_mail.Location = new System.Drawing.Point(728, 586);
            this.btn_send_mail.Name = "btn_send_mail";
            this.btn_send_mail.Size = new System.Drawing.Size(91, 42);
            this.btn_send_mail.TabIndex = 1;
            this.btn_send_mail.Text = "Send Email";
            this.btn_send_mail.UseVisualStyleBackColor = true;
            this.btn_send_mail.Click += new System.EventHandler(this.btn_send_mail_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(437, 12);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(379, 288);
            this.dataGridView1.TabIndex = 2;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(828, 640);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.btn_send_mail);
            this.Controls.Add(this.btn_customer);
            this.Controls.Add(this.btn_load_mail);
            this.Controls.Add(this.btn_save_mail);
            this.Controls.Add(this.richTextBox1);
            this.Name = "Main";
            this.Text = "Send Mail MyWealth";
            this.Load += new System.EventHandler(this.Main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Button btn_save_mail;
        private System.Windows.Forms.Button btn_load_mail;
        private System.Windows.Forms.Button btn_customer;
        private System.Windows.Forms.Button btn_send_mail;
        private System.Windows.Forms.DataGridView dataGridView1;
    }
}

