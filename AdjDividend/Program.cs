﻿using AdjDividend.Access;
using Microsoft.ApplicationBlocks.Data;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml;

namespace AdjDividend
{
    class Program
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private static string baseUrlGet = ConfigurationManager.AppSettings["WebApiGet"];
        private static string baseUrlSet = ConfigurationManager.AppSettings["WebApiSet"];
        private static string user = ConfigurationManager.AppSettings["user"];
        private static string password = ConfigurationManager.AppSettings["password"];
        private static string product = ConfigurationManager.AppSettings["product"];
        private static string sid { get; set; }
        private static CultureInfo _currentCultureInfo => new CultureInfo("en-Us");
        private static string _con = ConfigurationManager.ConnectionStrings["SSO_NEWDEVEntities"].ConnectionString;

        static void Main(string[] args)
        {
            try
            {

                if (Login())
                {
                    using (WealthAdvise_DevEntities wealthDB = new WealthAdvise_DevEntities())
                    {
                        //context.Database.Log = Console.Write;

                        using (DbContextTransaction transaction = wealthDB.Database.BeginTransaction())
                        {
                            try
                            {

                                var results = wealthDB.wdividend.ToList()
                                    .Where(w => w.is_buy.Equals(false))
                                    .GroupBy(w => w.profile_id)
                                    .Select(w => new { profile_id = w.Key });

                                foreach (var item in results)
                                {
                                    user_model_allocation user = wealthDB.user_model_allocation.ToList()
                                        .Where(u => u.profile_id.Equals(item.profile_id))
                                        .Where(u => u.is_active.Equals(true))
                                        .FirstOrDefault();

                                    StringBuilder sb = new StringBuilder();
                                    sb.AppendLine(" select fi.fundcode, fi.proj_id, fi.min_next, ut.amount, ut.tradedate ");
                                    sb.AppendLine("   from fund_information fi ");
                                    sb.AppendLine("   left join initial_allocation ia on ia.fundcode = fi.fundcode ");
                                    sb.AppendLine("   left join ut_outstanding ut on ut.fundcode = ia.fundcode ");
                                    sb.AppendFormat(" where ia.profile_id = '{0}' ", user.profile_id);
                                    sb.AppendLine(" and cast(ia.percent_invest as decimal(18,2)) > 0 ");
                                    sb.AppendLine(" and ia.asset_type_id = 7 ");
                                    sb.AppendFormat(" and ut.tradedate = (SELECT MAX(tradedate) FROM ut_outstanding WHERE ut.fundcode = ia.fundcode and ut.profile_id = '{0}') ", user.profile_id);
                                    sb.AppendLine(" order by ut.amount ");

                                    List<FundInformationOrder> funds = wealthDB.Database.SqlQuery<FundInformationOrder>(sb.ToString()).ToList();

                                    Console.WriteLine($"res : {user.profile_id}");

                                    var sumTotal = wealthDB.wdividend.ToList()
                                    .Where(w => w.is_buy.Equals(false))
                                    .Where(w => w.profile_id.Equals(user.profile_id))
                                    .GroupBy(w => w.profile_id)
                                    .Select(w => w.Sum(x => x.amount))
                                    .FirstOrDefault();

                                    bool is_buy = true;
                                    foreach (var fund in funds)
                                    {
                                        if (sumTotal.GetValueOrDefault() >= fund.min_next)
                                        {
                                            string refNo = Guid.NewGuid().ToString();
                                            order_fund order = new order_fund();
                                            order.profile_id = user.profile_id;
                                            order.account_no = user.account_no;
                                            order.user_model_allocation_id = user.id;
                                            order.reference_no = refNo;
                                            order.status = "Awaiting send order";
                                            order.action = "buy";
                                            order.description = $"Auto Order dividend at {DateTime.Now.ToString("dd/MM/yyyy HH:mm", CultureInfo.GetCultureInfo("en-US"))}";
                                            order.created_date = DateTime.Now;
                                            order.modified_date = DateTime.Now;

                                            wealthDB.order_fund.Add(order);
                                            wealthDB.SaveChanges();

                                            order_fund_item orderItem = new order_fund_item();
                                            orderItem.fundcode = fund.fundcode;
                                            orderItem.proj_id = fund.proj_id;
                                            orderItem.proj_abbr_name = fund.fundcode;
                                            orderItem.expected_unit = "0.000000";
                                            orderItem.total_cost = sumTotal.ToString();
                                            orderItem.reference_no = refNo;
                                            orderItem.action = "buy";
                                            orderItem.transition_status = "pending";
                                            orderItem.is_rebalance = false;
                                            orderItem.is_change_model = false;
                                            wealthDB.order_fund_item.Add(orderItem);
                                            wealthDB.SaveChanges();


                                            List<wdividend> update = wealthDB.wdividend.ToList()
                                                .Where(w => w.profile_id.Equals(user.profile_id))
                                                .Where(w => w.is_buy.Equals(false))
                                                .ToList();

                                            update.ForEach(w =>
                                            {
                                                w.is_buy = true;
                                                w.buy_date = DateTime.Now;
                                                w.order_ref = refNo;
                                            });

                                            wealthDB.SaveChanges();

                                            is_buy = false;
                                            break;
                                        }
                                    }

                                    if (is_buy)
                                    {
                                        Console.WriteLine($"sumTotal : {sumTotal}");
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                transaction.Rollback();
                                Console.WriteLine("Error occurred.");
                                logger.Info(ex, $"SaveReportHolding: {ex.Message}");
                            }

                            transaction.Commit();
                        }
                    }

                    List<UserAdjust> userAdjusts = new List<UserAdjust>();
                    foreach (UserAdjust userAdjust in userAdjusts)
                    {
                        var res = ProcessAdjustMoney(userAdjust);

                        if (res.code.Equals("0"))
                        {

                        }
                    }
                }
                else
                {
                    logger.Error("login : failed");
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Exception Main : " + ex.Message);
            }
        }

        static bool Login()
        {
            try
            {
                logger.Info("Start Login");

                string url = "loginservice.aspx?user=" + HttpUtility.UrlEncode(user) +
                                              "&password=" + HttpUtility.UrlEncode(password) +
                                              "&product=" + HttpUtility.UrlEncode(product);

                string responseString = SendRequest(baseUrlGet + url);

                XmlDocument doc = new XmlDocument();
                string _res = HttpUtility.HtmlDecode(responseString);
                doc.LoadXml(_res);

                if (responseString.Length > 0)
                {
                    doc.LoadXml(responseString);
                    XmlNodeList nodeList = doc.GetElementsByTagName("result");
                    string jsonText = JsonConvert.SerializeXmlNode(nodeList[0], Newtonsoft.Json.Formatting.None, true);
                    var ss = JsonConvert.DeserializeObject<LoginServiceModels>(jsonText);

                    if (ss.code != "0")
                    {
                        logger.Error("Login Fail : " + ss.msg); return false;
                    }

                    sid = ss.sid;

                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                logger.Error("Exception Login : " + ex.Message);
                return false;
            }
        }

        static string SendRequest(string url)
        {
            string result = string.Empty;
            WebRequest request = HttpWebRequest.Create(url);
            using (WebResponse response = request.GetResponse())
            {
                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    result = reader.ReadToEnd();
                }
            }

            return result;
        }

        static EsServiceApiModels ProcessAdjustMoney(UserAdjust item)
        {
            logger.Info("ProcessAdjustMoney RefNo : " + item.Account);

            decimal currentMoney = getMoney(item.Account);
            Console.WriteLine($"ProcessAdjustMoney Before: {currentMoney}");
            InsertLogAdj(item.Account, item.Amount.ToString(), currentMoney.ToString());
            InsertLogAdjWealthDB(item.Account, item.Amount.ToString(), currentMoney.ToString());

            var resPost = PostAdjMoney("adjustmoney",
                                        item.Account,
                                        item.Amount.ToString(),
                                        "-",
                                        item.Account,
                                        "N", DateTime.Now.ToString("yyyyMMdd", _currentCultureInfo));

            decimal afterMoney = getMoney(item.Account);
            Console.WriteLine($"ProcessAdjustMoney After: {afterMoney}");

            return resPost;
        }

        static decimal getMoney(string acc)
        {
            try
            {
                string url = "getmoney.aspx?userid=" + HttpUtility.UrlEncode(user) +
                                          "&sid=" + HttpUtility.UrlEncode(sid) +
                                          "&product=" + HttpUtility.UrlEncode(product) +
                                          "&accountno=" + HttpUtility.UrlEncode(acc);

                string responseString = SendRequest(baseUrlSet + url);
                logger.Info("GetMoney : " + baseUrlSet + url);

                XmlDocument doc = new XmlDocument();
                string _res = HttpUtility.HtmlDecode(responseString);
                doc.LoadXml(_res);
                XmlNodeList nodeList = doc.GetElementsByTagName("result");
                string jsonText = JsonConvert.SerializeXmlNode(nodeList[0], Newtonsoft.Json.Formatting.None, true);
                var ss = JsonConvert.DeserializeObject<EsServiceApiModels>(jsonText);

                if (ss.code == "0")
                {
                    logger.Info("GetMoney Result : " + ss.Amount);
                    return Convert.ToDecimal(ss.Amount);
                }
                else
                {
                    logger.Info("GetMoney Result : " + ss.msg);
                    logger.Error("getMoney not success : " + ss.code + " " + ss.msg + " , account no :" + acc);
                    return 0;
                }
            }
            catch (Exception ex)
            {
                logger.Error("getMoney Exception : " + ex.Message);
                return 0;
            }
        }

        static EsServiceApiModels PostAdjMoney(string program, string acc, string Money, string type, string accountnoto, string flagadjloan, string effDate)
        {
            try
            {
                string url = HttpUtility.UrlEncode(user) + "&sid=" + sid +
                                                           "&product=" + HttpUtility.UrlEncode(product) +
                                                           "&accountno=" + HttpUtility.UrlEncode(acc.ToUpper()) +
                                                           "&adjustamount=" + HttpUtility.UrlEncode(type + Money);

                if (accountnoto != "")
                {
                    url += "&accountnoto=" + HttpUtility.UrlEncode(accountnoto);
                }

                if (program == "adjustmoney")
                {
                    url += "&flagadjloan=" + HttpUtility.UrlEncode(flagadjloan);

                    if (!string.IsNullOrWhiteSpace(effDate))
                    {
                        url += "&effdate=" + HttpUtility.UrlEncode(effDate);
                    }
                }
                string responseString = SendRequest(baseUrlSet + program + ".aspx?userid=" + url);

                logger.Info("PostAdjMoney : " + baseUrlSet + program + ".aspx?userid=" + url);

                XmlDocument doc = new XmlDocument();
                string _res = HttpUtility.HtmlDecode(responseString);
                doc.LoadXml(_res);
                XmlNodeList nodeList = doc.GetElementsByTagName("result");
                string jsonText = JsonConvert.SerializeXmlNode(nodeList[0], Newtonsoft.Json.Formatting.None, true);

                var ss = JsonConvert.DeserializeObject<EsServiceApiModels>(jsonText);

                logger.Info("PostAdjMoney Result : " + ss.code + " " + ss.msg);

                return ss;
            }
            catch (Exception ex)
            {
                logger.Error("Call Web service Exception : " + ex.Message);
                EsServiceApiModels data = new EsServiceApiModels();
                data.code = "3";
                data.msg = ex.Message;

                return data;
            }
        }

        static bool InsertLogAdjWealthDB(string acc, string adj_Amt, string old)
        {
            try
            {
                int res = 0;
                using (WealthAdvise_DevEntities wealthDB = new WealthAdvise_DevEntities())
                {
                    log_adjust_credit_eservice adj = new log_adjust_credit_eservice();
                    adj.Adj_Date = DateTime.Now;
                    adj.User_Name = user;
                    adj.Inv_No = acc;
                    adj.Adjust_Amount = adj_Amt.Replace(",", "");
                    adj.Cash_Amount = "0";
                    adj.Product = product;
                    adj.Old_BuyLimit = !string.IsNullOrWhiteSpace(old) ? old.Replace(",", "") : "'0.00'";

                    wealthDB.log_adjust_credit_eservice.Add(adj);
                    res = wealthDB.SaveChanges();
                }
                return res > 0 ? true : false;
            }
            catch (Exception ex)
            {
                logger.Error(ex, $"API InsertLogAdjWealthDB userName: {user}, accountNo: {acc}, adjAmt: {adj_Amt}: {ex.Message}");
                return false;
            }
        }

        static bool InsertLogAdj(string acc, string adj_Amt, string old)
        {
            try
            {
                int res = 0;
                DataTable dtRes = new DataTable();
                StringBuilder sb = new StringBuilder();

                if (acc.ToUpper().Contains("M") || acc.EndsWith("8"))
                {
                    sb.Append("INSERT INTO [LOG_ADJUST_CREDIT_BALANCE]");
                    sb.Append(" ([Adj_Date],[User_Name],[Inv_No] ,[Adjust_CreditLine],[Adjust_LoanLimit],[Adjust_CashBal],[Old_EE],[Product]) ");
                    sb.AppendFormat("VALUES ( getdate(),'{0}' ,'{1}' ,0,0,{2} ,{3} ,'PendingUpdFront')", user, acc, adj_Amt.Replace(",", ""), old.Replace(",", ""));
                }
                else
                {
                    sb.Append("INSERT INTO [LOG_ADJUST_CREDIT]");
                    sb.Append("  ([Adj_Date]  ,[User_Name],[Inv_No],[Adjust_Amount],[Old_BuyLimit],[Cash_Amount] ,[Product]) ");
                    sb.AppendFormat("VALUES ( getdate(),'{0}' ,'{1}' ,{2} ,{3} ,0,'PendingUpdFront')", user, acc, adj_Amt.Replace(",", ""), old.Replace(",", ""));
                }

                res = SqlHelper.ExecuteNonQuery(_con, CommandType.Text, sb.ToString());

                return res == 1 ? true : false;
            }
            catch (Exception ex)
            {
                logger.Error("InsertLogAdj Exception : " + ex.Message);
                return false;
            }
        }

    }
}
