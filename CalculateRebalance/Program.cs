﻿using CalculateRebalance.Access;
using CalculateRebalance.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculateRebalance
{
    class Program
    {
        static void Main(string[] args)
        {
            //CalculateRebalance("139399");
            initData();
        }

        static void initData()
        {
            WealthAdvise_DevEntities wealthDB = new WealthAdvise_DevEntities();

            List<order_fund_item> orders = wealthDB.order_fund_item.ToList().Where(item => item.transition_status.Equals("complete")).ToList();

            List<temp_rebalance> temp_rebalances = new List<temp_rebalance>();
            List<log_temp_rebalance> log_temp_rebalances = new List<log_temp_rebalance>();
            Guid uid = Guid.NewGuid();
            string refNo = string.Empty;
            foreach (var item in orders)
            {
                if (!item.reference_no.Equals(refNo))
                {
                    refNo = item.reference_no;
                    uid = Guid.NewGuid();
                }

                order_fund order = wealthDB.order_fund.ToList().Where(o => o.reference_no.Equals(item.reference_no)).SingleOrDefault();

                temp_rebalance temp = new temp_rebalance();
                temp.profile_id = order.profile_id;
                temp.account_no = order.account_no;
                temp.fundcode = item.fundcode;                
                temp.init_percent_holding = decimal.Parse(item.percent_weight);
                temp.net_amount = decimal.Parse(item.total_cost);
                temp.action = "buy";
                temp.created_date = DateTime.Now;
                temp.modified_date = DateTime.Now;
                temp_rebalances.Add(temp);

                log_temp_rebalance log_temp = new log_temp_rebalance();
                log_temp.uid = uid;
                log_temp.profile_id = order.profile_id;
                log_temp.account_no = order.account_no;
                log_temp.fundcode = item.fundcode;
                log_temp.init_percent_holding = decimal.Parse(item.percent_weight);
                log_temp.net_amount = decimal.Parse(item.total_cost);
                log_temp.action = "buy";
                log_temp.created_date = DateTime.Now;
                log_temp.modified_date = DateTime.Now;
                log_temp_rebalances.Add(log_temp);
            }

            try
            {
                InsertTempRebalanceInit(temp_rebalances, log_temp_rebalances);
            }
            catch (Exception)
            {

                throw;
            }
        }

        static void InsertTempRebalanceInit(List<temp_rebalance> temp_rebalances, List<log_temp_rebalance> log_temp_rebalances)
        {
            try
            {
                WealthAdvise_DevEntities wealthDB = new WealthAdvise_DevEntities();

                wealthDB.Database.ExecuteSqlCommand("truncate table temp_rebalance");
                wealthDB.Database.ExecuteSqlCommand("truncate table log_temp_rebalance");
                wealthDB.SaveChanges();

                wealthDB.temp_rebalance.AddRange(temp_rebalances);
                wealthDB.log_temp_rebalance.AddRange(log_temp_rebalances);
                wealthDB.SaveChanges();
            }
            catch (Exception)
            {
                throw;
            }
        }

        static void CalculateRebalance(string profile_id)
        {
            WealthAdvise_DevEntities wealthDB = new WealthAdvise_DevEntities();

            List<Q_Get_DataForCalculateWithdraw_Result> data = wealthDB.Q_Get_DataForCalculateWithdraw(profile_id).ToList();

            List<RebalanceChangeModel> rebalances = new List<RebalanceChangeModel>();

            Console.WriteLine($"data size: {data.Count}");
            decimal summary_net_amount = 0M;
            foreach (var item in data)
            {
                RebalanceChangeModel rebalance = new RebalanceChangeModel();
                rebalance.fundcode = item.fundcode;
                rebalance.min_initial = item.min_initial;
                rebalance.min_next = item.min_next;
                rebalance.min_redeem = item.min_redeem;
                rebalance.min_unit_redeem = item.min_unit_redeem;
                rebalance.duere = item.duere;
                rebalance.last_nav = item.last_nav;
                rebalance.init_percent_holding = item.init_percent_holding;
                rebalance.net_unit = item.net_unit;
                rebalance.net_amount = item.net_amount;
                rebalance.market_weight = item.market_weight;

                summary_net_amount += Math.Round(item.net_amount.GetValueOrDefault(0M), 4);
                Console.WriteLine($"{item.fundcode} || {item.min_initial} || {item.min_next} || {item.min_unit_redeem} || {item.net_unit} || {item.net_amount}");
            }

            //foreach (var rebalance in rebalances)
            //{
            //    rebalance.net_amount_weight = Math.Round((), 4);
            //    rebalance.net_amount_weight_total = item.net_amount_weight_total;
            //    rebalance.net_unit_weight = item.net_unit_weight;
            //    rebalance.unit_total_buy_sell = item.unit_total_buy_sell;
            //}

            Console.ReadKey(true);
        }
    }
}
