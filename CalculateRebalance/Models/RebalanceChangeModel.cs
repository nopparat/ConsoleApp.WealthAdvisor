﻿using CalculateRebalance.Access;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculateRebalance.Models
{
    class RebalanceChangeModel : Q_Get_DataForCalculateWithdraw_Result
    {
        public decimal new_percent_allocatiom { get; set; }
        public decimal net_amount_weight { get; set; }
        public decimal net_amount_weight_total { get; set; }
        public decimal net_unit_weight { get; set; }
        public decimal unit_total_buy_sell { get; set; }
        public string action { get; set; }
        public string meet_next_or_redeem { get; set; }
        public decimal final_amount_result_sell { get; set; }
        public decimal final_unit_result_sell { get; set; }
        public decimal final_amount_result_buy { get; set; }
    }
}
