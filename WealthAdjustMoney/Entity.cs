﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WealthAdjustMoney
{
    public class LoginServiceModels
    {
        [JsonProperty(PropertyName = "code")]
        public string code { get; set; }

        [JsonProperty(PropertyName = "msg")]
        public string msg { get; set; }

        [JsonProperty(PropertyName = "invno")]
        public string invno { get; set; }

        [JsonProperty(PropertyName = "invname")]
        public string invname { get; set; }

        [JsonProperty(PropertyName = "type")]
        public string type { get; set; }

        [JsonProperty(PropertyName = "sbl")]
        public string sbl { get; set; }

        [JsonProperty(PropertyName = "market")]
        public string market { get; set; }

        [JsonProperty(PropertyName = "sid")]
        public string sid { get; set; }

        [JsonProperty(PropertyName = "user_grade")]
        public string user_grade { get; set; }
    }

    public class EsServiceApiModels
    {
        [JsonProperty(PropertyName = "code")]
        public string code { get; set; }

        [JsonProperty(PropertyName = "msg")]
        public string msg { get; set; }

        [JsonProperty(PropertyName = "Amount")]
        public string Amount { get; set; }

        [JsonProperty(PropertyName = "Units")]
        public string Units { get; set; }

        [JsonProperty(PropertyName = "Cost")]
        public string Cost { get; set; }

        [JsonProperty(PropertyName = "codeIFIS")]
        public string codeIFIS { get; set; }

        [JsonProperty(PropertyName = "msgIFIS")]
        public string msgIFIS { get; set; }
    }

    public class UserAdjust
    {
        public string Account { get; set; }
        public string Amount { get; set; }
    }

    public class FundInformationOrder
    {
        public string fundcode { get; set; }
        public string proj_id { get; set; }
        public decimal? min_next { get; set; }
        public decimal? amount { get; set; }
        public DateTime tradedate { get; set; }
    }
}
