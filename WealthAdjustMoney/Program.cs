﻿using Microsoft.ApplicationBlocks.Data;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Linq.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml;
using WealthAdjustMoney.Access;

namespace WealthAdjustMoney
{
    class Program
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private static string baseUrlGet = ConfigurationManager.AppSettings["WebApiGet"];
        private static string baseUrlSet = ConfigurationManager.AppSettings["WebApiSet"];
        private static string user = ConfigurationManager.AppSettings["user"];
        private static string password = ConfigurationManager.AppSettings["password"];
        private static string product = ConfigurationManager.AppSettings["product"];
        private static string sid { get; set; }
        private static CultureInfo _currentCultureInfo => new CultureInfo("en-Us");
        //private static string _con = ConfigurationManager.ConnectionStrings["SSOUAT_DEVEntities"].ConnectionString;
        private static string _con = ConfigurationManager.ConnectionStrings["EService_NEWDEVEntities"].ConnectionString;

        static void Main(string[] args)
        {
            if (CheckWorkingDay())
            {


                try
                {
                    if (Login())
                    {
                        // Final Allot
                        OrderAllotDividend();

                        logger.Info($"Start Adjust {DateTime.Now}");
                        LineService.lineNotify($"Start Adjust {DateTime.Now}");
                        List<UserAdjust> userAdjusts = GetUserAdjusts();
                        foreach (UserAdjust userAdjust in userAdjusts)
                        {
                            ProcessAdjustMoney(userAdjust);
                        }
                        logger.Info($"End Adjust {DateTime.Now}");
                        LineService.lineNotify($"End Adjust {DateTime.Now}");
                    }
                    else
                    {
                        LineService.lineNotify($"Error login : failed");
                        logger.Error("login : failed");
                    }
                }
                catch (Exception ex)
                {
                    LineService.lineNotify($"Error Exception Main : {ex.Message}");
                    logger.Error(ex, "Exception Main : " + ex.Message);
                }
            }
        }
        
        static bool Login()
        {
            try
            {
                logger.Info("Start Login");

                string url = "loginservice.aspx?user=" + HttpUtility.UrlEncode(user) +
                                              "&password=" + HttpUtility.UrlEncode(password) +
                                              "&product=" + HttpUtility.UrlEncode(product);

                string responseString = SendRequest(baseUrlGet + url);

                XmlDocument doc = new XmlDocument();
                string _res = HttpUtility.HtmlDecode(responseString);
                doc.LoadXml(_res);

                if (responseString.Length > 0)
                {
                    doc.LoadXml(responseString);
                    XmlNodeList nodeList = doc.GetElementsByTagName("result");
                    string jsonText = JsonConvert.SerializeXmlNode(nodeList[0], Newtonsoft.Json.Formatting.None, true);
                    var ss = JsonConvert.DeserializeObject<LoginServiceModels>(jsonText);

                    if (ss.code != "0")
                    {
                        LineService.lineNotify($"Error Login Fail : {ss.msg}");
                        //System.Threading.Thread.Sleep(2000);
                        Console.WriteLine($"Login Fail: {ss.msg}");
                        logger.Error("Login Fail : " + ss.msg);
                        return false;
                    }

                    sid = ss.sid;

                    //System.Threading.Thread.Sleep(2000);
                    Console.WriteLine($"Login Success: {sid}");
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                LineService.lineNotify($"Error Exception Login : {ex.Message}");
                logger.Error("Exception Login : " + ex.Message);
                return false;
            }
        }

        static string SendRequest(string url)
        {
            string result = string.Empty;
            WebRequest request = HttpWebRequest.Create(url);
            using (WebResponse response = request.GetResponse())
            {
                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    result = reader.ReadToEnd();
                }
            }

            return result;
        }

        static List<UserAdjust> GetUserAdjusts()
        {
            List<UserAdjust> listResult = new List<UserAdjust>();
            logger.Info($"GetUserAdjusts");

            using (WealthAdvise_DevEntities db = new WealthAdvise_DevEntities())
            {
                #region Order Buy
                List<order_fund> orderBuys = db.order_fund.ToList()
                    .Where(orf => orf.action == "buy"
                    && orf.status != "complete"
                    && orf.status != "cancel"
                    && orf.is_rebalance == false)
                    .ToList();

                //System.Threading.Thread.Sleep(2000);
                Console.WriteLine($"Order buy Count: {orderBuys.Count}");

                foreach (var item in orderBuys)
                {
                    logger.Info($"GetUserAdjusts today {DateTime.Today} <= {item.effdate} = {(DateTime.Today <= item.effdate)}");
                    bool adj = false;
                    if (item.effdate != null)
                    {
                        if (DateTime.Today <= item.effdate)
                        {
                            adj = true;
                        }
                    }
                    else
                    {
                        adj = true;
                    }

                    if (adj)
                    {
                        var res = db.order_fund_item.Where(orfi => orfi.reference_no == item.reference_no).ToList();
                        decimal resSum = res.Sum(s => decimal.Parse(s.total_cost));

                        UserAdjust userAdjust = new UserAdjust();
                        userAdjust.Account = item.account_no;
                        userAdjust.Amount = resSum.ToString();
                        listResult.Add(userAdjust);

                        logger.Info($"Order Buy at, {DateTime.Today} <= {item.effdate}, {userAdjust.Account}: {userAdjust.Amount}");
                        Console.WriteLine($"Order Buy at, {DateTime.Today} <= {item.effdate}, {userAdjust.Account}: {userAdjust.Amount}");
                    }
                }
                #endregion
                
                #region Order Sell Rebalance


                // 1. list oreder sell status processing and complete for rebalance
                List<order_fund> orderSellRebalances = db.order_fund.ToList()
                    .Where(orf => orf.action.ToUpper() == "sell".ToUpper()
                    && orf.status.ToUpper() != "pending".ToUpper()
                    && orf.status.ToUpper() != "Awaiting send order".ToUpper()
                    && orf.status.ToUpper() != "cancel".ToUpper()
                    && orf.is_rebalance == true)
                    .ToList();

                //System.Threading.Thread.Sleep(2000);
                Console.WriteLine($"Order Sell Rebalance Count Total: {orderSellRebalances.Count}");

                List<temp_order_sell> _temp_order_sells = new List<temp_order_sell>();
                _temp_order_sells = db.temp_order_sell.ToList();

                List<temp_order_sell> insertTempOrderSells = orderSellRebalances.Where(o => !_temp_order_sells.Any(t2 => t2.reference_no == o.reference_no))
                    .Select(o => new temp_order_sell()
                    {
                        profile_id = o.profile_id,
                        account_no = o.account_no,
                        reference_no = o.reference_no,
                        rebalance_no = o.rebalance_no,
                        is_buy = false
                    })
                    .ToList();

                //System.Threading.Thread.Sleep(2000);
                Console.WriteLine($"Insert Temp Order Sell Rebalance Count Total: {insertTempOrderSells.Count}");

                if (insertTempOrderSells.Count > 0)
                {
                    db.temp_order_sell.AddRange(insertTempOrderSells);
                    db.SaveChanges();

                    //System.Threading.Thread.Sleep(2000);
                    Console.WriteLine($"Insert Temp Order Sell Rebalance Count Total: {insertTempOrderSells.Count} Success");
                }

                _temp_order_sells = db.temp_order_sell.ToList();
                List<order_fund> orderSellAdj = (from o in orderSellRebalances
                                                 join t in _temp_order_sells on o.reference_no equals t.reference_no
                                                 where o.reference_no == t.reference_no && t.is_buy == false
                                                 select o).ToList();
                //System.Threading.Thread.Sleep(2000);
                Console.WriteLine($"Order Sell Rebalance For Adjust Count Total: {orderSellAdj.Count}");


                List<order_fund_item> orderItemsAdj = db.order_fund_item.ToList().Where(o1 => orderSellAdj.Any(o2 => o2.reference_no == o1.reference_no)).ToList();
                //System.Threading.Thread.Sleep(2000);
                Console.WriteLine($"Order Item Sell Rebalance For Adjust Count Total: {orderItemsAdj.Count}");

                DateTime now = DateTime.Now.Date;
                List<temp_due> _temp_due = db.temp_due.Where(t => now > t.due_date).ToList();

                //System.Threading.Thread.Sleep(2000);
                Console.WriteLine($"Temp DUE Count Total: {_temp_due.Count}");

                List<trading> tradings = db.trading.ToList().Where(t => t.action.ToUpper() == "sell".ToUpper()).ToList();
                foreach (var item in orderSellAdj)
                {
                    List<order_fund_item> _order_item = orderItemsAdj
                        .Where(o => o.reference_no.Trim() == item.reference_no.Trim() && o.transition_status != "cancel")
                        .ToList();

                    List<order_fund_item> _order_item_adj = _order_item
                        .Where(o1 =>
                            _temp_due.Any(td2 => td2.trade_date == o1.effdate
                            && td2.account_no.Trim() == item.account_no.Trim()
                            && td2.fundcode.Trim() == o1.fundcode.Trim())
                        ).ToList();

                    List<trading> tradingsCalculate = tradings
                        .Where(t =>
                            _order_item_adj.Any(o2 => o2.effdate == t.trade_date
                            && item.account_no.Trim() == t.account_no.Trim()
                            && o2.fundcode.Trim() == t.fundcode.Trim())
                        ).ToList();
                    
                    decimal sumSell = tradingsCalculate.Sum(s => s.amount.GetValueOrDefault());

                    //System.Threading.Thread.Sleep(2000);
                    Console.WriteLine($"{item.account_no} Trading Count Total: {tradingsCalculate.Count}, SumSell = {sumSell}");

                    UserAdjust userAdjust = new UserAdjust();
                    userAdjust.Account = item.account_no;
                    userAdjust.Amount = sumSell.ToString();
                    listResult.Add(userAdjust);

                    logger.Info($"Order Sell at, {DateTime.Today} <= {item.effdate}, {userAdjust.Account}: {userAdjust.Amount}");

                    if (_order_item.Count == _order_item_adj.Count)
                    {
                        string effStr = GetEffDate();
                        DateTime effDate = DateTime.ParseExact(effStr, "dd/MM/yyyy", CultureInfo.GetCultureInfo("en-US"));

                        temp_order_sell updateFlag = _temp_order_sells.Where(t => t.reference_no == item.reference_no).FirstOrDefault();
                        updateFlag.is_buy = true;
                        updateFlag.flag_buy_date = DateTime.Now;
                        updateFlag.eff_buy_date = effDate;

                        db.SaveChanges();
                    }
                }
                #endregion

                #region Order Buy Rebalance
                List<order_fund> orderBuyRebalances = db.order_fund.ToList()
                    .Where(orf => orf.action == "buy"
                    && orf.status != "complete"
                    && orf.status != "cancel"
                    && orf.is_rebalance == true)
                    .ToList();

                //System.Threading.Thread.Sleep(2000);
                Console.WriteLine($"Order buy rebalance Count: {orderBuyRebalances.Count}");

                foreach (var item in orderBuyRebalances)
                {
                     //if order buy 
                    order_fund orderSellCompletes = db.order_fund.ToList()
                        .Where(orf => orf.action == "sell"
                        && orf.status == "complete"
                        && orf.rebalance_no == item.rebalance_no
                        && orf.is_rebalance == true)
                        .FirstOrDefault();
                    if (orderSellCompletes != null)
                    {
                        if (DateTime.Today <= item.effdate)
                        {
                            List<trading> _tradings = tradings
                                .Where(t => t.profile_id == orderSellCompletes.profile_id
                                && t.trade_date == orderSellCompletes.effdate
                                && t.action.ToUpper() == "Sell".ToUpper())
                                .ToList();

                            decimal sumSell = _tradings.Sum(s => s.amount.GetValueOrDefault());

                            UserAdjust userAdjust = new UserAdjust();
                            userAdjust.Account = item.account_no;
                            userAdjust.Amount = sumSell.ToString();
                            listResult.Add(userAdjust);

                            logger.Info($"Order Buy Rebalance at, {DateTime.Today} <= {item.effdate}, {userAdjust.Account}: {userAdjust.Amount}");
                            Console.WriteLine($"Order Buy Rebalance at, {DateTime.Today} <= {item.effdate}, {userAdjust.Account}: {userAdjust.Amount}");
                        }
                    }
                }
                #endregion
                
            }

            //System.Threading.Thread.Sleep(2000);
            Console.WriteLine($"Adjust Money Count: {listResult.Count}");

            return listResult;
        }

        static void ProcessAdjustMoney(UserAdjust item)
        {
            logger.Info("ProcessAdjustMoney Account : " + item.Account);

            decimal currentMoney = getMoney(item.Account);
            Console.WriteLine($"Before: {currentMoney}");
            InsertLogAdj(item.Account, item.Amount.ToString(), currentMoney.ToString());
            InsertLogAdjWealthDB(item.Account, item.Amount.ToString(), currentMoney.ToString());

            var resPost = PostAdjMoney("adjustmoney",
                                        item.Account,
                                        item.Amount.ToString(),
                                        "-",
                                        item.Account,
                                        "N", DateTime.Now.ToString("yyyyMMdd", _currentCultureInfo));

            decimal afterMoney = getMoney(item.Account);
            Console.WriteLine($"Before: {afterMoney}");

            //System.Threading.Thread.Sleep(2000);
            Console.WriteLine($"Adjust Money: {currentMoney} to {afterMoney}");

            LineService.lineNotify($"Adjust Money Account {item.Account}, {currentMoney} to {afterMoney}");
        }

        static decimal getMoney(string acc)
        {
            try
            {
                string url = "getmoney.aspx?userid=" + HttpUtility.UrlEncode(user) +
                                          "&sid=" + HttpUtility.UrlEncode(sid) +
                                          "&product=" + HttpUtility.UrlEncode(product) +
                                          "&accountno=" + HttpUtility.UrlEncode(acc);

                string responseString = SendRequest(baseUrlSet + url);
                logger.Info("GetMoney : " + baseUrlSet + url);

                XmlDocument doc = new XmlDocument();
                string _res = HttpUtility.HtmlDecode(responseString);
                doc.LoadXml(_res);
                XmlNodeList nodeList = doc.GetElementsByTagName("result");
                string jsonText = JsonConvert.SerializeXmlNode(nodeList[0], Newtonsoft.Json.Formatting.None, true);
                var ss = JsonConvert.DeserializeObject<EsServiceApiModels>(jsonText);

                if (ss.code == "0")
                {
                    logger.Info("GetMoney Result : " + ss.Amount);
                    return Convert.ToDecimal(ss.Amount);
                }
                else
                {
                    logger.Info("GetMoney Result : " + ss.msg);
                    logger.Error("getMoney not success : " + ss.code + " " + ss.msg + " , account no :" + acc);
                    return 0;
                }
            }
            catch (Exception ex)
            {
                LineService.lineNotify($"Error getMoney Exception : {ex.Message}");
                logger.Error("getMoney Exception : " + ex.Message);
                return 0;
            }
        }

        static EsServiceApiModels PostAdjMoney(string program, string acc, string Money, string type, string accountnoto, string flagadjloan, string effDate)
        {
            try
            {
                string url = HttpUtility.UrlEncode(user) + "&sid=" + sid +
                                                           "&product=" + HttpUtility.UrlEncode(product) +
                                                           "&accountno=" + HttpUtility.UrlEncode(acc.ToUpper()) +
                                                           "&adjustamount=" + HttpUtility.UrlEncode(type + Money);

                if (accountnoto != "")
                {
                    url += "&accountnoto=" + HttpUtility.UrlEncode(accountnoto);
                }

                if (program == "adjustmoney")
                {
                    url += "&flagadjloan=" + HttpUtility.UrlEncode(flagadjloan);

                    if (!string.IsNullOrWhiteSpace(effDate))
                    {
                        url += "&effdate=" + HttpUtility.UrlEncode(effDate);
                    }
                }
                string responseString = SendRequest(baseUrlSet + program + ".aspx?userid=" + url);

                logger.Info("PostAdjMoney : " + baseUrlSet + program + ".aspx?userid=" + url);

                XmlDocument doc = new XmlDocument();
                string _res = HttpUtility.HtmlDecode(responseString);
                doc.LoadXml(_res);
                XmlNodeList nodeList = doc.GetElementsByTagName("result");
                string jsonText = JsonConvert.SerializeXmlNode(nodeList[0], Newtonsoft.Json.Formatting.None, true);

                var ss = JsonConvert.DeserializeObject<EsServiceApiModels>(jsonText);

                logger.Info("PostAdjMoney Result : " + ss.code + " " + ss.msg);

                //System.Threading.Thread.Sleep(2000);
                Console.WriteLine($"PostAdjMoney Result : {ss.code} to {ss.msg}");

                return ss;
            }
            catch (Exception ex)
            {
                LineService.lineNotify($"Error Call Web service Exception : {ex.Message}");
                //System.Threading.Thread.Sleep(2000);
                Console.WriteLine($"Call Web service Exception : {ex.Message}");

                logger.Error("Call Web service Exception : " + ex.Message);
                EsServiceApiModels data = new EsServiceApiModels();
                data.code = "3";
                data.msg = ex.Message;

                return data;
            }
        }

        static bool InsertLogAdjWealthDB(string acc, string adj_Amt, string old)
        {
            try
            {
                int res = 0;
                using (WealthAdvise_DevEntities wealthDB = new WealthAdvise_DevEntities())
                {
                    log_adjust_credit_eservice adj = new log_adjust_credit_eservice();
                    adj.Adj_Date = DateTime.Now;
                    adj.User_Name = user;
                    adj.Inv_No = acc;
                    adj.Adjust_Amount = adj_Amt.Replace(",", "");
                    adj.Cash_Amount = "0";
                    adj.Product = product;
                    adj.Old_BuyLimit = !string.IsNullOrWhiteSpace(old) ? old.Replace(",", "") : "'0.00'";

                    wealthDB.log_adjust_credit_eservice.Add(adj);
                    res = wealthDB.SaveChanges();
                }
                return res > 0 ? true : false;
            }
            catch (Exception ex)
            {
                LineService.lineNotify($"Error API InsertLogAdjWealthDB userName: {user}, accountNo: {acc}, adjAmt: {adj_Amt}: {ex.Message}");
                logger.Error(ex, $"API InsertLogAdjWealthDB userName: {user}, accountNo: {acc}, adjAmt: {adj_Amt}: {ex.Message}");
                return false;
            }
        }

        static bool InsertLogAdj(string acc, string adj_Amt, string old)
        {
            try
            {
                int res = 0;
                DataTable dtRes = new DataTable();
                StringBuilder sb = new StringBuilder();

                if (acc.ToUpper().Contains("M") || acc.EndsWith("8"))
                {
                    //Product = PendingUpdFront
                    sb.Append("INSERT INTO [LOG_ADJUST_CREDIT_BALANCE]");
                    sb.Append(" ([Adj_Date],[User_Name],[Inv_No] ,[Adjust_CreditLine],[Adjust_LoanLimit],[Adjust_CashBal],[Old_EE],[Product]) ");
                    sb.AppendFormat("VALUES ( getdate(),'{0}' ,'{1}' ,0,0,{2} ,{3} ,'{4}')", user, acc, adj_Amt.Replace(",", ""), old.Replace(",", ""), product);
                }
                else
                {
                    //Product = PendingUpdFront
                    sb.Append("INSERT INTO [LOG_ADJUST_CREDIT]");
                    sb.Append("  ([Adj_Date]  ,[User_Name],[Inv_No],[Adjust_Amount],[Old_BuyLimit],[Cash_Amount] ,[Product]) ");
                    sb.AppendFormat("VALUES ( getdate(),'{0}' ,'{1}' ,{2} ,{3} ,0,'{4}')", user, acc, adj_Amt.Replace(",", ""), old.Replace(",", ""), product);
                }

                res = SqlHelper.ExecuteNonQuery(_con, CommandType.Text, sb.ToString());

                return res == 1 ? true : false;
            }
            catch (Exception ex)
            {
                LineService.lineNotify($"Error InsertLogAdj Exception : {ex.Message}");
                logger.Error("InsertLogAdj Exception : " + ex.Message);
                return false;
            }
        }

        static void OrderAllotDividend()
        {
            logger.Info("Order Allot Dividend Start");
            using (WealthAdvise_DevEntities wealthDB = new WealthAdvise_DevEntities())
            {
                //context.Database.Log = Console.Write;

                using (DbContextTransaction transaction = wealthDB.Database.BeginTransaction())
                {
                    try
                    {
                        // find user have dividend
                        var results = wealthDB.wdividend.ToList()
                            .Where(w => w.is_buy == false)
                            .GroupBy(w => w.profile_id)
                            .Select(w => new { profile_id = w.Key });

                        if (results != null)
                        {
                            //System.Threading.Thread.Sleep(2000);
                            Console.WriteLine($"Dividend Count: {results.Count()}");
                        }

                        List<order_fund> orders = new List<order_fund>();
                        List<order_fund_item> order_items = new List<order_fund_item>();
                        int count_buy = 0;
                        foreach (var item in results)
                        {
                            user_model_allocation user = wealthDB.user_model_allocation.ToList()
                                .Where(u => u.profile_id == item.profile_id
                                && u.is_active == true)
                                .FirstOrDefault();

                            //System.Threading.Thread.Sleep(2000);
                            Console.WriteLine($"Dividend user: {user.profile_id}");

                            StringBuilder sb = new StringBuilder();
                            sb.AppendLine(" select fi.fundcode, fi.proj_id, fi.min_next, (CAST(ut.unit as decimal(18, 4)) * CAST(ut.nav as decimal(18, 4))) amount, ut.tradedate ");
                            sb.AppendLine("   from fund_information fi ");
                            sb.AppendLine("   left join initial_allocation ia on ia.fundcode = fi.fundcode ");
                            sb.AppendLine("   left join ut_outstanding ut on ut.fundcode = ia.fundcode ");
                            sb.AppendFormat(" where ia.profile_id = '{0}' ", user.profile_id);
                            sb.AppendLine(" and cast(ia.percent_invest as decimal(18,2)) > 0 ");
                            sb.AppendLine(" and ia.asset_type_id = 7 ");
                            sb.AppendFormat(" and ut.tradedate = (SELECT MAX(tradedate) FROM ut_outstanding WHERE ut.fundcode = ia.fundcode and ut.profile_id = '{0}') ", user.profile_id);
                            sb.AppendLine(" order by (CAST(ut.unit as decimal(18, 4)) * CAST(ut.nav as decimal(18, 4))) asc ");

                            List<FundInformationOrder> funds = wealthDB.Database.SqlQuery<FundInformationOrder>(sb.ToString()).ToList();

                            var sumTotal = wealthDB.wdividend.ToList()
                            .Where(w => w.is_buy == false && w.profile_id == user.profile_id && w.as_of_date > DateTime.Now.Date)
                            .GroupBy(w => w.profile_id)
                            .Select(w => w.Sum(x => x.amount))
                            .FirstOrDefault();

                            bool isOrder = false;

                            var isDue = wealthDB.wdividend.Where(w => w.is_buy == false && w.profile_id == user.profile_id && w.as_of_date > DateTime.Now.Date).Count();

                            if (isDue == 0)
                            {
                                // TODO Disable Auto Buy Order Dividend
                                bool isAllocate = true;
                                foreach (var fund in funds)
                                {
                                    if (sumTotal.GetValueOrDefault() >= fund.min_next && isAllocate)
                                    {
                                        Console.WriteLine($"Buy fund: {fund.fundcode} = {sumTotal} บาท");

                                        string refNo = Guid.NewGuid().ToString().ToUpper();
                                        order_fund order = new order_fund();
                                        order.profile_id = user.profile_id;
                                        order.account_no = user.account_no;
                                        order.user_model_allocation_id = user.id;
                                        order.reference_no = refNo;
                                        order.status = "Awaiting send order";
                                        order.action = "buy";
                                        order.description = $"Auto Order dividend at {DateTime.Now.ToString("dd/MM/yyyy HH:mm", CultureInfo.GetCultureInfo("en-US"))}";
                                        order.created_date = DateTime.Now;
                                        order.modified_date = DateTime.Now;
                                        order.is_change_model = false;
                                        order.is_rebalance = false;
                                        orders.Add(order);

                                        order_fund_item orderItem = new order_fund_item();
                                        orderItem.fundcode = fund.fundcode;
                                        orderItem.proj_id = fund.proj_id;
                                        orderItem.proj_abbr_name = fund.fundcode;
                                        orderItem.expected_unit = "0.000000";
                                        orderItem.total_cost = sumTotal.ToString();
                                        orderItem.reference_no = refNo;
                                        orderItem.action = "buy";
                                        orderItem.transition_status = "pending";
                                        orderItem.is_rebalance = false;
                                        orderItem.is_change_model = false;
                                        order_items.Add(orderItem);

                                        isAllocate = false;
                                    }
                                }
                            }
                            else
                            {
                                count_buy++;
                            }

                            if (!isOrder)
                            {
                                //System.Threading.Thread.Sleep(2000);
                                Console.WriteLine($"Adjust Money Eservice: {user.account_no} amount {sumTotal} บาท");

                                Console.WriteLine($"sumTotal : {sumTotal}");
                                UserAdjust userAdjust = new UserAdjust();
                                userAdjust.Account = user.account_no;
                                userAdjust.Amount = sumTotal.GetValueOrDefault().ToString();

                                ProcessAdjustMoney(userAdjust);
                            }
                        }

                        if (count_buy == 0 && CheckDividenTradDateFromMonth())
                        {
                            wealthDB.order_fund.AddRange(orders);
                            wealthDB.SaveChanges();

                            wealthDB.order_fund_item.AddRange(order_items);
                            wealthDB.SaveChanges();
                            foreach (var order in orders)
                            {
                                List<wdividend> update = wealthDB.wdividend.ToList()
                                                .Where(w => w.profile_id == order.profile_id && w.is_buy == false && w.as_of_date > DateTime.Now.Date)
                                                .ToList();

                                foreach (var w in update)
                                {
                                    w.is_buy = true;
                                    w.buy_date = DateTime.Now;
                                    w.order_ref = order.reference_no;
                                }

                                wealthDB.SaveChanges();
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        LineService.lineNotify($"Error Order Allot Dividend : {ex.Message}");
                        transaction.Rollback();
                        Console.WriteLine($"Error occurred: {ex.Message}");
                        logger.Info(ex, $"Order Allot Dividend : {ex.Message}");
                    }

                    transaction.Commit();
                    logger.Info("Order Allot Dividend End");
                }
            }
        }

        private static string GetEffDate()
        {
            DataTable dtRes = new DataTable();
            string res = string.Empty;

            try
            {
                string now = DateTime.Now.ToString("dd/MM/yyyy", CultureInfo.GetCultureInfo("en-US"));
                StringBuilder str = new StringBuilder();
                str.Append("SELECT TOP 1 [Trade_Date], [Due_Date]");
                str.Append("  FROM [DUE_DATE]");
                str.AppendFormat("  where convert(date,Trade_Date,103) > convert(date,'{0}',103)", now);
                str.Append("  order by convert(date,Trade_Date,103)");

                using (EFUnitOfWorkWEBDB unitOfWork = new EFUnitOfWorkWEBDB())
                {
                    dtRes = unitOfWork.GetData(str.ToString());
                }

                DataTable dt = dtRes;

                if (dt != null && dt.Rows.Count > 0)
                {
                    res = dt.Rows[0]["Trade_Date"].ToString();
                }
                else
                {
                    logger.Info("No data");
                }
            }
            catch (Exception ex)
            {
                LineService.lineNotify($"Error GetEffDate: {ex.Message}");
                logger.Error(ex, $"GetEffDate: {ex.Message}");
            }

            return res;
        }

        public static bool CheckDividenTradDateFromMonth()
        {
            bool isAllocate = false;
            try
            {
                
                string sql = "";
                sql += " SELECT TOP 1 [Trade_Date]";
                sql += "   FROM DUE_DATE";
                sql += "  WHERE CONVERT(DATE,Trade_Date,103) >= CONVERT(DATE,'" + DateTime.Now.ToString("yyyyMM") + "20" + "',103)";
                sql += "  ORDER BY CONVERT(DATE,Trade_Date,103)";

                using (EFUnitOfWorkWEBDB unitOfWork = new EFUnitOfWorkWEBDB())
                {
                    DataTable dt = unitOfWork.GetData(sql);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        string dtTradDate = dt.Rows[0]["Trade_Date"].ToString();
                        DateTime tradDate = DateTime.ParseExact(dtTradDate, "dd/MM/yyyy", CultureInfo.GetCultureInfo("en-US"));

                        if (DateTime.Now.Date == tradDate)
                        {
                            isAllocate = true;
                        }
                    }
                    else
                    {
                        logger.Info("No data");
                    }
                }
            }
            catch (Exception e)
            {
                logger.Error("Error Exception GetFirstTradDateFromMonth : " + e.Message);
            }

            return isAllocate;
        }

        private static bool CheckWorkingDay()
        {
            DataTable dtRes = new DataTable();
            bool chk = false;

            try
            {
                string now = DateTime.Now.ToString("dd/MM/yyyy", CultureInfo.GetCultureInfo("en-US"));
                string str = "";
                str += " SELECT *";
                str += " FROM   DUE_DATE";
                str += " WHERE  Due_Date = '" + now + "'";

                using (EFUnitOfWorkWEBDB unitOfWork = new EFUnitOfWorkWEBDB())
                {
                    dtRes = unitOfWork.GetData(str.ToString());
                }

                DataTable dt = dtRes;

                if (dt != null && dt.Rows.Count > 0)
                {
                    chk = true;
                }
                else
                {
                    chk = false;
                }
            }
            catch (Exception ex)
            {
                chk = false;
                LineService.lineNotify($"Error CheckWorkingDay, {ex.Message}");
                logger.Error(ex, $"CheckWorkingDay: {ex.Message}");
            }

            if (chk)
            {
                LineService.lineNotify($"Starting Process...");
            }
            else
            {
                LineService.lineNotify($"Holiday Not Start Process...");
            }


            return chk;
        }

    }
}
