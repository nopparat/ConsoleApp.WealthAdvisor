//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WealthAdjustMoney.Access
{
    using System;
    using System.Collections.Generic;
    
    public partial class temp_order_sell
    {
        public int id { get; set; }
        public string profile_id { get; set; }
        public string account_no { get; set; }
        public string reference_no { get; set; }
        public string rebalance_no { get; set; }
        public Nullable<bool> is_buy { get; set; }
        public Nullable<System.DateTime> flag_buy_date { get; set; }
        public Nullable<System.DateTime> eff_buy_date { get; set; }
    }
}
