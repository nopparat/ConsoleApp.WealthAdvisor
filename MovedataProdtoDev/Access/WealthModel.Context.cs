﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MovedataProdtoDev.Access
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class WealthAdviseDevEntities : DbContext
    {
        public WealthAdviseDevEntities()
            : base("name=WealthAdviseDevEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<trading> trading { get; set; }
        public virtual DbSet<ut_fund_nav> ut_fund_nav { get; set; }
        public virtual DbSet<ut_outstanding> ut_outstanding { get; set; }
        public virtual DbSet<userwinfo> userwinfo { get; set; }
    }
}
