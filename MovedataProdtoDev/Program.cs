﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MovedataProdtoDev.Access;

namespace MovedataProdtoDev
{
    class Program
    {
        static void Main(string[] args)
        {
            GetData();
        }

        private static void GetData()
        {
            WEBDBEntities webDB = new WEBDBEntities();
            List<ut_fund_nav> fundNavs = webDB.ut_fund_nav.ToList();
            List<trading> tradings = webDB.trading.ToList();
            List<ut_outstanding> outstandings = webDB.ut_outstanding.ToList();
            List<userwinfo> userwinfos = webDB.userwinfo.ToList();

            using (WealthAdviseDevEntities wealthDB = new WealthAdviseDevEntities())
            {
                //context.Database.Log = Console.Write;

                using (DbContextTransaction transaction = wealthDB.Database.BeginTransaction())
                {
                    try
                    {
                        wealthDB.Database.ExecuteSqlCommand("TRUNCATE TABLE [ut_fund_nav]");
                        Console.WriteLine("TRUNCATE TABLE [ut_fund_nav]");

                        wealthDB.Database.ExecuteSqlCommand("TRUNCATE TABLE [trading]");
                        Console.WriteLine("TRUNCATE TABLE [trading]");

                        wealthDB.Database.ExecuteSqlCommand("TRUNCATE TABLE [ut_outstanding]");
                        Console.WriteLine("TRUNCATE TABLE [ut_outstanding]");

                        wealthDB.Database.ExecuteSqlCommand("TRUNCATE TABLE [userwinfo]");
                        Console.WriteLine("TRUNCATE TABLE [userwinfo]");

                        tradings.ForEach(trading =>
                        {
                            if (trading.profile_id.Equals("251825"))
                            {
                                trading.profile_id = "700101";
                                trading.account_no = "7001011W";
                            }
                        });

                        outstandings.ForEach(outstanding =>
                        {
                            if (outstanding.profile_id.Equals("251825"))
                            {
                                outstanding.profile_id = "700101";
                                outstanding.account_no = "7001011W";
                            }
                        });

                        wealthDB.ut_fund_nav.AddRange(fundNavs);
                        Console.WriteLine("Insert ut_fund_nav");
                        wealthDB.trading.AddRange(tradings);
                        Console.WriteLine("Insert trading");
                        wealthDB.ut_outstanding.AddRange(outstandings);
                        Console.WriteLine("Insert ut_outstanding");
                        wealthDB.userwinfo.AddRange(userwinfos);
                        Console.WriteLine("Insert userwinfo");
                        wealthDB.SaveChanges();
                        Console.WriteLine("=== Save ===");
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Console.WriteLine($"Error occurred, {ex.Message}");
                    }

                    transaction.Commit();
                }
            }
        }
    }
}
