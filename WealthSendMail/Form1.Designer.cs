﻿namespace WealthSendMail
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_send = new System.Windows.Forms.Button();
            this.txt_subject = new System.Windows.Forms.TextBox();
            this.txt_file = new System.Windows.Forms.TextBox();
            this.txt_body = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txt_to_order = new System.Windows.Forms.TextBox();
            this.txt_cc_order = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txt_to_dev = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txt_cc_dev = new System.Windows.Forms.TextBox();
            this.label_ccdev = new System.Windows.Forms.Label();
            this.chk_order = new System.Windows.Forms.CheckBox();
            this.chk_dev = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // btn_send
            // 
            this.btn_send.Location = new System.Drawing.Point(86, 375);
            this.btn_send.Name = "btn_send";
            this.btn_send.Size = new System.Drawing.Size(148, 47);
            this.btn_send.TabIndex = 0;
            this.btn_send.Text = "Send Mail";
            this.btn_send.UseVisualStyleBackColor = true;
            this.btn_send.Click += new System.EventHandler(this.btn_send_Click);
            // 
            // txt_subject
            // 
            this.txt_subject.Location = new System.Drawing.Point(86, 33);
            this.txt_subject.Name = "txt_subject";
            this.txt_subject.Size = new System.Drawing.Size(410, 20);
            this.txt_subject.TabIndex = 1;
            // 
            // txt_file
            // 
            this.txt_file.Location = new System.Drawing.Point(86, 82);
            this.txt_file.Multiline = true;
            this.txt_file.Name = "txt_file";
            this.txt_file.Size = new System.Drawing.Size(410, 77);
            this.txt_file.TabIndex = 2;
            // 
            // txt_body
            // 
            this.txt_body.Location = new System.Drawing.Point(86, 181);
            this.txt_body.Multiline = true;
            this.txt_body.Name = "txt_body";
            this.txt_body.Size = new System.Drawing.Size(410, 149);
            this.txt_body.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Subject";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 111);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Path File";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 236);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Body";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 443);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "ToOrder";
            // 
            // txt_to_order
            // 
            this.txt_to_order.Location = new System.Drawing.Point(86, 440);
            this.txt_to_order.Name = "txt_to_order";
            this.txt_to_order.ReadOnly = true;
            this.txt_to_order.Size = new System.Drawing.Size(410, 20);
            this.txt_to_order.TabIndex = 1;
            // 
            // txt_cc_order
            // 
            this.txt_cc_order.Location = new System.Drawing.Point(86, 466);
            this.txt_cc_order.Name = "txt_cc_order";
            this.txt_cc_order.ReadOnly = true;
            this.txt_cc_order.Size = new System.Drawing.Size(410, 20);
            this.txt_cc_order.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 469);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "CCOrder";
            // 
            // txt_to_dev
            // 
            this.txt_to_dev.Location = new System.Drawing.Point(86, 492);
            this.txt_to_dev.Name = "txt_to_dev";
            this.txt_to_dev.ReadOnly = true;
            this.txt_to_dev.Size = new System.Drawing.Size(410, 20);
            this.txt_to_dev.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 495);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "ToDev";
            // 
            // txt_cc_dev
            // 
            this.txt_cc_dev.Location = new System.Drawing.Point(86, 518);
            this.txt_cc_dev.Name = "txt_cc_dev";
            this.txt_cc_dev.ReadOnly = true;
            this.txt_cc_dev.Size = new System.Drawing.Size(410, 20);
            this.txt_cc_dev.TabIndex = 1;
            // 
            // label_ccdev
            // 
            this.label_ccdev.AutoSize = true;
            this.label_ccdev.Location = new System.Drawing.Point(16, 521);
            this.label_ccdev.Name = "label_ccdev";
            this.label_ccdev.Size = new System.Drawing.Size(41, 13);
            this.label_ccdev.TabIndex = 4;
            this.label_ccdev.Text = "CCDev";
            // 
            // chk_order
            // 
            this.chk_order.AutoSize = true;
            this.chk_order.Location = new System.Drawing.Point(86, 347);
            this.chk_order.Name = "chk_order";
            this.chk_order.Size = new System.Drawing.Size(68, 17);
            this.chk_order.TabIndex = 5;
            this.chk_order.Text = "To Order";
            this.chk_order.UseVisualStyleBackColor = true;
            // 
            // chk_dev
            // 
            this.chk_dev.AutoSize = true;
            this.chk_dev.Checked = true;
            this.chk_dev.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chk_dev.Enabled = false;
            this.chk_dev.Location = new System.Drawing.Point(172, 347);
            this.chk_dev.Name = "chk_dev";
            this.chk_dev.Size = new System.Drawing.Size(62, 17);
            this.chk_dev.TabIndex = 5;
            this.chk_dev.Text = "To Dev";
            this.chk_dev.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(529, 550);
            this.Controls.Add(this.chk_dev);
            this.Controls.Add(this.chk_order);
            this.Controls.Add(this.label_ccdev);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txt_body);
            this.Controls.Add(this.txt_file);
            this.Controls.Add(this.txt_cc_dev);
            this.Controls.Add(this.txt_to_dev);
            this.Controls.Add(this.txt_cc_order);
            this.Controls.Add(this.txt_to_order);
            this.Controls.Add(this.txt_subject);
            this.Controls.Add(this.btn_send);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_send;
        private System.Windows.Forms.TextBox txt_subject;
        private System.Windows.Forms.TextBox txt_file;
        private System.Windows.Forms.TextBox txt_body;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txt_to_order;
        private System.Windows.Forms.TextBox txt_cc_order;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txt_to_dev;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txt_cc_dev;
        private System.Windows.Forms.Label label_ccdev;
        private System.Windows.Forms.CheckBox chk_order;
        private System.Windows.Forms.CheckBox chk_dev;
    }
}

