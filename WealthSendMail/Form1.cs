﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Configuration;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WealthSendMail
{
    public partial class Form1 : Form
    {
        string toOrder = ConfigurationManager.AppSettings["MailToOrder"];
        string ccOrder = ConfigurationManager.AppSettings["MailCCOrder"];
        string todev = ConfigurationManager.AppSettings["MailToDev"];
        string ccdev = ConfigurationManager.AppSettings["MailCCDev"];

        public Form1()
        {
            InitializeComponent();

            txt_subject.Text = "Order My Wealth at (effDate dd/MM/yyyy)";
            txt_body.Text = "Please find Attached document for place order at (effDate dd/MM/yyyy)." +
                "<br/>" +
                "<br/>" +
                "My wealth System";

            txt_to_order.Text = toOrder;
            txt_cc_order.Text = ccOrder;
            txt_to_dev.Text = todev;
            txt_cc_dev.Text = ccdev;
        }

        private void btn_send_Click(object sender, EventArgs e)
        {
            string subject = txt_subject.Text;
            string files = txt_file.Text;
            string body = txt_body.Text;
            string errorMessage = string.Empty;
            if (subject == string.Empty)
            {
                errorMessage += "subject Empty \n";
            }

            if (body == string.Empty)
            {
                errorMessage += "Body Empty";
            }

            if (errorMessage == string.Empty)
            {
                List<string> filePaths = new List<string>();
                if (files != string.Empty)
                {
                    filePaths = files.Split(';').ToList();
                }
                
                if (chk_order.Checked)
                {
                    List<string> ccs = new List<string>();
                    if (ccOrder != string.Empty)
                    {
                        ccs = ccOrder.Split(';').ToList();
                    }
                    SendMail(subject, toOrder, TempleteMailOrder(body), filePaths, null, ccs);
                    MessageBox.Show("chk_order checked");
                }
                
                if (chk_dev.Checked)
                {
                    List<string> ccdevs = new List<string>();
                    if (ccdev != string.Empty)
                    {
                        ccdevs = ccdev.Split(';').ToList();
                    }
                    SendMail(subject, todev, TempleteMailOrder(body), filePaths, null, ccdevs);
                    MessageBox.Show("chk_dev checked");
                }
            }
            else
            {
                MessageBox.Show(errorMessage);
            }
        }


        public static void SendMail(string subject, string emailTo, AlternateView htmlbody, List<string> attachlist, List<string> emailBCC = null, List<string> emailCC = null)
        {
            SmtpSection section = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");

            string smtpAddress = section.Network.Host;
            int portNumber = section.Network.Port;
            bool enableSSL = false;

            string emailFrom = section.From;
            string password = "";

            //  string body = htmlbody;
            try
            {
                using (MailMessage mail = new MailMessage())
                {
                    mail.From = new MailAddress(emailFrom);
                    //mail.Headers.Add("Disposition-Notification-To", section.From);
                    //mail.To.Add(emailTo);

                    foreach (var address in emailTo.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        mail.To.Add(address);
                    }

                    if (emailCC != null)
                        foreach (var cc in emailCC)
                        {
                            mail.CC.Add(cc);
                        }
                    if (emailBCC != null)
                        foreach (var bcc in emailBCC)
                        {
                            mail.Bcc.Add(bcc);
                        }

                    mail.Subject = subject;
                    mail.AlternateViews.Add(htmlbody);
                    mail.IsBodyHtml = true;

                    if (attachlist != null)
                    {
                        List<Stream> streams = new List<Stream>();

                        foreach (string file in attachlist)
                        {
                            Stream attachmentStream = File.OpenRead(file);
                            mail.Attachments.Add(new Attachment(attachmentStream, Path.GetFileName(file)));
                        }
                    }
                    using (SmtpClient smtp = new SmtpClient(smtpAddress, portNumber))
                    {
                        smtp.Credentials = new NetworkCredential(emailFrom, password);
                        smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

                        smtp.EnableSsl = enableSSL;
                        smtp.Send(mail);

                    }
                }
            }
            catch (SmtpFailedRecipientException ex)
            {
                throw new Exception("Send Fail Email " + emailTo, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("Send Fail Email " + emailTo, ex);
            }
        }

        public static AlternateView TempleteMailOrder(string body)
        {
            return AlternateView.CreateAlternateViewFromString(body, new System.Net.Mime.ContentType("text/html"));
        }
    }
}
