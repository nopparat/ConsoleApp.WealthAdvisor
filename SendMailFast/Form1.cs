﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Configuration;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SendMailFast
{

    public partial class Text : Form
    {
        public static string ESERVICEDB = ConfigurationManager.AppSettings["ESERVICEDB"];

        static string SUBJECT_MAIL = "MyWealth: การปรับพอร์ตในรอบไตรมาส Q1/2020";

        public Text()
        {
            InitializeComponent();
        }

        private List<UserModel> getUserFund()
        {
            FundModel TSMCap = new FundModel() { fundnameEN = "T-SM Cap", assetType = "กองทุนหุ้นไทย" };
            FundModel KTPROPERTY = new FundModel() { fundnameEN = "KT-PROPERTY", assetType = "กองทุนอสังหาริมทรัพย์" };
            FundModel PRINCIPALGOPPA = new FundModel() { fundnameEN = "PRINCIPAL GOPP-A", assetType = "กองทุนหุ้นต่างประเทศ" };

            List<UserModel> users = new List<UserModel>() {
                new UserModel() { accountno = "1421115W", funds = new List<FundModel>() { KTPROPERTY, PRINCIPALGOPPA } },
                new UserModel() { accountno = "1422945W", funds = new List<FundModel>() { TSMCap } },
                new UserModel() { accountno = "1424904W", funds = new List<FundModel>() { TSMCap } },
                new UserModel() { accountno = "1425199W", funds = new List<FundModel>() { TSMCap } },
                new UserModel() { accountno = "1442953W", funds = new List<FundModel>() { TSMCap } },
                new UserModel() { accountno = "1443012W", funds = new List<FundModel>() { TSMCap } },
                new UserModel() { accountno = "1453980W", funds = new List<FundModel>() { TSMCap } },
                new UserModel() { accountno = "1460420W", funds = new List<FundModel>() { TSMCap } },
                new UserModel() { accountno = "1479792W", funds = new List<FundModel>() { TSMCap } },
                new UserModel() { accountno = "2518252W", funds = new List<FundModel>() { TSMCap } },
            };

            return users;
        }

        private void Mode(bool dev = true)
        {
            string sql = @"
select distinct a.account_no accountno, first_name + ' ' + last_name as custname , u.email custemail , a.marketing_id mktid, s.email mktemail
from user_profile u 
left join account a on u.profile_id = a.profile_id
left join staff s on s.user_id = 'YT' + a.marketing_id
where u.profile_id in (
 '142111'
,'142294'
,'142490'
,'142519'
,'144295'
,'144301'
,'145398'
,'146042'
,'147979'
,'251825'
)
and a.account_no like '%W'
";
            try
            {
                DataTable dt = EFUnitOfWork.GetData(ESERVICEDB, sql);
                SUBJECT_MAIL = "MyWealth: การปรับพอร์ตในรอบไตรมาส Q1/2020";
                string msg = SUBJECT_MAIL + ". Mode " + (dev ? "Develop" : "User");
                MessageBox.Show(msg);

                if (dt.Rows.Count > 0)
                {
                    
                    string quarterDate = "10/01/2020";
                    List<UserModel> users = getUserFund();

                    List<string> mailbcc = new List<string>();
                    mailbcc.Add("Nopparat.M@yuanta.co.th");

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        string accountno = dt.Rows[i]["accountno"].ToString();
                        string custname = dt.Rows[i]["custname"].ToString();
                        string custemail = dt.Rows[i]["custemail"].ToString();
                        string mktid = dt.Rows[i]["mktid"].ToString();
                        string mktemail = dt.Rows[i]["mktemail"].ToString();

                        List<string> mailcc = new List<string>();
                        mailcc.Add(mktemail);
                        mailcc.Add("wealthsvc@yuanta.co.th");

                        UserModel user = users.Where(x => x.accountno == accountno).FirstOrDefault();
                        if (user != null)
                        {
                            if (user.funds.Count > 0)
                            {
                                AlternateView maildata = MailTemplate(custname, accountno, quarterDate, user.funds);
                                if (dev)
                                {
                                    SendMail(SUBJECT_MAIL, "Nopparat.M@yuanta.co.th", maildata, null, null, null);
                                }
                                else
                                {
                                    SendMail(SUBJECT_MAIL, custemail, maildata, null, mailbcc, mailcc);
                                }
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Test_Click(object sender, EventArgs e)
        {
            this.Test.Enabled = false;
            
            Mode(true);
            InitProgress();
        }

        public static void SendMail(string subject, string emailTo, AlternateView htmlbody, List<string> attachlist, List<string> emailBCC = null, List<string> emailCC = null)
        {
            SmtpSection section = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");

            string smtpAddress = section.Network.Host;
            int portNumber = section.Network.Port;
            bool enableSSL = false;

            string emailFrom = section.From;
            string password = "";

            //  string body = htmlbody;
            try
            {

                using (MailMessage mail = new MailMessage())
                {
                    mail.From = new MailAddress(emailFrom);
                    //mail.Headers.Add("Disposition-Notification-To", section.From);
                    //mail.To.Add(emailTo);

                    foreach (var address in emailTo.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        mail.To.Add(address);
                    }

                    if (emailCC != null)
                        foreach (var cc in emailCC)
                        {
                            mail.CC.Add(cc);
                        }
                    if (emailBCC != null)
                        foreach (var bcc in emailBCC)
                        {
                            mail.Bcc.Add(bcc);
                        }

                    mail.Subject = subject;
                    mail.AlternateViews.Add(htmlbody);
                    mail.IsBodyHtml = true;

                    if (attachlist != null)
                    {
                        List<Stream> streams = new List<Stream>();

                        foreach (string file in attachlist)
                        {
                            Stream attachmentStream = File.OpenRead(file);
                            mail.Attachments.Add(new Attachment(attachmentStream, Path.GetFileName(file)));
                        }
                    }
                    using (SmtpClient smtp = new SmtpClient(smtpAddress, portNumber))
                    {
                        smtp.Timeout = 100000;
                        smtp.Credentials = new NetworkCredential(emailFrom, password);
                        smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

                        smtp.EnableSsl = enableSSL;
                        smtp.Send(mail);

                    }
                }
            }
            catch (SmtpFailedRecipientException ex)
            {
                throw new Exception("Send Fail Email " + emailTo, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("Send Fail Email " + emailTo, ex);
            }
        }
        
        public void InitProgress()
        {
            progressBar1.Maximum = 100;
            progressBar1.Step = 1;
            progressBar1.Value = 0;
            backgroundWorker.WorkerReportsProgress = true;
            backgroundWorker.RunWorkerAsync();
        }

        private void btnSendCustomer_Click(object sender, EventArgs e)
        {
            this.btnSendCustomer.Enabled = false;
            
            Mode(false);
            InitProgress();
        }

        private void Calculate(int i)
        {
            double pow = Math.Pow(i, i);
        }

        private void backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar1.Value = e.ProgressPercentage;
            this.Text = "Progress: " + e.ProgressPercentage.ToString() + "%";
        }

        private void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // TODO: do something with final calculation.
            this.Test.Enabled = true;
            this.btnSendCustomer.Enabled = true;
        }

        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            //var backgroundWorker = sender as BackgroundWorker;
            for (int j = 0; j < 1000000; j++)
            {
                Calculate(j);
                backgroundWorker.ReportProgress(((j + 1) * 100) / 1000000);
            }
        }

        private AlternateView MailTemplate(string custName, string custAccount, string quarterDate, List<FundModel> funds)
        {
            string body = "";

            body += "<div class='allowTextSelection _mcp_W1 customScrollBar ms-bg-color-white ms-font-color-black owa-font-compose' dir='ltr' style='outline: none; font-size: 12pt; color: #000000; font-family: Calibri, Arial, Helvetica, sans-serif,;' tabindex='0' role='textbox' contenteditable='true' spellcheck='true' aria-label='Message body'>";
            body += "<p class='MsoNormal'><span lang='TH' style='font-size: 14.0pt;'>เรียน คุณ</span><span lang='TH' style='font-size: 14.0pt;'> " + custName + "</span><span style='font-size: 14.0pt;'>, <span lang='TH' style='color: #222222; background: white;'>ผู้ใช้บริการ </span>My Wealth<span lang='TH' style='color: #222222; background: white;'> บัญชีเลขที่ </span></span><span style='font-size: 14.0pt;'>" + custAccount + "</span><span style='font-size: 14.0pt;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></p>";
            body += "<p class='MsoNormal'><span style='font-size: 14.0pt;'>&nbsp;</span></p>";
            body += "<p class='MsoNormal' style='text-indent: 36.0pt;'><span lang='TH' style='font-size: 14.0pt;'>เนื่องด้วยการลงทุนใน</span><span style='font-size: 14.0pt;'> My Wealth<span lang='TH'> จะมีการปรับการกระจายการลงทุน โดยกำหนดการปรับพอร์ตในรอบไตรมาสมีกำหนดในวันที่ " + quarterDate + "</span>&nbsp;<span lang='TH'> <strong>และได้มีการนำกองทุนบางส่วนออกจากคำแนะนำ</strong></span>&nbsp;<span lang='TH'> และเปลี่ยนกองทุนใหม่เข้ามาแทน</span></span></p>";
            body += "<p class='MsoNormal'><span lang='TH' style='font-size: 14.0pt;'>โดยการลงทุนของท่าน มีการลงทุนในกองทุนที่ออกจากคำแนะนำดังนี้ </span></p>";

            int index = 1;
            foreach (var fund in funds)
            {
                body += "<p class='MsoNormal' style='margin-bottom: 8.0pt; text-indent: 36.0pt; line-height: 105%;'><span lang='TH' style='font-size: 14.0pt; line-height: 105%;'>" + index + ". </span><span style='font-size: 14.0pt; line-height: 105%;'>" + fund.fundnameEN + "&nbsp; &nbsp; &nbsp;<span lang='TH'> " + fund.assetType + "</span></span></p>";
                index++;
            }

            body += "<p class='MsoNormal' style='text-indent: 36.0pt;'><span lang='TH' style='font-size: 14.0pt;'>เนื่องด้วยท่านได้เลือกลงทุนในกองทุนดังกล่าว ท่านต้องเข้ามาเลือกกองทุนใหม่ทดแทน ตามคำแนะนำกองทุนใหม่ เพื่อไม่ให้กระทบกับการปรับพอร์ตการลงทุนของท่าน <u><span style='color: #2f5496; background: white;'><a href='https://wealth.yuanta.co.th'>กรุณาคลิกที่นี่เพื่อเปลี่ยนแปลงรายการกองทุน</a></span></u> </span><span style='font-size: 14.0pt;'>&nbsp;</span></p>";
            body += "<p class='MsoNormal'><span style='font-size: 14.0pt;'>&nbsp;</span></p>";
            body += "<p class='MsoNormal'><span lang='TH' style='font-size: 14.0pt;'>ขอแสดงความนับถือ</span></p>";
            body += "<p class='MsoNormal'><span lang='TH' style='font-size: 14.0pt;'>บริษัทหลักทรัพย์ หยวนต้า (ประเทศไทย) จำกัด</span></p>";
            body += "<p class='MsoNormal'><span lang='TH' style='font-size: 14.0pt;'>หมายเหตุ:</span></p>";
            body += "<ul>";
            body += "<li><span lang='TH' style='font-size: 14.0pt; line-height: 105%;'>อีเมลฉบับนี้เป็นการแจ้งข้อมูลโดยอัตโนมัติ กรุณาอย่าตอบกลับ หากต้องการสอบถามรายละเอียดเพิ่มเติม โปรดติดต่อ </span><span style='font-size: 14.0pt; line-height: 105%;'>Yuanta Wealth <span lang='TH'>โทร. +</span>66 <span lang='TH'>(</span>0<span lang='TH'>) </span>26<span lang='TH'>0098</span>064<span lang='TH'> หรือ +</span>66 <span lang='TH'>(</span>0<span lang='TH'>) </span>26<span lang='TH'>0098</span>313 email <span lang='TH'>: </span><span class='MsoHyperlink'><a href='mailto:wealthsvc@yuanta.co.th'>wealthsvc@yuanta<span lang='TH'>.</span>co<span lang='TH'>.</span>th</a></span></span></li>";
            body += "</ul>";
            body += "<p class='MsoNormal'><span style='font-size: 14.0pt;'>&nbsp;</span></p>";
            body += "<p class='MsoNormal'>&nbsp;</p>";
            body += "</div>";

            return AlternateView.CreateAlternateViewFromString(body, new System.Net.Mime.ContentType("text/html"));
        }
    }

    public class UserModel
    {
        public UserModel()
        {
            this.funds = new List<FundModel>();
        }
        public string accountno { get; set; }
        public List<FundModel> funds { get; set; }
    }

    public class FundModel
    {
        public string fundnameEN { get; set; }
        public string assetType { get; set; }
    }
}
