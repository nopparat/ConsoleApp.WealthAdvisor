﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace SendMailFast
{
    public static class EFUnitOfWork
    {
        public static DataTable GetDataText(string ConnectionString, string sql, SqlParameter[] param = null)
        {
            try
            {
                string connectionString = ConnectionString;
                DataSet retVal = new DataSet();
                SqlConnection sqlConn = new SqlConnection(connectionString);
                SqlCommand cmdReport = new SqlCommand(sql, sqlConn);
                SqlDataAdapter daReport = new SqlDataAdapter(cmdReport);
                using (SqlCommand cmd = new SqlCommand(sql, sqlConn))
                {
                    cmdReport.CommandTimeout = 1800; //int.MaxValue;
                    cmdReport.CommandType = CommandType.Text;

                    if (param != null)
                    {
                        foreach (SqlParameter item in param)
                        {
                            cmdReport.Parameters.Add(item);
                        }
                    }

                    daReport.Fill(retVal);
                }
                sqlConn.Close();

                return retVal.Tables[0];
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }

        }

        public static DataTable GetDataStored(string ConnectionString, string sql, SqlParameter[] param = null)
        {
            try
            {
                string connectionString = ConnectionString;

                DataSet retVal = new DataSet();
                SqlConnection sqlConn = new SqlConnection(connectionString);
                SqlCommand cmdReport = new SqlCommand(sql, sqlConn);
                SqlDataAdapter daReport = new SqlDataAdapter(cmdReport);
                using (cmdReport)
                {
                    SqlParameter[] questionIdPrm = param;
                    cmdReport.CommandTimeout = 1800; //int.MaxValue;
                    cmdReport.CommandType = CommandType.StoredProcedure;
                    if (param != null)
                    {
                        foreach (SqlParameter item in param)
                        {
                            cmdReport.Parameters.Add(item);
                        }
                    }
                    daReport.Fill(retVal);
                }
                sqlConn.Close();

                return retVal.Tables[0];
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }

        }

        public static List<DataTable> GetDataStoreds(string ConnectionString, string sql, SqlParameter[] param)
        {
            try
            {
                string connectionString = ConnectionString;

                DataSet retVal = new DataSet();
                SqlConnection sqlConn = new SqlConnection(connectionString);
                SqlCommand cmdReport = new SqlCommand(sql, sqlConn);
                SqlDataAdapter daReport = new SqlDataAdapter(cmdReport);
                using (cmdReport)
                {
                    SqlParameter[] questionIdPrm = param;
                    cmdReport.CommandTimeout = 1800; //int.MaxValue;
                    cmdReport.CommandType = CommandType.StoredProcedure;
                    foreach (SqlParameter item in param)
                    {
                        cmdReport.Parameters.Add(item);
                    }
                    daReport.Fill(retVal);
                }
                sqlConn.Close();

                List<DataTable> res = new List<DataTable>();
                try
                {
                    foreach (DataTable item in retVal.Tables)
                    {
                        res.Add(item);
                    }
                }
                catch (Exception ex)
                {
                    string error = ex.Message;
                    throw;
                }
                return res;
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }

        }

        public static DataTable GetData(string ConnectionString, string sql)
        {
            try
            {
                string connectionString = ConnectionString; ;
                DataSet retVal = new DataSet();
                SqlConnection sqlConn = new SqlConnection(connectionString);
                SqlCommand cmdReport = new SqlCommand(sql, sqlConn);
                SqlDataAdapter daReport = new SqlDataAdapter(cmdReport);
                using (SqlCommand cmd = new SqlCommand(sql, sqlConn))
                {
                    cmdReport.CommandTimeout = 1800; //int.MaxValue;
                    cmdReport.CommandType = CommandType.Text;
                    daReport.Fill(retVal);
                }
                sqlConn.Close();

                return retVal.Tables[0];
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }

        }

        public static List<DataTable> GetDatas(string ConnectionString, string sql)
        {
            try
            {
                string connectionString = ConnectionString; ;
                DataSet retVal = new DataSet();
                SqlConnection sqlConn = new SqlConnection(connectionString);
                SqlCommand cmdReport = new SqlCommand(sql, sqlConn);
                SqlDataAdapter daReport = new SqlDataAdapter(cmdReport);
                using (SqlCommand cmd = new SqlCommand(sql, sqlConn))
                {
                    cmdReport.CommandTimeout = 1800; //int.MaxValue;
                    cmdReport.CommandType = CommandType.Text;
                    daReport.Fill(retVal);
                }
                sqlConn.Close();

                List<DataTable> res = new List<DataTable>();
                try
                {
                    foreach (DataTable item in retVal.Tables)
                    {
                        res.Add(item);
                    }
                }
                catch (Exception ex)
                {
                    string error = ex.Message;
                    throw;
                }
                return res;
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }

        }

        public static DataTable GetData(string ConnectionString, string sql, SqlParameter[] param)
        {
            try
            {
                string connectionString = ConnectionString;

                DataSet retVal = new DataSet();
                SqlConnection sqlConn = new SqlConnection(connectionString);
                SqlCommand cmdReport = new SqlCommand(sql, sqlConn);
                SqlDataAdapter daReport = new SqlDataAdapter(cmdReport);
                using (cmdReport)
                {
                    SqlParameter[] questionIdPrm = param;
                    cmdReport.CommandTimeout = 1800; //int.MaxValue;
                    cmdReport.CommandType = CommandType.StoredProcedure;
                    foreach (SqlParameter item in param)
                    {
                        cmdReport.Parameters.Add(item);
                    }
                    daReport.Fill(retVal);
                }
                sqlConn.Close();

                return retVal.Tables[0];
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }

        }

        public static Int32 execCmdwithTransaction(string ConnectionString, string _sql)  // parameter
        {
            Int32 _rows = 0;
            try
            {
                DataTable dt = new DataTable();

                using (SqlConnection con = new SqlConnection(ConnectionString))
                {
                    con.Open();
                    SqlTransaction transaction;
                    transaction = con.BeginTransaction("MIS");
                    //con.BeginTransaction();
                    try
                    {
                        using (SqlCommand cmd = new SqlCommand(_sql, con))
                        {
                            cmd.CommandTimeout = 1800; //int.MaxValue;
                            cmd.CommandType = CommandType.Text;
                            cmd.Connection = con;
                            cmd.Transaction = transaction;
                            _rows = cmd.ExecuteNonQuery();
                            transaction.Commit();
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        con.Close();

                        throw new ApplicationException(ex.Message);
                    }
                    con.Close();
                }
                return _rows;
            }
            catch (Exception ex)
            {
                string _ex = ex.Message;
                return _rows;
            }

        }

        public static Object exec_CmdwithTransaction(string ConnectionString, string _sql)  // parameter
        {
            Int32 _rows = 0;
            try
            {
                DataTable dt = new DataTable();

                using (SqlConnection con = new SqlConnection(ConnectionString))
                {
                    con.Open();
                    SqlTransaction transaction;
                    transaction = con.BeginTransaction("MIS");
                    //con.BeginTransaction();
                    try
                    {
                        using (SqlCommand cmd = new SqlCommand(_sql, con))
                        {
                            cmd.CommandTimeout = 1800; //int.MaxValue;
                            cmd.CommandType = CommandType.Text;
                            cmd.Connection = con;
                            cmd.Transaction = transaction;
                            _rows = cmd.ExecuteNonQuery();
                            transaction.Commit();
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        con.Close();

                        throw new ApplicationException(ex.Message);
                    }
                    con.Close();
                }
                return _rows;
            }
            catch (Exception ex)
            {
                string _ex = ex.Message;
                return _rows;
            }

        }

        public static void ExecNonQuery(string _con, string _sql)
        {
            using (SqlConnection con = new SqlConnection(_con))
            {
                con.Open();
                SqlTransaction transaction;
                transaction = con.BeginTransaction();
                //con.BeginTransaction();
                try
                {
                    using (SqlCommand cmd = new SqlCommand(_sql, con))
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandTimeout = 1800; //int.MaxValue;
                        cmd.Connection = con;
                        cmd.Transaction = transaction;
                        cmd.ExecuteNonQuery();
                        transaction.Commit();
                    }
                }
                catch (SqlException)
                {
                    transaction.Rollback();
                    con.Close();

                    throw;
                }
                con.Close();
            }
        }

        public static void DropTableTemp(string _con, string tableName)
        {
            using (SqlConnection con = new SqlConnection(_con))
            {
                con.Open();
                string _sql = "IF OBJECT_ID('tempdb..#" + tableName + "') IS NOT NULL DROP TABLE #" + tableName + "";
                using (SqlCommand cmd = new SqlCommand(_sql, con))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandTimeout = 1800; //int.MaxValue;
                    cmd.Connection = con;
                    cmd.ExecuteNonQuery();
                }

                con.Close();
            }
        }

        public static void CreateTableForDataTable(string _con, DataTable dt, string tableName, bool isCopyData = true, bool dropexits = false, string columnName = "")
        {
            using (SqlConnection con = new SqlConnection(_con))
            {
                con.Open();

                if (dropexits)
                {
                    string _drop = "IF OBJECT_ID('dbo." + tableName + "', 'U') IS NOT NULL DROP TABLE dbo." + tableName + ";";
                    using (SqlCommand cmd = new SqlCommand(_drop, con))
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandTimeout = 1800; //int.MaxValue;
                        cmd.Connection = con;
                        cmd.ExecuteNonQuery();

                        Console.WriteLine($"_drop query = {_drop}");
                    }
                }

                List<string> cl = columnName.Split(',').ToList();
                DataTable localTempTable = new DataTable(tableName);

                #region Create Column
                var dtlFieldNames = dt.Columns.Cast<DataColumn>().
                Select(item => new {
                    Name = item.ColumnName,
                    Type = item.DataType
                }).ToList();

                foreach (var dtField in dtlFieldNames)
                {
                    if (isCopyData)
                    {
                        Console.WriteLine($"ALL Name: {dtField.Name}, TypeOf: {dtField.Type}");

                        DataColumn column = new DataColumn();
                        column.DataType = dtField.Type;
                        column.ColumnName = dtField.Name;
                        localTempTable.Columns.Add(column);
                    }
                    else
                    {
                        if (cl.Any(x => x == dtField.Name))
                        {
                            Console.WriteLine($"Any Name: {dtField.Name}, TypeOf: {dtField.Type}");

                            DataColumn column = new DataColumn();
                            column.DataType = dtField.Type;
                            column.ColumnName = dtField.Name;
                            localTempTable.Columns.Add(column);
                        }
                    }
                }
                #endregion

                if (isCopyData)
                {
                    localTempTable = dt.Copy();
                }
                else
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow row = localTempTable.NewRow();
                        for (int j = 0; j < cl.Count; j++)
                        {
                            string clName = cl[j];
                            row[j] = dt.Rows[i][clName].ToString();
                        }

                        localTempTable.Rows.Add(row);
                    }
                }

                localTempTable.AcceptChanges();

                string sql = GetCreateTableSql(localTempTable, tableName);
                Console.WriteLine(sql);
                SqlCommand create = new SqlCommand(sql, con);
                create.ExecuteNonQuery();

                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(con))
                {
                    bulkCopy.BulkCopyTimeout = 1800;
                    bulkCopy.DestinationTableName = tableName;
                    bulkCopy.WriteToServer(localTempTable);
                }

                con.Close();
            }
        }

        public static void InsertDataForDataTable(string _con, DataTable dt, string tableName, bool isCopyData = true, string columnName = "")
        {
            using (SqlConnection con = new SqlConnection(_con))
            {
                con.Open();

                List<string> cl = columnName.Split(',').ToList();
                DataTable localTempTable = new DataTable(tableName);

                #region Create Column
                var dtlFieldNames = dt.Columns.Cast<DataColumn>().
                Select(item => new {
                    Name = item.ColumnName,
                    Type = item.DataType
                }).ToList();

                foreach (var dtField in dtlFieldNames)
                {
                    if (isCopyData)
                    {
                        Console.WriteLine($"ALL Name: {dtField.Name}, TypeOf: {dtField.Type}");

                        DataColumn column = new DataColumn();
                        column.DataType = dtField.Type;
                        column.ColumnName = dtField.Name;
                        localTempTable.Columns.Add(column);
                    }
                    else
                    {
                        if (cl.Any(x => x == dtField.Name))
                        {
                            Console.WriteLine($"Any Name: {dtField.Name}, TypeOf: {dtField.Type}");

                            DataColumn column = new DataColumn();
                            column.DataType = dtField.Type;
                            column.ColumnName = dtField.Name;
                            localTempTable.Columns.Add(column);
                        }
                    }
                }
                #endregion

                if (isCopyData)
                {
                    localTempTable = dt.Copy();
                }
                else
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow row = localTempTable.NewRow();
                        for (int j = 0; j < cl.Count; j++)
                        {
                            string clName = cl[j];
                            row[j] = dt.Rows[i][clName].ToString();
                        }

                        localTempTable.Rows.Add(row);
                    }
                }

                localTempTable.AcceptChanges();

                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(con))
                {
                    bulkCopy.BulkCopyTimeout = 1800;
                    bulkCopy.DestinationTableName = tableName;
                    bulkCopy.WriteToServer(localTempTable);
                }

                con.Close();
            }
        }

        public static void UpdateDataFromDataTable(string _con, DataTable dt, string tableName, bool isCopyData = true, string columnName = "")
        {
            using (SqlConnection conn = new SqlConnection(_con))
            {
                using (SqlTransaction tran = conn.BeginTransaction())
                {

                    try
                    {
                        conn.Open();

                        string tableTempName = $"#{tableName}";

                        #region drop table temp
                        string _drop = "IF OBJECT_ID('tempdb..#" + tableName + "') IS NOT NULL DROP TABLE #" + tableName + "";
                        using (SqlCommand cmd = new SqlCommand(_drop, conn))
                        {
                            cmd.CommandType = CommandType.Text;
                            cmd.CommandTimeout = 1800; //int.MaxValue;
                            cmd.Connection = conn;
                            cmd.ExecuteNonQuery();

                            Console.WriteLine($"_drop temp query = {_drop}");
                        }
                        #endregion

                        #region create table temp
                        List<string> cl = columnName.Split(',').ToList();
                        DataTable localTempTable = new DataTable(tableName);

                        #region Create Column
                        var dtlFieldNames = dt.Columns.Cast<DataColumn>().
                        Select(item => new {
                            Name = item.ColumnName,
                            Type = item.DataType
                        }).ToList();

                        foreach (var dtField in dtlFieldNames)
                        {
                            if (isCopyData)
                            {
                                Console.WriteLine($"ALL Name: {dtField.Name}, TypeOf: {dtField.Type}");

                                DataColumn column = new DataColumn();
                                column.DataType = dtField.Type;
                                column.ColumnName = dtField.Name;
                                localTempTable.Columns.Add(column);
                            }
                            else
                            {
                                if (cl.Any(x => x == dtField.Name))
                                {
                                    Console.WriteLine($"Any Name: {dtField.Name}, TypeOf: {dtField.Type}");

                                    DataColumn column = new DataColumn();
                                    column.DataType = dtField.Type;
                                    column.ColumnName = dtField.Name;
                                    localTempTable.Columns.Add(column);
                                }
                            }
                        }
                        #endregion

                        #region insert data to datatable
                        if (isCopyData)
                        {
                            localTempTable = dt.Copy();
                        }
                        else
                        {
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                DataRow row = localTempTable.NewRow();
                                for (int j = 0; j < cl.Count; j++)
                                {
                                    string clName = cl[j];
                                    row[j] = dt.Rows[i][clName].ToString();
                                }

                                localTempTable.Rows.Add(row);
                            }
                        }

                        localTempTable.AcceptChanges();
                        #endregion

                        #region create table and execute
                        string sql = GetCreateTableSql(localTempTable, tableTempName);
                        Console.WriteLine(sql);
                        SqlCommand create = new SqlCommand(sql, conn);
                        create.ExecuteNonQuery();

                        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(conn))
                        {
                            bulkCopy.BulkCopyTimeout = 1800;
                            bulkCopy.DestinationTableName = tableTempName;
                            bulkCopy.WriteToServer(localTempTable);
                        }
                        #endregion

                        #endregion


                        // Updating destination table
                        #region Updating destination table
                        using (SqlCommand cmd = new SqlCommand(_drop, conn))
                        {
                            cmd.CommandType = CommandType.Text;
                            cmd.CommandTimeout = 1800; //int.MaxValue;
                            cmd.Connection = conn;

                            StringBuilder _sql = new StringBuilder();
                            _sql.Append("UPDATE T");
                            _sql.AppendFormat("\n\tSET {0}", "");
                            _sql.AppendFormat("\n\tFROM {0} T ", tableName);
                            _sql.AppendFormat("\n\tINNER JOIN {0} Temp ON {1} ", tableTempName);
                            _sql.AppendFormat("\n\t{0}", "");
                            cmd.CommandText = _sql.ToString();
                            cmd.ExecuteNonQuery();
                        }
                        #endregion

                        #region dropping temp table
                        using (SqlCommand cmd = new SqlCommand(_drop, conn))
                        {
                            cmd.CommandType = CommandType.Text;
                            cmd.CommandTimeout = 1800; //int.MaxValue;
                            cmd.Connection = conn;
                            cmd.ExecuteNonQuery();

                            Console.WriteLine($"_drop temp query = {_drop}");
                        }
                        #endregion

                        tran.Commit();
                    }
                    catch (Exception ex)
                    {
                        tran.Rollback();
                        conn.Close();

                        throw new ApplicationException(ex.Message);
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Inspects a DataTable and return a SQL string that can be used to CREATE a TABLE in SQL Server.
        /// </summary>
        /// <param name="table">System.Data.DataTable object to be inspected for building the SQL CREATE TABLE statement.</param>
        /// <returns>String of SQL</returns>
        public static string GetCreateTableSql(DataTable table, string tableName = "")
        {
            StringBuilder sql = new StringBuilder();
            StringBuilder alterSql = new StringBuilder();

            tableName = String.IsNullOrEmpty(tableName) ? table.TableName : tableName;

            sql.AppendFormat("CREATE TABLE {0} (", tableName);

            for (int i = 0; i < table.Columns.Count; i++)
            {
                bool isNumeric = false;
                bool usesColumnDefault = true;

                sql.AppendFormat("\n\t[{0}]", table.Columns[i].ColumnName);

                string columnSize = ((table.Columns[i].MaxLength == -1) ? "255" : (table.Columns[i].MaxLength > 8000) ? "MAX" : table.Columns[i].MaxLength.ToString());
                switch (table.Columns[i].DataType.ToString().ToUpper())
                {
                    case "SYSTEM.INT16":
                        sql.Append(" smallint");
                        isNumeric = true;
                        break;
                    case "SYSTEM.INT32":
                        sql.Append(" int");
                        isNumeric = true;
                        break;
                    case "SYSTEM.INT64":
                        sql.Append(" bigint");
                        isNumeric = true;
                        break;
                    case "SYSTEM.DATETIME":
                        sql.Append(" datetime");
                        usesColumnDefault = false;
                        break;
                    case "SYSTEM.STRING":
                        sql.AppendFormat(" VARCHAR({0})", columnSize);
                        break;
                    case "SYSTEM.SINGLE":
                        sql.Append(" single");
                        isNumeric = true;
                        break;
                    case "SYSTEM.DOUBLE":
                        sql.Append(" double");
                        isNumeric = true;
                        break;
                    case "SYSTEM.DECIMAL":
                        sql.AppendFormat(" decimal(21, 6)");
                        isNumeric = true;
                        break;
                    case "System.Guid":
                        sql.AppendFormat(" UNIQUEIDENTIFIER");
                        break;
                    default:
                        sql.AppendFormat(" VARCHAR({0})", columnSize);
                        break;
                }

                if (table.Columns[i].AutoIncrement)
                {
                    sql.AppendFormat(" IDENTITY({0},{1})",
                        table.Columns[i].AutoIncrementSeed,
                        table.Columns[i].AutoIncrementStep);
                }
                else
                {
                    // DataColumns will add a blank DefaultValue for any AutoIncrement column. 
                    // We only want to create an ALTER statement for those columns that are not set to AutoIncrement. 
                    if (table.Columns[i].DefaultValue != null)
                    {
                        if (usesColumnDefault)
                        {
                            if (isNumeric)
                            {
                                var val = (table.Columns[i].DefaultValue.ToString() == string.Empty) ? "'0'" : table.Columns[i].DefaultValue;
                                alterSql.AppendFormat("\nALTER TABLE {0} ADD CONSTRAINT [DF_{0}_{1}]  DEFAULT ({2}) FOR [{1}];",
                                    tableName,
                                    table.Columns[i].ColumnName,
                                    val);
                            }
                            else
                            {
                                alterSql.AppendFormat("\nALTER TABLE {0} ADD CONSTRAINT [DF_{0}_{1}]  DEFAULT ('{2}') FOR [{1}];",
                                    tableName,
                                    table.Columns[i].ColumnName,
                                    table.Columns[i].DefaultValue);
                            }
                        }
                        else
                        {
                            // Default values on Date columns, e.g., "DateTime.Now" will not translate to SQL.
                            // This inspects the caption for a simple XML string to see if there is a SQL compliant default value, e.g., "GETDATE()".
                            try
                            {
                                System.Xml.XmlDocument xml = new System.Xml.XmlDocument();

                                xml.LoadXml(table.Columns[i].Caption);

                                alterSql.AppendFormat("\nALTER TABLE {0} ADD CONSTRAINT [DF_{0}_{1}]  DEFAULT ({2}) FOR [{1}];",
                                    tableName,
                                    table.Columns[i].ColumnName,
                                    xml.GetElementsByTagName("defaultValue")[0].InnerText);
                            }
                            catch
                            {
                                // Handle
                            }
                        }
                    }
                }

                if (!table.Columns[i].AllowDBNull)
                {
                    sql.Append(" NOT NULL");
                }

                sql.Append(",");
            }

            if (table.PrimaryKey.Length > 0)
            {
                StringBuilder primaryKeySql = new StringBuilder();

                primaryKeySql.AppendFormat("\n\tCONSTRAINT PK_{0} PRIMARY KEY (", tableName);

                for (int i = 0; i < table.PrimaryKey.Length; i++)
                {
                    primaryKeySql.AppendFormat("{0},", table.PrimaryKey[i].ColumnName);
                }

                primaryKeySql.Remove(primaryKeySql.Length - 1, 1);
                primaryKeySql.Append(")");

                sql.Append(primaryKeySql);
            }
            else
            {
                sql.Remove(sql.Length - 1, 1);
            }

            sql.AppendFormat("\n);\n{0}", alterSql.ToString());

            return sql.ToString();
        }

    }
}